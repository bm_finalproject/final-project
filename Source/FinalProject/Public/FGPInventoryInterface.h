/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPInventoryInterface.h
 * Description	: Interface for actors that provide a set of FGPItems
 *					bound to ItemSlots.
 *					
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "FGPTypes.h"
#include "FGPInventoryInterface.generated.h"

/**
* Interface for actors that provide a set of FGPItems bound to ItemSlots
* This exists so FGPCharacterBase can query inventory without doing hacky
* player controller casts.
* It is designed only for use by native classes.
*/
UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UFGPInventoryInterface
	: public UInterface
{
	GENERATED_BODY()
};

class FINALPROJECT_API IFGPInventoryInterface
{
	GENERATED_BODY()
	
public:
	/** Returns the map of items to data */
	virtual const TMap<UFGPItem*, FFGPItemData>&
		GetInventoryDataMap() const = 0;

	/** Returns the map of slots to items */
	virtual const TMap<FFGPItemSlot, UFGPItem*>&
		GetSlottedItemMap() const = 0;

	/** Gets the delegate for inventory item changes */
	virtual FOnInventoryItemChangedNative& GetInventoryItemChangedDelegate() = 0;

	/** Gets the delegate for inventory slot changes */
	virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate() = 0;

	/** Gets the delegate for when the inventory loads */
	virtual FOnInventoryLoadedNative& GetInventoryLoadedDelegate() = 0;
};