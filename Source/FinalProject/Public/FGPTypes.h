/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPTypes.h
 * Description	: This header is for enums and structs used by classes and
 *		blueprints across the game. Collecting these in a single header helps 
 *		avoid problems with recursive header includes. It's also a good place 
 *		to put things like data table row structs.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once


#include "UObject/PrimaryAssetId.h"
#include "Runtime/GameplayTags/Classes/GameplayTagContainer.h"
#include "FGPTypes.generated.h"

class UFGPItem;

/** Struct representing a slot for an item, shown in the UI */
USTRUCT(BlueprintType)
struct FINALPROJECT_API FFGPItemSlot
{
	GENERATED_BODY()

public:
	/** Constructor, -1 means an invalid slot */
	FFGPItemSlot()
		: SlotNumber(-1)
	{}

	FFGPItemSlot(const FPrimaryAssetType& InItemType, int32 InSlotNumber)
		: ItemType(InItemType)
		, SlotNumber(InSlotNumber)
	{}
	
	/** The type of items that can go in this slot */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	FPrimaryAssetType ItemType;

	/** The number of this slot, 0 indexed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 SlotNumber;

	/** Equality operators */
	bool operator==(const FFGPItemSlot& Other) const
	{
		return (ItemType == Other.ItemType && SlotNumber == Other.SlotNumber);
	}
	bool operator!=(const FFGPItemSlot& Other) const
	{
		return !(*this == Other);
	}

	/** Implemented so it can be used in Maps/Sets */
	friend inline uint32 GetTypeHash(const FFGPItemSlot& Key)
	{
		uint32 Hash = 0;
		
		Hash = HashCombine(Hash, GetTypeHash(Key.ItemType));
		Hash = HashCombine(Hash, (uint32)Key.SlotNumber);
		return Hash;
	}

	/** Returns true if slot is valid */
	bool IsValid() const
	{
		return (ItemType.IsValid() && SlotNumber >= 0);
	}

	// TODO : decide if inventory has a max number of slots
};

/** Extra information about a UFGPItem that is in a player's inventory */
USTRUCT(BlueprintType)
struct FINALPROJECT_API FFGPItemData
{
	GENERATED_BODY()
	
public:
	/**
	 * Constructor, default to count/level 1 so declaring them in blueprints
	 * gives us the expected behaviour.
	 */
	FFGPItemData() 
		: ItemCount(1)
		, ItemLevel(1)
	{}


	FFGPItemData(int32 InItemCount, int32 InItemLevel)
		: ItemCount(InItemCount)
		, ItemLevel(InItemLevel)
	{}
	
	/** Struct UPROPERTY */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 ItemCount;

	/** Struct UPROPERTY */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Item)
	int32 ItemLevel;

	/** Equality operators */
	bool operator==(const FFGPItemData& Other) const
	{
		return (ItemCount == Other.ItemCount && ItemLevel == Other.ItemLevel);
	}
	bool operator!=(const FFGPItemData& Other) const
	{
		return !(*this == Other);
	}

	/** Returns true if count is greated than 0 */
	bool IsValid() const
	{
		return (ItemCount >= 0);
	}

	/** Append an item data, this adds the count and overrides everything else */
	void UpdateItemData(
		const FFGPItemData& Other
		, int32 MaxCount
		, int32 MaxLevel
	) {
		if (MaxCount <= 0)
		{
			MaxCount = MAX_int32;
		}

		if (MaxLevel <= 0)
		{
			MaxLevel = MAX_int32;
		}

		ItemCount = FMath::Clamp(ItemCount + Other.ItemCount, 1, MaxCount);
		ItemLevel = FMath::Clamp(Other.ItemLevel, 1, MaxLevel);
	}
};

/** Delegate called when an inventory item changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(
	FOnInventoryItemChanged, bool, bAdded, UFGPItem*, Item);
DECLARE_MULTICAST_DELEGATE_TwoParams(
	FOnInventoryItemChangedNative, bool, UFGPItem*);

/** Delegate called when the contents of an inventory slot change */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(
	FOnSlottedItemChanged, FFGPItemSlot, ItemSlot, UFGPItem*, Item);
DECLARE_MULTICAST_DELEGATE_TwoParams(
	FOnSlottedItemChangedNative, FFGPItemSlot, UFGPItem*);

/**
 * Delegate called when the entire inventory has been loaded, all items may
 * have been replaced.
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInventoryLoaded);
DECLARE_MULTICAST_DELEGATE(FOnInventoryLoadedNative);


////// Damage info structure targeting AFGPCharacterBase subclasses ////////
USTRUCT(BlueprintType)
struct FINALPROJECT_API FDamageInfoParams
{

	GENERATED_BODY()

	public:
	/** Constructor null damage */
	FDamageInfoParams()
		: DamageAmount(0.0f)
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info Params")
	float DamageAmount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info Params")
	FHitResult HitInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info Params")
	FGameplayTagContainer DamageTags;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info Params")
	TWeakObjectPtr<class AFGPCharacterBase> InstigatorPawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info Params")
	TWeakObjectPtr<class AActor> DamageCauser;
};

/**
 * Gender property type for characters
 */
UENUM(BlueprintType)
enum class ECharacterGender : uint8
{
	Female					UMETA(DisplayName = "Female"),				/** Female */
	Male					UMETA(DisplayName = "Male"),				/** Male */
	//256th entry
	ECG_MAX					UMETA(Hidden)
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGenderChanged, ECharacterGender, Gender);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnGenderChangedNative, ECharacterGender);