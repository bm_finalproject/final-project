/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCharacterBase.h
 * Description	: Base class for Character
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "GameFramework/Character.h"
#include "Abilities/FGPAbilitySystemComponent.h"
#include "Abilities/FGPAttributeSet.h"
#include "FGPTypes.h"
#include "FGPInventoryInterface.h"
#include "FGPPostInitializationInterface.h"
#include "CharacterModular/FGPCharacterModularTypes.h"

#include "Runtime/CoreUObject/Public/UObject/ScriptInterface.h"
#include "GameplayAbilities/Public/AbilitySystemInterface.h"
#include "FGPCharacterBase.generated.h"

class UFGPGameplayAbility;
class UGameplayEffect;

/** Base class for Character, designed to be blueprinted */
UCLASS()
class FINALPROJECT_API AFGPCharacterBase
	: public ACharacter
	, public IAbilitySystemInterface
	, public IFGPPostInitializationInterface
{
	GENERATED_BODY()

public:
	/** Constructor and overrides */
	AFGPCharacterBase();
	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;
	virtual void OnRep_Controller() override;
	virtual void GetLifetimeReplicatedProps(
		TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Implement IAbilitySystemInterface
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	/** Returns current health, will be 0 if dead */
	UFUNCTION(BlueprintCallable)
	virtual float GetHealth() const;

	/** Returns maximum health, health will never be greater than this */
	UFUNCTION(BlueprintCallable)
	virtual float GetMaxHealth() const;

	/** Returns current mana */
	UFUNCTION(BlueprintCallable)
	virtual float GetMana() const;

	/** Returns maximum mana, mana will never be greater than this */
	UFUNCTION(BlueprintCallable)
	virtual float GetMaxMana() const;

	/** Returns current movement speed */
	UFUNCTION(BlueprintCallable)
	virtual float GetMoveSpeed() const;

	/** Returns the character level that is passed to the ability system */
	UFUNCTION(BlueprintCallable)
	virtual int32 GetCharacterLevel() const;

	/** 
	 * Modifies the character level, this may change abilities.
	 * return | true on success
	 */
	UFUNCTION(BlueprintCallable)
	virtual bool SetCharacterLevel(int32 NewLevel);

	/** 
	 * Attempts to activate any ability in the specified item slot. Will return
	 * false if no activatable ability is found or activation fails.
	 * Returns true if it thinks it activated, but it may return false positives
	 * due to failure later in activation.
	 * If bAllowRemoteActivation is true, it will remotely activate local/server
	 * abilities, if false it will only try to locally activate the ability.
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool ActivateAbilitiesWithItemSlot(FFGPItemSlot ItemSlot,
		bool bAllowRemoteActivation = true);

	/**
	 * Returns a list of active abilities bound to the item slot.
	 * This only returns if the ability is currently running
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void GetActiveAbilitiesWithItemSlot(FFGPItemSlot ItemSlot,
		TArray<UFGPGameplayAbility*>& ActiveAbilities);

	/**
	 * Attempts to activate all abilities that math the specified tags
	 * Returns true if it thinks it activated, but it may return false positives
	 * due to failure later in activation.
	 * If bAllowRemoteActivation is true, it will remotely activate local/server
	 * abilities, if false it will only try to locally activate the ability.
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool ActivateAbilitiesWithTags(FGameplayTagContainer AbilityTags,
		bool bAllowRemoteActivation = true);

	/**
	 * Returns a list of active abilities matching the specified tags. This
	 * only returns if the ability is currently running.
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void GetActiveAbilitiesWithTags(FGameplayTagContainer AbilityTags,
		TArray<UFGPGameplayAbility*>& ActiveAbilities);

	/**
	 * Returns total time and remaining time for cooldown tags.
	 * Returns false if no active cooldowns found.
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool GetCooldownRemainingForTag(FGameplayTagContainer CooldownTags,
		float& TimeRemaining, float& CooldownDuration);

	/**
	 * Returns true if character has any interactive tag
	 */
	UFUNCTION(BlueprintCallable, Category = "Interaction")
	bool IsInteractable(FGameplayTagContainer TagContainer) const;

	/**
	 * Called after character takes damage, for displaying damage amount on scree
	 *
	 * @param DamageAmount Amount of damage that was done, not clamped based
	 *		on current health.
	 */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnShowDamageAmount(const float& Damage);

	/** Delegate called when the character skeletal mesh is changed */
	UPROPERTY(BlueprintAssignable, Category = "FGP Character Modular Parts")
		FOnSkeletalMeshChanged OnSkeletalMeshChanged;
	/** Native version of above delegate, called before BP delegate */
	FOnSkeletalMeshChangedNative OnSkeletalMeshChangedNative;


	virtual void PostInitializeComponents() override;

	virtual bool FGPPostInitializeComponents_Implementation() override;

	//virtual void Tick(float DeltaSeconds) override;

protected:

	UPROPERTY(EditAnywhere, Replicated, Category = Abilities)
	float Health;

	/**
	 * The level of this character, should not be modified directly
	 * once it has already spawned
	 */
	UPROPERTY(EditAnywhere, Replicated, Category = Abilities)
	int32 CharacterLevel;

	/**
	 * Abilities to grant to this character on creation.
	 * These will be activated by tag or event and are not bound
	 * to specific inputs.
	 */
	UPROPERTY(EditAnywhere, Replicated, Category = Abilities)
	TArray<TSubclassOf<UFGPGameplayAbility>> GameplayAbilities;

	/**
	 * Map of item slot to gameplay ability class, these are bound before any
	 * abilities added by the inventory.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	TMap<FFGPItemSlot, TSubclassOf<UFGPGameplayAbility>> DefaultSlottedAbilities;

	/**
	 * Passive gameplay effects applied on creation.
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Abilities)
	TArray<TSubclassOf<UGameplayEffect>> PassiveGameplayEffects;

	/** The component used to handle ability system interactions */
	UPROPERTY()
	UFGPAbilitySystemComponent* AbilitySystemComponent;

	/** List of attributes modified by the ability system */
	UPROPERTY()
	UFGPAttributeSet* AttributeSet;

	/**
	 * Cached pointer to the inventory source for this character,
	 * can be null
	 */
	UPROPERTY()
	TScriptInterface<IFGPInventoryInterface> InventorySource;

	/** If true we have initialized our abilities */
	UPROPERTY()
	bool bAbilitiesInitialized;

	/**
	 * Map of slot to ability granted by that slot.
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	TMap<FFGPItemSlot, FGameplayAbilitySpecHandle> SlottedAbilities;

	/** Delegate handles */
	FDelegateHandle InventoryUpdateHandle;
	FDelegateHandle InventoryLoadedHandle;

	/**
	 * Called when character takes damage, which may have killed them
	 *
	 * @param DamageAmount Amount of damage that was done, not clamped based
	 *		on current health.
	 * @param HitInfo The hit info that generated this damage
	 * @param DamageTags The gameplay tags of the event that did the damage
	 * @param InstigatorCharacter The character that initiated this damage
	 * @param DamageCauser The actual actor that did the damage, might be
	 *		a weapon or projectile
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void OnDamaged(const FDamageInfoParams& DamageInfoParams);

	/**
	 * Called when health is changed, either from healing or from being damaged
	 * For damage this is called in addition to OnDamaged/OnKilled
	 *
	 * @param DeltaValue Change in health value, positive for heal, negative
	 *		for cost. If 0 the delta is unknown
	 * @param EventTags The gameplay tags of the event that changed health
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void OnHealthChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);

	/**
	 * Called when mana is changed, either from healing or from being
	 * used as a cost
	 *
	 * @param DeltaValue Change in mana value, positive for heal, negative
	 *		for cost. If 0 the delta is unknown
	 * @param EventTags The gameplay tags of the event that changed mana
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void OnManaChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void OnDeathEvent();

	/**
	 * Called when movement speed is changed
	 *
	 * @param DeltaValue Change in move speed
	 * @param EventTags The gameplay tags of the event that changed
	 *		movement speed
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void OnMoveSpeedChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);

	/** Called when slotted items change, bound to delegate on interface */
	void OnItemSlotChanged(FFGPItemSlot ItemSlot, UFGPItem* Item);
	void RefreshSlottedGameplayAbilities();

	/** Apply the startup gameplay abilities and effects */
	void AddStartupGameplayAbilities();

	/** Attempts to remove any startup gameplay abilities */
	void RemoveStartupGameplayAbilities();

	/** Adds slotted item abilities if needed */
	void AddSlottedGameplayAbilities();

	/** Fills in with ability specs, based on defaults and inventory */
	void FillSlottedAbilitySpecs(TMap<FFGPItemSlot,
		FGameplayAbilitySpec>& SlottedAbilitySpecs);

	/** 
	 * Remove slotted abilities, if bRemoveAll is false it only
	 * removes invalid ones
	 */
	void RemoveSlottedGameplayAbilities(bool bRemoveAll);

	// Called from UFGPRPGAttributeSet, these call BP events above
	virtual void HandleDamage(const FDamageInfoParams& DamageInfoParams);
	virtual void HandleHealthChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);
	virtual void HandleManaChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);
	virtual void HandleMoveSpeedChanged(float DeltaValue,
		const struct FGameplayTagContainer& EventTags);

	/**
	 * Gameplay tags for dealing with interaction feature
	 */
	UPROPERTY(EditAnywhere, Replicated, Category = Interaction)
	FGameplayTagContainer InteractionTags;

	// Friended to allow access to handle functions above
	friend UFGPAttributeSet;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "FGP Character Modular Parts")
	void OnSkeletalMeshChangedEvent();
};