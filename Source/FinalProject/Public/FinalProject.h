/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FinalProject.h
 * Description	: Final Game Project common header files included statements,
 *					ready-to-use log functions and helper functions library.
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "EngineMinimal.h"
#include "Engine/Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Public/Net/UnrealNetwork.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

#include "FGPTypes.h"

//Global project define rules
#define BUFFER_SIZE 2*1024*1024
//Input list
#define InputSetDestination FName("SetDestination")
#define InputZoomIn FName("ZoomIn")
#define InputZoomOut FName("ZoomOut")
#define InputDisplayInventory FName("DisplayInventory")
#define InputHot1 FName("Hot1")
#define InputHot2 FName("Hot2")
#define InputHot3 FName("Hot3")
#define InputHot4 FName("Hot4")
#define InputHot5 FName("Hot5")
#define InputHot6 FName("Hot6")
#define InputHot7 FName("Hot7")
#define InputHot8 FName("Hot8")
#define InputHot9 FName("Hot9")
#define InputHot0 FName("Hot0")
#define InputMainAbility FName("MainAbility")
#define FGP_LOG_CATEGORY FName("LogGame")

FINALPROJECT_API DECLARE_LOG_CATEGORY_EXTERN(LogGame, Log, All);

FORCEINLINE void ScreenMsg(const FString& Msg)
{
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, *Msg);
}

FORCEINLINE void ScreenMsg(const FString& Msg, const float Value)
{
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White,
			FString::Printf(TEXT("%s %f"), *Msg, Value));
}

FORCEINLINE void ScreenMsg(const FString& Msg, const FString& Msg2)
{
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White,
			FString::Printf(TEXT("%s %s"), *Msg, *Msg2));
}

FORCEINLINE void ScreenMsg(const FColor& color, const FString& Msg)
{
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, color, *Msg);
}

FORCEINLINE void ScreenMsg(
	const FColor& color
	, const FString& Msg
	, const float Value
) {
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, color,
			FString::Printf(TEXT("%s %f"), *Msg, Value));
}

FORCEINLINE void ScreenMsg(
	const FColor& color
	, const FString& Msg
	, const FString& Msg2
) {
	if (GEngine != NULL)
		GEngine->AddOnScreenDebugMessage(-1, 5.f, color,
			FString::Printf(TEXT("%s %s"), *Msg, *Msg2));
}



FORCEINLINE void LogMsg(const FString& Msg, const FString& Msg2)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY, ELogVerbosity::Log,
			FString::Printf(TEXT("%s %s"), *Msg, *Msg2));
}

template <typename T>
FORCEINLINE void LogMsg(const T& Msg)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY, ELogVerbosity::Log, Msg);
}

template <typename T>
FORCEINLINE void LogWarning(const T& Msg)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY, ELogVerbosity::Warning, Msg);
}

FORCEINLINE void LogWarning(const FString& Msg)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY, ELogVerbosity::Warning, Msg);
}

FORCEINLINE void LogWarning(const FString& Msg, const FString& Msg2)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY,
			ELogVerbosity::Warning,
			FString::Printf(TEXT("%s %s"), *Msg, *Msg2));
}

template <typename T>
FORCEINLINE void LogFatal(const T& Msg)
{
	if (GLog != NULL)
		GLog->Log(FGP_LOG_CATEGORY, ELogVerbosity::Fatal, Msg);
}

FORCEINLINE const FString EnumToString(const TCHAR* Enum, int32 EnumValue)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, Enum, true);
	if (!EnumPtr)
		return NSLOCTEXT("Invalid", "Invalid", "Invalid").ToString();

	//#if WITH_EDITOR
	return EnumPtr->GetDisplayNameTextByIndex(EnumValue).ToString();
	//#endif
		//return "";
}


//TEMPLATE Load Obj From Path
template <typename ObjClass>
static FORCEINLINE ObjClass* LoadObjFromPath(const FName& Path)
{
	if (Path == NAME_None) return NULL;
	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(),
		NULL, *Path.ToString()));
}

//Load Static Mesh From Path
static FORCEINLINE UStaticMesh* LoadMeshFromPath(const FName& Path)
{
	if (Path == NAME_None) return NULL;
	return LoadObjFromPath<UStaticMesh>(Path);
}


//Helper for implementing delay events
/*
 *FTimerHandle UnusedHandle;
 GetWorldTimerManager().SetTimer(
	 UnusedHandle, this, &AMyActor::TimerElapsed, TimerDelay, false);
*/