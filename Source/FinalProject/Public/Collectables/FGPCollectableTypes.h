/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCollectableTypes.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once


UENUM(BlueprintType)
enum class EFGPCollectableTypesEnum : uint8
{
	CT_Sword		UMETA(DisplayName = "Sword"),		/** Basic sword */
	CT_Shield		UMETA(DisplayName = "Shield"),		/** Basic shield */

	//256th entry
	CT_MAX			UMETA(Hidden)
};