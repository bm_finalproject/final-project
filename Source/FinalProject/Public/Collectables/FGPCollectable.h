/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCollectable.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Collectables/FGPCollectableTypes.h"

//Engine
#include "GameFramework/Actor.h"
#include "FGPCollectable.generated.h"

class UFGPDataSingleton;
class UFGPCollectableItem;
class UFGPInteractionComponent;

class USphereComponent;
class UStaticMeshComponent;

UCLASS(Category = "Items")
class FINALPROJECT_API AFGPCollectable
	: public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFGPCollectable(const FObjectInitializer& ObjectInitializer);

	/** Interaction component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	UFGPInteractionComponent* InteractionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	USphereComponent* TriggerArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Items")
	UFGPCollectableItem* ItemType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items", meta=(ExposeOnSpawn="true"))
	EFGPCollectableTypesEnum CollectableType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	

private:
	UPROPERTY()
	UFGPDataSingleton* DataSingleton;

};
