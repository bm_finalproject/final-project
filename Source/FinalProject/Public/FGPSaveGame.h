/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPSaveGame.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "GameFramework/SaveGame.h"
#include "FGPTypes.h"
#include "Items/FGPItem.h"
#include "FGPSaveGame.generated.h"

/** List of versions, native code will handle fix-ups for any old versions */
namespace EFGPSaveGameVersion
{
	enum type
	{
		// Initial version
		Initial,
		//Added Inventory
		AddedInventory,
		//Added ItemData to store count/level
		AddedItemData,

		// ---<new versions must be added before this line>------------
		VersionPlusOne,
		LatestVersion = VersionPlusOne - 1
	};
}

/**
 * Object that is written to and read from the save game archive, with
 * data version
 */
UCLASS(BlueprintType)
class FINALPROJECT_API UFGPSaveGame
	: public USaveGame
{
	GENERATED_BODY()
	
public:
	// Constructor
	UFGPSaveGame()
	{
		// Set to current version, this will get overwritten during
		// serialization when loading
		SavedDataVersion = EFGPSaveGameVersion::LatestVersion;
	}

	/** Map of items to item data */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category	= SaveGame)
	TMap<FPrimaryAssetId, FFGPItemData> InventoryData;

	/** Map of slotted items */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = SaveGame)
	TMap<FFGPItemSlot, FPrimaryAssetId> SlottedItems;

	/** User's unique id */
	FString UserId;

protected:
	/** Deprecated way of storing items, this is read in but not saved out */
	UPROPERTY()
	TArray<FPrimaryAssetId> InventoryItems_DEPRECATED;

	/** What latestVersion was when the archive was saved */
	UPROPERTY()
	int32 SavedDataVersion;

	/** Overridden to allow version fix-ups */
	virtual void Serialize(FArchive& Ar) override;
};
