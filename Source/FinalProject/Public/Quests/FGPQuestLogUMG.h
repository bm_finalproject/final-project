/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLogUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPQuestLogUMG.generated.h"

class AFGPQuest;
class UFGPQuestLog;
class UFGPQuestDescriptionUMG;
class UFGPQuestLogQuestListEntryUMG;

class UButton;
class UScrollBox;
class UTextBlock;
class UVerticalBox;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPQuestLogUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public: //UPROPERTIES
	
	/** The quest actor to which this widget is concerned with */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	UFGPQuestLog* QuestLog;

	/** The text widget that displays the quest name */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UScrollBox* ScrollBox_QuestList;

	/** A interactive button */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_QuestListShow;

	/** A interactive button */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_CompletedQuestsListShow;

	/** A interactive button for removing the quest being displayed from the user's accepted quests */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_AbandonQuest;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UFGPQuestDescriptionUMG* WB_Quest_Description;

	/** Title of quest currently displaying */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Quest_Title;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* VerticalBox_QuestDetailsContainer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	TSubclassOf<UFGPQuestLogQuestListEntryUMG> QuestListEntryUMGLayout;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	bool bIsDisplayingCompleted;
	
protected:
	virtual void NativeConstruct() override;

public: //METHODS

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateElements();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestLog(UFGPQuestLog* NewQuestLog);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestLogChanged();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void DisplayQuestDetails(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestToDisplay(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void AbandonQuest();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void ToggleIsDisplayingCompleted();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetIsDisplayingCompleted(const bool& bIsDisplaying);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void IsDisplayingCompletedChanged(bool& bIsDisplaying);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	ESlateVisibility QuestListBoxVisibilityState;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	AFGPQuest* QuestInDisplay;
};
