/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestObjectiveItemUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "Quests/FGPQuestTypes.h"
#include "FGPQuestObjectiveItemUMG.generated.h"

class UCheckBox;
class UTextBlock;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPQuestObjectiveItemUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	/** Objective IsComplete check box widget */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UCheckBox* CheckBox_IsComplete;
	
	/** Objective description text block widget */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Description;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn="true"))
	FFGPObjectiveData ObjectiveData;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FText CompletionMarkSymbol;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FText UnCompletedMarkSymbol;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	bool bIsInsideJournal;

protected:
	//Constructor
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetObjectiveData(FFGPObjectiveData Objective);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void ObjectiveDataChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateElements();


private:

};
