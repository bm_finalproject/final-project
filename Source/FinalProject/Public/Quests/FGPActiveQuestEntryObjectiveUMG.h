/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPActiveQuestEntryObjectiveUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "Quests/FGPQuestTypes.h"
#include "FGPActiveQuestEntryObjectiveUMG.generated.h"

class UTextBlock;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPActiveQuestEntryObjectiveUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:

	/** Objective description text block widget */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Description;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	FFGPObjectiveData ObjectiveData;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_CompleteMarker;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FText CompletionMarkSymbol;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FText UnCompletedMarkSymbol;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	bool bIsInsideQuestPrompt;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System")
	bool bIsInsideActiveQuestPanel;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FSlateFontInfo TextFontInsideActiveQuestsPanel;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Quest System")
	FSlateFontInfo TextFontInsideOtherPanels;
	
protected:
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetObjectiveData(FFGPObjectiveData Objective);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void ObjectiveDataChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateElements();

};
