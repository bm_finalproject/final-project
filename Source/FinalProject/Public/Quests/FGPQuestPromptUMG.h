/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestPromptUMG.h
 * Description	: Native class for UMG
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPQuestPromptUMG.generated.h"

class AFGPQuest;
class UButton;
class UTextBlock;
class UScrollBox;
class UFGPQuestObjectiveItemUMG;
class UFGPQuestLog;
class UFGPActiveQuestEntryObjectiveUMG;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPQuestPromptUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:
	// Binding widgets to custom native logic

	/** Title of the quest panel*/
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* Quest_Title;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* Quest_Description;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_Reject;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_Accept;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UScrollBox* ScrollBox_Objectives;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Reject;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Accept;

	UFUNCTION(BlueprintCallable)
	void AcceptQuest();

	UFUNCTION(BlueprintCallable)
	void CloseDialog();

	UFUNCTION(BlueprintCallable)
	void TurnInQuest();

	UFUNCTION(BlueprintCallable)
	void DisplayDialog();

	bool bIsBeingDisplayed;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	AFGPQuest* GetQuestInformation() const;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	AFGPQuest* QuestInformation;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestInformation(AFGPQuest* NewQuest);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestInformationChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateQuestDialogElements();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	TSubclassOf<UFGPActiveQuestEntryObjectiveUMG> ObjectiveItemUMGLayout;
	

protected:
	virtual void NativeConstruct() override;

private:
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetDefaultValuesToQuestInformationElements();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta =(AllowPrivateAccess="true"))
	UFGPQuestLog* QuestLog;
};
