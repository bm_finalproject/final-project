/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLog.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Components/ActorComponent.h"
#include "FGPQuestLog.generated.h"

class AFGPQuest;
class UFGPQuestLog;
class UFGPMainQuestLabelUMG;
class AFGPPlayerControllerBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFGPMainQuestsContainerChanged, AFGPQuest*, NewQuest);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFGPActiveQuestsContainerChanged);


UCLASS(Blueprintable, BlueprintType, Category=QuestSystem, ClassGroup=(QuestSystem), meta=(BlueprintSpawnableComponent) )
class FINALPROJECT_API UFGPQuestLog
	: public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFGPQuestLog(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System")
	TArray<AFGPQuest*> Quests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System")
	TArray<AFGPQuest*> MainQuests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System")
	TArray<AFGPQuest*> SideQuests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System")
	TArray<AFGPQuest*> ActiveQuests;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FFGPMainQuestsContainerChanged OnMainQuestsContainerChanged;

	UPROPERTY(BlueprintAssignable, Category = "EventDispatchers")
	FFGPActiveQuestsContainerChanged OnActiveQuestsContainerChanged;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void AddQuestToLog(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	int GetActiveQuestsMaxCount() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetActiveQuestsMaxCount(const int& MaxCount);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Quest System")
	void ActiveQuestMaxCountChanged(const int& MaxCount);

	/**
	 * Add a given quest if possible to the active quest container
	 */
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	bool MakeQuestActiveIfPossible(AFGPQuest* Quest);

	/**
	 * Helper function to remove a quest from the active quests container,
	 * and update its state to complete or inactive, depending on the value
	 * of AFGPQuest::IsComplete() which MUST BE SET BEFORE calling this function
	 */
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	bool RemoveQuestFromActiveIfPossible(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	static bool RemoveQuestFromActiveIfPossibleStatic(AFGPPlayerControllerBase* Player, AFGPQuest* Quest);

	/**
	 * Helper function to remove a quest from any quest container,
	 * if quest was claimed and not completed.
	 */
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	bool AbandonQuestIfPossible(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void CheckQuestCompletionForCollectedItems(AFGPQuest* Quest);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void MainQuestsContainerChangedEvent();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void ActiveQuestsContainerChangedEvent();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void NotifyInventoryItemChanged(bool bAdded, UFGPItem* Item);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	static bool DoesQuestLogContainQuest(AFGPPlayerControllerBase* Player, AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	static void BroadcastEnemyKillToQuests(AFGPPlayerControllerBase* Player, TSubclassOf<AActor> EnemyKilled);

private:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Quest System", meta=(AllowPrivateAccess="true"))
	int ActiveQuestsMaxCount;

};
