/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPOnScreenActiveQuestsListUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPOnScreenActiveQuestsListUMG.generated.h"

class UFGPQuestLog;
class UFGPActiveQuestListEntryUMG;

//Engine
class UVerticalBox;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPOnScreenActiveQuestsListUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* VerticalBox_QuestList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	TSubclassOf<UFGPActiveQuestListEntryUMG> QuestItemUMGLayout;
	
protected:
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateElements();

private:
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetDefaultValuesToElements();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	UFGPQuestLog* QuestLog;
};
