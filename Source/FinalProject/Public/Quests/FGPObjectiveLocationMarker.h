/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPObjectiveLocationMarker.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FGPObjectiveLocationMarker.generated.h"

class USphereComponent;
class UPrimitiveComponent;
class AFGPCharacterBase;
class UFGPQuestLog;

UCLASS(Category = "Quest System")
class FINALPROJECT_API AFGPObjectiveLocationMarker
	: public AActor
{
	GENERATED_BODY()
	
public:	
	// Constructor
	AFGPObjectiveLocationMarker(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	USphereComponent* TriggerArea;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	FText LocationName;

protected:
	
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION(BlueprintNativeEvent, Category = Components)
	void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:

// 	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
// 	AFGPCharacterBase* PlayerCharacter;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AFGPCharacterBase> PlayerCharacterActorClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	UFGPQuestLog* QuestLog;
};
