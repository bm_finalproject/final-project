/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPMainQuestLabelUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPMainQuestLabelUMG.generated.h"

class AFGPQuest;
class UFGPQuestLog;

class UTextBlock;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPMainQuestLabelUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_QuestName;


protected:
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	AFGPQuest* GetQuest() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuest(AFGPQuest* NewQuest);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateQuestElements();

	UFUNCTION()
	void GetLatestMainQuestFromQuestLog(AFGPQuest* NewQuest);

private:
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetDefaultValuesToQuestElements();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	UFGPQuestLog* QuestLog;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	AFGPQuest* QuestInDisplay;

};
