/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestDescriptionUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPQuestDescriptionUMG.generated.h"

class AFGPQuest;
class UFGPQuestLog;
class UFGPActiveQuestEntryObjectiveUMG;

class UButton;
class UTextBlock;
class UScrollBox;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPQuestDescriptionUMG
	: public UUserWidget
{
	GENERATED_BODY()

	public:
	// Binding widgets to custom native logic

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* Quest_Description;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UScrollBox* ScrollBox_Objectives;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	AFGPQuest* QuestInformation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	TSubclassOf<UFGPActiveQuestEntryObjectiveUMG> ObjectiveItemUMGLayout;

protected:
	virtual void NativeConstruct() override;
	
public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	AFGPQuest* GetQuestInformation() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestInformation(AFGPQuest* NewQuest);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestInformationChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateQuestDialogElements();

private:
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetDefaultValuesToQuestInformationElements();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	UFGPQuestLog* QuestLog;
	
};
