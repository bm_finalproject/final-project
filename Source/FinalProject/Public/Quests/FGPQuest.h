/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuest.h
 * Description	: Base class for all quest info containers
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FGPQuestTypes.h"
#include "FGPQuest.generated.h"

class UFGPQuestLog;
class AFGPObjectiveLocationMarker;
class AActor;
class UFGPItem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFGPLocationReached, AFGPObjectiveLocationMarker*, Location);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFGPInteractedWithTarget, AActor*, InteractedTarget);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFGPCollectedItem, UFGPItem*, ItemCollected);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFGPQuestObjectivesChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFGPKilledTarget, TSubclassOf<AActor>, KilledTarget);


UCLASS(Category = "Quest System", hidecategories = ("Actor", "Actor Tick", "Input", "LOD", "Rendering", "Replication", "Cooking"))
class FINALPROJECT_API AFGPQuest
	: public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFGPQuest(const FObjectInitializer& ObjectInitializer);

	/** Name of current quest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	FText QuestName;

	/** The type of quest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	EFGPQuestTypeEnum QuestType;

	/** The state of the quest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	EFGPQuestStatesEnum QuestState;

	/** Quest description */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (MultiLine = true))
	FText QuestDescription;

	/** Quest description */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (MultiLine = true))
	FText QuestTurnInDialog;

	/** List of objectives */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	TArray<FFGPObjectiveData> QuestObjectives;

	/** Pre-requisite */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	AFGPQuest* PreRequisite;
	
	UPROPERTY(BlueprintAssignable, Category = "Quest System")
	FFGPLocationReached LocationReachedDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Quest System")
	FFGPInteractedWithTarget InteractedWithTargetDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Quest System")
	FFGPCollectedItem CollectedItemDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Quest System")
	FFGPKilledTarget KilledTargetDelegate;

	UPROPERTY(BlueprintAssignable, Category = "Quest System")
	FFGPQuestObjectivesChanged QuestObjectivesChangedDelegate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	FText GetQuestName() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	EFGPQuestTypeEnum GetQuestType() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	EFGPQuestStatesEnum GetQuestState() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	FText GetQuestDescription() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void GetQuestObjectives(TArray<FFGPObjectiveData>& ObjectiveList) const;

	/**
	 * Helper event that can be called from a function made in a blueprinted subclass
	 * which can be set to be called from the editor as a button
	 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Quest System")
	void AlignQuestWithParentActor();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Quest System")
	void UpdateThisActorOnEditor();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	UFGPQuestLog* GetQuestLog() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestLog(UFGPQuestLog* QuestLog);

	UFUNCTION(BlueprintNativeEvent, Category = "Quest System")
	void QuestLogChanged(UFGPQuestLog* QuestLog);

	UFUNCTION(BlueprintGetter, Category = "Quest System")
	bool IsCompleted() const;

	UFUNCTION(BlueprintSetter, Category = "Quest System")
	void SetIsCompleted(const bool& _bIsCompleted);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void RevertToInitialState();

	UFUNCTION(BlueprintNativeEvent, Category = "Quest System")
	void IsCompletedChanged(bool& _bIsCompleted);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void LocationReachedEvent(AFGPObjectiveLocationMarker* Location);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void InteractedWithObjectiveTargetEvent(AActor* InteractedTarget);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void CollectedItemEvent(UFGPItem* ItemCollected);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void KilledTargetEvent(TSubclassOf<AActor> InteractedTarget);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	static bool IsQuestCompletedArchived(AFGPQuest* Quest);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	static bool IsQuestCompletedNotYetArchived(AFGPQuest* Quest);

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

private:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	bool bIsCompleted;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Quest System", meta =(AllowPrivateAccess="true"))
	UFGPQuestLog* QuestLogComponent;

};
