/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPActiveQuestListEntryUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPActiveQuestListEntryUMG.generated.h"

class AFGPQuest;
class UFGPActiveQuestEntryObjectiveUMG;

//Engine
class UTextBlock;
class UVerticalBox;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPActiveQuestListEntryUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_QuestName;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UVerticalBox* VerticalBox_Objectives;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	AFGPQuest* QuestInformation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	TSubclassOf<UFGPActiveQuestEntryObjectiveUMG> ObjectiveItemUMGLayout;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	FSlateColor MainQuestTextColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	FSlateColor SideQuestTextColor;

protected:
	virtual void NativeConstruct() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	AFGPQuest* GetQuestInformation() const;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestInformation(AFGPQuest* NewQuest);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestInformationChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateQuestDialogElements();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestObjectivesChangedEvent();

private:
	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetDefaultValuesToQuestInformationElements();

// 	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
// 	UFGPQuestLog* QuestLog;

};
