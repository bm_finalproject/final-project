/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLogQuestListEntryUMG.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Blueprint/UserWidget.h"
#include "FGPQuestLogQuestListEntryUMG.generated.h"

//API Forward declarations
class AFGPQuest;
class UFGPQuestLogUMG;
//Engine forward declarations
class UTextBlock;
class UButton;
class UButtonWidgetStyle;
class USlateWidgetStyleAsset;
class UCheckBox;

/**
 * 
 */
UCLASS()
class FINALPROJECT_API UFGPQuestLogQuestListEntryUMG
	: public UUserWidget
{
	GENERATED_BODY()
	
public: //UPROPERTIES
	
	/** The quest actor to which this widget is concerned with */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	AFGPQuest* QuestInformation;

	/** The text widget that displays the quest name */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_QuestName;

	/** A interactive button */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_QuestEntry;

	/** Toggle button for making this quest active or removing it from active quests */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_QuestActiveToggle;

	/** Placeholder for when has no quest information */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UTextBlock* TextBlock_Placeholder;

	/** Checkbox for marking active quests */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UCheckBox* CheckBox_IsQuestActive;

	/** Widget style for main quest entries */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	USlateWidgetStyleAsset* MainQuestStyle;

	/** Widget style for side quest entries */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (ExposeOnSpawn = "true"))
	USlateWidgetStyleAsset* SideQuestStyle;
	
protected:
	virtual void NativeConstruct() override;

public: //METHODS

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateElements();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void SetQuestInformation(AFGPQuest* NewQuest, UFGPQuestLogUMG* EntryContainer);

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void UpdateCheckBoxIfQuestActive();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	bool MakeQuestActiveIfPossible();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	bool RemoveQuestFromActiveIfPossible();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void ToggleActiveStateIfPossible();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Quest System")
	void QuestInformationChanged();

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	void DisplayQuestInformation();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Quest System", meta=(AllowPrivateAccess="true"))
	UFGPQuestLogUMG* QuestLogUMGContainer;
};