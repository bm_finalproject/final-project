/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestTypes.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FGPQuestTypes.generated.h"

class AActor;
class UFGPItem;

//namespace EFGPQuestSystem {

UENUM(BlueprintType)
enum class EFGPQuestObjectiveTypesEnum : uint8
{
	QOT_Location		UMETA(DisplayName = "Location"),	/** Visit a location */
	QOT_Interact		UMETA(DisplayName = "Interact"),	/** Interact with a given actor */
	QOT_Collect			UMETA(DisplayName = "Collect"),		/** Collect something */
	QOT_Kill			UMETA(DisplayName = "Kill"),		/** Kill something/someone */

	//256th entry
	QOT_MAX				UMETA(Hidden)
};

UENUM(BlueprintType)
enum class EFGPQuestStatesEnum : uint8
{
	QS_Unclaimed	UMETA(DisplayName = "Unclaimed"),		/** Quest was not claimed by the player */
	QS_Active		UMETA(DisplayName = "Active"),			/** Quest was accepted and is currently active */
	QS_Inactive		UMETA(DisplayName = "Inactive"),		/** Quest was accepted and but is not active */
	QS_Completed	UMETA(DisplayName = "Completed"),		/** Quest was accepted and completed */

	//256th entry
	QS_MAX			UMETA(Hidden)
};

UENUM(BlueprintType)
enum class EFGPQuestTypeEnum : uint8
{
	QT_MainQuest	UMETA(DisplayName = "MainQuest"),		/** Quest is main quest, main story related */
	QT_SideQuest	UMETA(DisplayName = "SideQuest"),		/** Quest is a side quest, being optional */

	//256th entry
	QT_MAX			UMETA(Hidden)
};

/**
	* Struct defining the details about a quest specific objective
	*/
USTRUCT(BlueprintType, Category = "Quest System")
struct FINALPROJECT_API FFGPObjectiveData
{
	GENERATED_BODY()

public:
	FFGPObjectiveData()
	{
		Description = FText::FromString("Something for you to do");
		Type = EFGPQuestObjectiveTypesEnum::QOT_Location;
		bIsComplete = false;
		Target = nullptr;
		TargetToKill = nullptr;
		ItemToCollect = nullptr;
		Amount = 1;
		AmountCurrent = 0;
	}

	/** What the objective is all about */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	FText Description;

	/** The type of the current objective */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	EFGPQuestObjectiveTypesEnum Type;

	/** If the objective is complete */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	bool bIsComplete;

	/** The objective's target actor (NPC, enemy) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	AActor* Target;

	/** The objective's target actor to kill (enemy) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	TSubclassOf<AActor> TargetToKill;

	/** The objective's target item */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	UFGPItem* ItemToCollect;

	/** The number of times needed to perform the objectives action in order for it to be completed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	int Amount;

	/** The number of times needed to perform the objectives action in order for it to be completed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	int AmountCurrent;

	/** Equality operators */
	bool operator==(const FFGPObjectiveData& Other) const
	{
		return (Description.IdenticalTo(Other.Description)
			&& Type == Other.Type
			&& bIsComplete == Other.bIsComplete
			&& Target == Other.Target
			&& TargetToKill == Other.TargetToKill
			&& ItemToCollect == Other.ItemToCollect
			&& Amount == Other.Amount);
	}
	bool operator!=(const FFGPObjectiveData& Other) const
	{
		return !(*this == Other);
	}

	static FORCEINLINE FName PropertyNameType()
	{
		return FName("Type");
	};

	static FORCEINLINE FName PropertyNameTarget()
	{
		return FName("Target");
	};

	static FORCEINLINE FName PropertyNameTargetToKill()
	{
		return FName("TargetToKill");
	};

	static FORCEINLINE FName PropertyNameItemToCollect()
	{
		return FName("ItemToCollect");
	};

	static FORCEINLINE FName PropertyNameDescription()
	{
		return FName("Description");
	};
};