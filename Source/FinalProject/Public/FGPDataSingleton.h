/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPDataSingleton.h
 * Description	: Singleton base class for accessing persistent data, such as
 *		references to static types of blueprinted classes and more.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FGPDataSingleton.generated.h"

class UFGPQuestLog;
class UFGPInteractionComponent;

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class FINALPROJECT_API UFGPDataSingleton : public UObject
{
	GENERATED_BODY()
	
public:
	UFGPDataSingleton(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FGP Classes")
	TSubclassOf<UFGPQuestLog> QuestLogComponent_BPC;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FGP Classes")
	TSubclassOf<UFGPInteractionComponent> InteractionComponent_BPC;
};
