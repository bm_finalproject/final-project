/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPGenderComponent.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "FGPTypes.h"

// Engine
#include "Components/ActorComponent.h"
#include "FGPGenderComponent.generated.h"



UCLASS(Blueprintable, BlueprintType, Category = "FGP Character Gender", ClassGroup = ("FGP Character Gender"), meta = (BlueprintSpawnableComponent), hidecategories = ("ComponentTick", "Tags", "Activation", "Cooking", "ComponentReplication", "Variable", "AssetUserData", "Collision"))
class FINALPROJECT_API UFGPGenderComponent
	: public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFGPGenderComponent(const FObjectInitializer& ObjectInitializer);

	/** The component's owner gender  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Gender", BlueprintSetter = SetGender, BlueprintGetter = GetGender)
	ECharacterGender Gender;

	/** Delegate called when gender is changed */
	UPROPERTY(BlueprintAssignable, Category = "FGP Character Gender")
	FOnGenderChanged OnGenderChanged;

	/** Native version of above delegate, called before BP delegate */
	FOnGenderChangedNative OnGenderChangedNative;

protected:

public:	
	
	UFUNCTION(BlueprintGetter)
	ECharacterGender GetGender() const;

	UFUNCTION(BlueprintSetter)
	void SetGender(const ECharacterGender& SelectedGender);

	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	bool IsFemale();
	
	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	void ToggleGender();

	void NotifyOnGenderChanged(const ECharacterGender& SelectedGender);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Gender")
	void GenderChangedNative(const ECharacterGender& SelectedGender);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "FGP Character Gender")
	void GenderChanged(const ECharacterGender& SelectedGender);


	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	static FName GetGenderNameStatic(const ECharacterGender& SelectedGender);
	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	static FString GetGenderNameStringStatic(const ECharacterGender& SelectedGender);
	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	FName GetGenderName();
	UFUNCTION(BlueprintCallable, Category = "FGP Character Gender")
	FString GetGenderNameString();
};
