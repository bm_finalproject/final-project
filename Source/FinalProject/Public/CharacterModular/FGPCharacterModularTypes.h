/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCharacterModularTypes.h
 * Description	: Custom types and structures for defining modular characters
 *			and customize material parameters
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "Engine/DataTable.h"
#include "FGPCharacterModularTypes.generated.h"

class USkeletalMesh;
class UDataTable;


/**
 * Material Instance parameter names
 */
UENUM(BlueprintType)
enum class ETextureParameter : uint8
{
	SkinColor					UMETA(DisplayName = "SkinColor"),				/** SkinColor */
	HairColor					UMETA(DisplayName = "HairColor"),				/** HairColor */
	FacialHair					UMETA(DisplayName = "FacialHair"),				/** FacialHair */
	FacialHairStubbleColor		UMETA(DisplayName = "FacialHairStubbleColor"),	/** FacialHairStubbleColor */
	TatooColor					UMETA(DisplayName = "TatooColor"),				/** TatooColor */
	BodyMainColor				UMETA(DisplayName = "BodyMainColor"),			/** BodyMainColor */
	BodySecondaryColor			UMETA(DisplayName = "BodySecondaryColor"),		/** BodySecondaryColor */
	BodyTertiaryColor			UMETA(DisplayName = "BodyTertiaryColor"),		/** BodyTertiaryColor */
	LeatherColor				UMETA(DisplayName = "LeatherColor"),			/** LeatherColor */
	LeatherColor2				UMETA(DisplayName = "LeatherColor2"),			/** LeatherColor2 */
	MetalColor					UMETA(DisplayName = "MetalColor"),				/** MetalColor */
	MetalColor2					UMETA(DisplayName = "MetalColor2"),				/** MetalColor2 */
	MetalColor3					UMETA(DisplayName = "MetalColor3"),				/** MetalColor3 */

	//256th entry
	TP_MAX						UMETA(Hidden)
};

/**
 * Body parts enumeration
 */
UENUM(BlueprintType)
enum class EBodyParts : uint8
{
	PreMade				UMETA(DisplayName = "PreMade"),			/** PreMade */
	Head				UMETA(DisplayName = "Head"),			/** Head */
	Ears				UMETA(DisplayName = "Ears"),			/** Ears */
	Hair				UMETA(DisplayName = "Hair"),			/** Hair */
	Eyebrow				UMETA(DisplayName = "Eyebrow"),			/** Eyebrow */
	FacialHair			UMETA(DisplayName = "FacialHair"),		/** FacialHair */
	Torso				UMETA(DisplayName = "Torso"),			/** Torso */
	ArmUpper			UMETA(DisplayName = "ArmUpper"),		/** ArmUpper */
	ArmLower			UMETA(DisplayName = "ArmLower"),		/** ArmLower */
	Hand				UMETA(DisplayName = "Hand"),			/** Hand */
	Hips				UMETA(DisplayName = "Hips"),			/** Hips */
	Leg					UMETA(DisplayName = "Leg"),				/** Leg */

	//256th entry
	BP_MAX						UMETA(Hidden)
};

/**
* Struct defining parameter name and respective linear color parameter value
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_ColorStructure
{
	GENERATED_BODY()

	public:
	FFGPChrParts_ColorStructure()
	{
		ParameterName = ETextureParameter::SkinColor;
		ParameterValue = FLinearColor();
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	ETextureParameter ParameterName;

	/** The linear color value */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FLinearColor ParameterValue;

	/** Equality operators */
	bool operator==(const FFGPChrParts_ColorStructure& Other) const
	{
		return (ParameterName == Other.ParameterName
			&& ParameterValue == Other.ParameterValue);
	}
	bool operator!=(const FFGPChrParts_ColorStructure& Other) const
	{
		return !(*this == Other);
	}

	FString GetParameterName_String()
	{
		FString ParameterName_String = "";
		switch (ParameterName)
		{
		case ETextureParameter::SkinColor: { ParameterName_String = "SkinColor"; break; }
		case ETextureParameter::HairColor: { ParameterName_String = "HairColor"; break; }
		case ETextureParameter::FacialHair: { ParameterName_String = "FacialHair"; break; }
		case ETextureParameter::FacialHairStubbleColor: { ParameterName_String = "FacialHairStubbleColor"; break; }
		case ETextureParameter::TatooColor: { ParameterName_String = "TatooColor"; break; }
		case ETextureParameter::BodyMainColor: { ParameterName_String = "BodyMainColor"; break; }
		case ETextureParameter::BodySecondaryColor: { ParameterName_String = "BodySecondaryColor"; break; }
		case ETextureParameter::BodyTertiaryColor: { ParameterName_String = "BodyTertiaryColor"; break; }
		case ETextureParameter::LeatherColor: { ParameterName_String = "LeatherColor"; break; }
		case ETextureParameter::LeatherColor2: { ParameterName_String = "LeatherColor2"; break; }
		case ETextureParameter::MetalColor: { ParameterName_String = "MetalColor"; break; }
		case ETextureParameter::MetalColor2: { ParameterName_String = "MetalColor2"; break; }
		case ETextureParameter::MetalColor3: { ParameterName_String = "MetalColor3"; break; }
		default: 
		{
			break;
		}
		}

		return ParameterName_String;
	};
	
	FName GetParameterName()
	{
		return FName(*GetParameterName_String());
	};
};

/**
* Struct defining body color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_BodyTextureColorStructure : public FTableRowBase
{
	GENERATED_BODY()

public:
	FFGPChrParts_BodyTextureColorStructure()
	{
		SkinColor.ParameterName = ETextureParameter::SkinColor;
		SkinColor.ParameterValue = FLinearColor(1.000000, 0.590619, 0.407240, 1.000000);

		HairColor.ParameterName = ETextureParameter::HairColor;
		HairColor.ParameterValue = FLinearColor(0.054480, 0.038204, 0.014444, 1.000000);

		FacialHair.ParameterName = ETextureParameter::FacialHair;
		FacialHair.ParameterValue = FLinearColor(0.054480, 0.038204, 0.014444, 1.000000);

		FacialHairStubbleColor.ParameterName = ETextureParameter::FacialHairStubbleColor;
		FacialHairStubbleColor.ParameterValue = FLinearColor(0.617207, 0.439657, 0.351533, 1.000000);

		TatooColor.ParameterName = ETextureParameter::TatooColor;
		TatooColor.ParameterValue = FLinearColor(0.059511, 0.127438, 0.391572, 1.000000);
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure SkinColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure HairColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure FacialHair;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure FacialHairStubbleColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure TatooColor;

	TArray<FFGPChrParts_ColorStructure> GetColorArray()
	{
		TArray<FFGPChrParts_ColorStructure> Colors;
		Colors.Add(SkinColor);
		Colors.Add(HairColor);
		Colors.Add(FacialHair);
		Colors.Add(FacialHairStubbleColor);
		Colors.Add(TatooColor);
		return Colors;
	};
};

/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_EquipmentTextureColorStructure : public FTableRowBase
{
	GENERATED_BODY()

public:
	FFGPChrParts_EquipmentTextureColorStructure()
	{
		BodyMainColor.ParameterName = ETextureParameter::BodyMainColor;
		BodyMainColor.ParameterValue = FLinearColor(0.068478, 0.130136, 0.205079, 1.000000);

		BodySecondaryColor.ParameterName = ETextureParameter::BodySecondaryColor;
		BodySecondaryColor.ParameterValue = FLinearColor(0.520996, 0.323143, 0.082283, 1.000000);

		BodyTertiaryColor.ParameterName = ETextureParameter::BodyTertiaryColor;
		BodyTertiaryColor.ParameterValue = FLinearColor(0.162029, 0.031896, 0.024158, 1.000000);

		LeatherColor.ParameterName = ETextureParameter::LeatherColor;
		LeatherColor.ParameterValue = FLinearColor(0.063010, 0.034340, 0.021219, 1.000000);

		LeatherColor2.ParameterName = ETextureParameter::LeatherColor2;
		LeatherColor2.ParameterValue = FLinearColor(0.520996, 0.323143, 0.082283, 1.000000);

		MetalColor.ParameterName = ETextureParameter::MetalColor;
		MetalColor.ParameterValue = FLinearColor(0.262251, 0.274677, 0.287441, 1.000000);

		MetalColor2.ParameterName = ETextureParameter::MetalColor2;
		MetalColor2.ParameterValue = FLinearColor(0.097587, 0.111932, 0.127438, 1.000000);

		MetalColor3.ParameterName = ETextureParameter::MetalColor3;
		MetalColor3.ParameterValue = FLinearColor(0.027321, 0.031896, 0.036889, 1.000000);
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure BodyMainColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure BodySecondaryColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure BodyTertiaryColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure LeatherColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure LeatherColor2;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure MetalColor;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure MetalColor2;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_ColorStructure MetalColor3;

	TArray<FFGPChrParts_ColorStructure> GetColorArray()
	{
		TArray<FFGPChrParts_ColorStructure> Colors;
		Colors.Add(BodyMainColor);
		Colors.Add(BodySecondaryColor);
		Colors.Add(BodyTertiaryColor);
		Colors.Add(LeatherColor);
		Colors.Add(LeatherColor2);
		Colors.Add(MetalColor);
		Colors.Add(MetalColor2);
		Colors.Add(MetalColor3);
		return Colors;
	};
};

/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_MaleFemaleMeshes : public FTableRowBase
{
	GENERATED_BODY()

public:
	FFGPChrParts_MaleFemaleMeshes()
	{
		Male = nullptr;
		Female = nullptr;
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	USkeletalMesh* Male;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	USkeletalMesh* Female;

	bool operator==(const FFGPChrParts_MaleFemaleMeshes& Other) const
	{
		return (Male == Other.Male
			&& Female == Other.Female);
	}
	bool operator!=(const FFGPChrParts_MaleFemaleMeshes& Other) const
	{
		return !(*this == Other);
	}
};


/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_BodyPartsHead
{
	GENERATED_BODY()

public:
	FFGPChrParts_BodyPartsHead()
	{
		Head = nullptr;
		Ears = nullptr;
		Hair = nullptr;
		Eyebrow = nullptr;
		FacialHair = nullptr;
	}
	
	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
	USkeletalMesh* Head;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
	USkeletalMesh* Ears;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
	USkeletalMesh* Hair;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
	USkeletalMesh* Eyebrow;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
	USkeletalMesh* FacialHair;

	bool operator==(const FFGPChrParts_BodyPartsHead& Other) const
	{
		return (Head == Other.Head
			&& Ears == Other.Ears
			&& Hair == Other.Hair
			&& Eyebrow == Other.Eyebrow
			&& FacialHair == Other.FacialHair);
	}
	bool operator!=(const FFGPChrParts_BodyPartsHead& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;
		if (Head != nullptr) meshes.Add(Head);
		if (Ears != nullptr) meshes.Add(Ears);
		if (Hair != nullptr) meshes.Add(Hair);
		if (Eyebrow != nullptr) meshes.Add(Eyebrow);
		if (FacialHair != nullptr) meshes.Add(FacialHair);
		return meshes;
	}

	void UpdateBodyPart(const EBodyParts& BodyPartToUpdate, USkeletalMesh* Mesh)
	{
		switch (BodyPartToUpdate)
		{
		case EBodyParts::Head:
		{
			Head = Mesh;
			break;
		}
		case EBodyParts::Ears:
		{
			Ears = Mesh;
			break;
		}
		case EBodyParts::Hair:
		{
			Hair = Mesh;
			break;
		}
		case EBodyParts::Eyebrow:
		{
			Eyebrow = Mesh;
			break;
		}
		case EBodyParts::FacialHair:
		{
			FacialHair = Mesh;
			break;
		}
		default:
		{
			break;
		}
		}
	}
};

/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_BodyPartsUpper
{
	GENERATED_BODY()

public:
	FFGPChrParts_BodyPartsUpper()
	{
		Torso = nullptr;
		ArmUpper = nullptr;
		ArmLower = nullptr;
		Hand = nullptr;
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
	USkeletalMesh* Torso;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
	USkeletalMesh* ArmUpper;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
	USkeletalMesh* ArmLower;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
	USkeletalMesh* Hand;

	bool operator==(const FFGPChrParts_BodyPartsUpper& Other) const
	{
		return (Torso == Other.Torso
			&& ArmUpper == Other.ArmUpper
			&& ArmLower == Other.ArmLower
			&& Hand == Other.Hand);
	}
	bool operator!=(const FFGPChrParts_BodyPartsUpper& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;
		if (Torso != nullptr) meshes.Add(Torso);
		if (ArmUpper != nullptr) meshes.Add(ArmUpper);
		if (ArmLower != nullptr) meshes.Add(ArmLower);
		if (Hand != nullptr) meshes.Add(Hand);
		return meshes;
	}

	void UpdateBodyPart(const EBodyParts& BodyPartToUpdate, USkeletalMesh* Mesh)
	{
		switch (BodyPartToUpdate)
		{
		case EBodyParts::Torso:
		{
			Torso = Mesh;
			break;
		}
		case EBodyParts::ArmUpper:
		{
			ArmUpper = Mesh;
			break;
		}
		case EBodyParts::ArmLower:
		{
			ArmLower = Mesh;
			break;
		}
		case EBodyParts::Hand:
		{
			Hand = Mesh;
			break;
		}
		default:
		{
			break;
		}
		}
	}
};



/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_BodyPartsLower
{
	GENERATED_BODY()

public:
	FFGPChrParts_BodyPartsLower()
	{
		Hips = nullptr;
		Leg = nullptr;
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Lower")
	USkeletalMesh* Hips;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Lower")
	USkeletalMesh* Leg;

	bool operator==(const FFGPChrParts_BodyPartsLower& Other) const
	{
		return (Hips == Other.Hips
			&& Leg == Other.Leg);
	}
	bool operator!=(const FFGPChrParts_BodyPartsLower& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;
		if (Hips != nullptr) meshes.Add(Hips);
		if (Leg != nullptr) meshes.Add(Leg);
		return meshes;
	}

	void UpdateBodyPart(const EBodyParts& BodyPartToUpdate, USkeletalMesh* Mesh)
	{
		switch (BodyPartToUpdate)
		{
		case EBodyParts::Hips:
		{
			Hips = Mesh;
			break;
		}
		case EBodyParts::Leg:
		{
			Leg = Mesh;
			break;
		}
		default:
		{
			break;
		}
		}
	}
};

/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_BodyParts
{
	GENERATED_BODY()

public:
	FFGPChrParts_BodyParts()
	{

	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Head")
		FFGPChrParts_BodyPartsHead HeadParts;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
		FFGPChrParts_BodyPartsUpper BodyUpperParts;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Lower")
		FFGPChrParts_BodyPartsLower BodyLowerParts;

	bool operator==(const FFGPChrParts_BodyParts& Other) const
	{
		return (HeadParts == Other.HeadParts
			&& BodyUpperParts == Other.BodyUpperParts
			&& BodyLowerParts == Other.BodyLowerParts);
	}
	bool operator!=(const FFGPChrParts_BodyParts& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;

		TArray<USkeletalMesh*> headMeshes = HeadParts.GetMeshes();
		TArray<USkeletalMesh*> bodyUpperMeshes = BodyUpperParts.GetMeshes();
		TArray<USkeletalMesh*> bodyLowerMeshes = BodyLowerParts.GetMeshes();

		if (headMeshes.Num() > 0) meshes.Append(headMeshes);
		if (bodyUpperMeshes.Num() > 0) meshes.Append(bodyUpperMeshes);
		if (bodyLowerMeshes.Num() > 0) meshes.Append(bodyLowerMeshes);

		return meshes;
	}

	void UpdateBodyPart(const EBodyParts& BodyPartToUpdate, USkeletalMesh* Mesh)
	{
		switch (BodyPartToUpdate)
		{
		case EBodyParts::Head:
		case EBodyParts::Ears:
		case EBodyParts::Hair:
		case EBodyParts::Eyebrow:
		case EBodyParts::FacialHair:
		{
			HeadParts.UpdateBodyPart(BodyPartToUpdate, Mesh);
			break;
		}
		case EBodyParts::Torso:
		case EBodyParts::ArmUpper:
		case EBodyParts::ArmLower:
		case EBodyParts::Hand:
		{
			BodyUpperParts.UpdateBodyPart(BodyPartToUpdate, Mesh);
			break;
		}
		case EBodyParts::Hips:
		case EBodyParts::Leg:
		{
			BodyLowerParts.UpdateBodyPart(BodyPartToUpdate, Mesh);
			break;
		}
		case EBodyParts::PreMade:
		default:
		{
			break;
		}
		}
	}
};


/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_EquipmentAttachment
{
	GENERATED_BODY()

public:
	FFGPChrParts_EquipmentAttachment()
	{
		Attachment = nullptr;
		Location = FVector::ZeroVector;
	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Attachment")
	USkeletalMesh* Attachment;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Body Upper")
	FVector Location;

	bool operator==(const FFGPChrParts_EquipmentAttachment& Other) const
	{
		return (Attachment == Other.Attachment
			&& Location == Other.Location);
	}
	bool operator!=(const FFGPChrParts_EquipmentAttachment& Other) const
	{
		return !(*this == Other);
	}
};


/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_EquipmentAttachments
{
	GENERATED_BODY()

public:
	FFGPChrParts_EquipmentAttachments()
	{

	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment HeadCoverings_NoFacialHair;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment HeadCoverings_BaseHair;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment HeadCoverings_NoHair_Helmet;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment HelmetAttachment;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment ElbowAttach;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment HipsAttachment;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment KneeAttach;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment ShoulderAttach;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attachments")
	FFGPChrParts_EquipmentAttachment BackAttachment;

	bool operator==(const FFGPChrParts_EquipmentAttachments& Other) const
	{
		return (HeadCoverings_NoFacialHair == Other.HeadCoverings_NoFacialHair
			&& HeadCoverings_BaseHair == Other.HeadCoverings_BaseHair
			&& HeadCoverings_NoHair_Helmet == Other.HeadCoverings_NoHair_Helmet
			&& HelmetAttachment == Other.HelmetAttachment
			&& ElbowAttach == Other.ElbowAttach
			&& HipsAttachment == Other.HipsAttachment
			&& KneeAttach == Other.KneeAttach
			&& ShoulderAttach == Other.ShoulderAttach
			&& BackAttachment == Other.BackAttachment);
	}
	bool operator!=(const FFGPChrParts_EquipmentAttachments& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;

		if (HeadCoverings_NoFacialHair.Attachment != nullptr) meshes.Add(HeadCoverings_NoFacialHair.Attachment);
		if (HeadCoverings_BaseHair.Attachment != nullptr) meshes.Add(HeadCoverings_BaseHair.Attachment);
		if (HeadCoverings_NoHair_Helmet.Attachment != nullptr) meshes.Add(HeadCoverings_NoHair_Helmet.Attachment);
		if (HelmetAttachment.Attachment != nullptr) meshes.Add(HelmetAttachment.Attachment);
		if (ElbowAttach.Attachment != nullptr) meshes.Add(ElbowAttach.Attachment);
		if (HipsAttachment.Attachment != nullptr) meshes.Add(HipsAttachment.Attachment);
		if (KneeAttach.Attachment != nullptr) meshes.Add(KneeAttach.Attachment);
		if (ShoulderAttach.Attachment != nullptr) meshes.Add(ShoulderAttach.Attachment);
		if (BackAttachment.Attachment != nullptr) meshes.Add(BackAttachment.Attachment);

		return meshes;
	}
};




/**
* Struct defining equipment color parameters and respective values for mesh material
*/
USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_AllParts
{
	GENERATED_BODY()

public:
	FFGPChrParts_AllParts()
	{

	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_BodyParts BodyParts;

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_EquipmentAttachments EquipmentAttachments;

	bool operator==(const FFGPChrParts_AllParts& Other) const
	{
		return (BodyParts == Other.BodyParts
			&& EquipmentAttachments == Other.EquipmentAttachments);
	}
	bool operator!=(const FFGPChrParts_AllParts& Other) const
	{
		return !(*this == Other);
	}

	TArray<USkeletalMesh*> GetMeshes()
	{
		TArray<USkeletalMesh*> meshes;

		TArray<USkeletalMesh*> bodyMeshes = BodyParts.GetMeshes();
		TArray<USkeletalMesh*> attachmentMeshes = EquipmentAttachments.GetMeshes();

		if (bodyMeshes.Num() > 0) meshes.Append(bodyMeshes);
		if (attachmentMeshes.Num() > 0) meshes.Append(attachmentMeshes);

		return meshes;
	}

	void UpdateBodyPart(const EBodyParts& BodyPartToUpdate, USkeletalMesh* Mesh)
	{
		switch (BodyPartToUpdate)
		{
		case EBodyParts::Head:
		case EBodyParts::Ears:
		case EBodyParts::Hair:
		case EBodyParts::Eyebrow:
		case EBodyParts::FacialHair:
		case EBodyParts::Torso:
		case EBodyParts::ArmUpper:
		case EBodyParts::ArmLower:
		case EBodyParts::Hand:
		case EBodyParts::Hips:
		case EBodyParts::Leg:
		{
			BodyParts.UpdateBodyPart(BodyPartToUpdate, Mesh);
			break;
		}
		case EBodyParts::PreMade:
		default:
		{
			break;
		}
		}
	}
};


template <typename T>
static FORCEINLINE bool GetRowByName(UDataTable* DataTable, FName RowName, T& Out, bool bDisplayWarnings = false)
{
	bool bWasSuccessful = false;
	if (DataTable == nullptr) return bWasSuccessful;

	T* data = DataTable->FindRow<T>(RowName, "", bDisplayWarnings);
	if (data == nullptr)
	{
		bWasSuccessful = false;
		//Out = T();
	}
	else {
		bWasSuccessful = true;
		Out = *data;
	}
	return bWasSuccessful;
}

/** Delegate called when a gender property changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnIsFemaleChanged, bool, bIsFemale);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnIsFemaleChangedNative, bool);

/** Delegate called when a isModular property changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnIsModularChanged, bool, bIsModular);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnIsModularChangedNative, bool);

/** Delegate to be called when the character skeletal mesh property changes */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSkeletalMeshChanged);
DECLARE_MULTICAST_DELEGATE(FOnSkeletalMeshChangedNative);


USTRUCT(BlueprintType, Category = "FGP Character Modular Parts")
struct FINALPROJECT_API FFGPChrParts_Wrapper
{
	GENERATED_BODY()

public:
	FFGPChrParts_Wrapper()
		: MeshPreview(nullptr)
		, MatInstanceDynamic(nullptr)
	{

	}

	/** The material parameter name */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	USkeletalMesh* MeshPreview;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FDataTableRowHandle BodyPart;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	UMaterialInstanceDynamic* MatInstanceDynamic;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FFGPChrParts_EquipmentTextureColorStructure EquipmentColors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts")
	FDataTableRowHandle EquipmentColorSet;
};