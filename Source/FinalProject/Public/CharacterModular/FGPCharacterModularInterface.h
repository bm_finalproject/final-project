/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCharacterModularInterface.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "Object.h"
#include "FinalProject.h"
#include "FGPCharacterModularTypes.h"

#include "Engine/DataTable.h"
#include "FGPCharacterModularInterface.generated.h"

class UFGPGenderComponent;
class UMaterialInstanceDynamic;
class USkeletalMeshComponent;

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class FINALPROJECT_API UFGPModularManagerInterface : public UInterface
{
	GENERATED_BODY()
};

/** Interface for actors that expose access to a character modular component */
class FINALPROJECT_API IFGPModularManagerInterface
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	bool UpdateCharacterMesh();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	UMaterialInstanceDynamic* CreateMaterialInstanceDynamic(USkeletalMeshComponent* SelectedMesh);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	FFGPChrParts_BodyTextureColorStructure ApplyBodyColorsToMeshComponent(
		const FName& BodyPartName
		, USkeletalMeshComponent* MeshComponentSelected
		, UMaterialInstanceDynamic* MatInstanceDynamic
		, const FFGPChrParts_BodyTextureColorStructure& ColorBody
		, FDataTableRowHandle ColorBodyDataTableHandle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	FFGPChrParts_EquipmentTextureColorStructure ApplyEquipmentColorsToMeshComponent(
			const FName& BodyPartName
			, USkeletalMeshComponent* MeshComponentSelected
			, UMaterialInstanceDynamic* MatInstanceDynamic
			, const FFGPChrParts_EquipmentTextureColorStructure& ColorBody
			, FDataTableRowHandle ColorBodyDataTableHandle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetHeadMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetEarsMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetHairMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetEyebrowMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetFacialHairMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetTorsoMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetArmUpperMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetArmLowerMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetHandMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetHipsMeshComponent();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character Modular")
	USkeletalMeshComponent* GetLegMeshComponent();
};
