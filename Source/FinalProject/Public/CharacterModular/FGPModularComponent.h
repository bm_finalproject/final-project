/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPModularComponent.h
 * Description	: An actor component class that gives modular character properties
 *				and functionalities to its owner.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "CharacterModular/FGPCharacterModularTypes.h"
#include "FGPCharacterModularInterface.h"
#include "FGPGenderInterface.h"

 // Engine
#include "Components/ActorComponent.h"
#include "FGPModularComponent.generated.h"


UCLASS(Blueprintable
	, BlueprintType
	, Category = "FGP Character Modular"
	, ClassGroup = ("FGP Character Modular")
	, meta = (BlueprintSpawnableComponent)
	, hidecategories = ("ComponentTick"
		, "Tags", "Activation", "Cooking"
		, "ComponentReplication", "Variable"
		, "AssetUserData", "Collision"))
class FINALPROJECT_API UFGPModularComponent
	: public UActorComponent
	, public IFGPModularManagerInterface
	, public IFGPGenderInterface
{
	GENERATED_BODY()

public:	
	UFGPModularComponent(const FObjectInitializer& ObjectInitializer);

	/** A list with every body part name */
	UPROPERTY(BlueprintReadWrite, Category = "FGP Character Modular | Parts")
	TArray<FName> BodyPartsNames;

	/** A boolean to inform if the character should use template sets or customized colors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular | Colors")
	bool bShouldModifyBodyColors;

	/** Color structure for the body parts */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular | Colors")
	FFGPChrParts_BodyTextureColorStructure BodyColors;

	/** Table row handle for selecting template color structures to be applied to body parts */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular | Colors")
	FDataTableRowHandle MatInst_BodyParameters;

	/** Premade material instances interfaces for color reference */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FGP Character Modular | Colors")
	TArray<UMaterialInstance*> MatInstances;

	/** The material parameter name */
// 	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "FGP Character Modular Parts | Colors")
// 	UMaterialInstance* MaterialInstance_OriginalPreview;

	/** Map containing body part wrapper structure for every body part (skeletal mesh, colors and material instances) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FGP Character Modular | Parts")
	TMap<FName, FFGPChrParts_Wrapper> BodyParts;

protected:
	virtual void BeginPlay() override;

public:

	/** Updates the component's owner character model */
	UFUNCTION(BlueprintCallable, Category = "FGP Character Modular | Parts")
	bool UpdateOwnerCharacterMesh();

	/**
	 * Blueprint function that adds a dynamic material instance to an entry of our body part map.
	 */
	UFUNCTION(BlueprintCallable, Category = "FGP Character Modular | Parts")
	void AddDynamicMaterialInstanceToBodyPartsMap(
			const FName& BodyPart
			, UMaterialInstanceDynamic* MatInstanceDynamic);

private:

	/** For a given body part name, selects a mesh from its respective data table */
	void SelectMeshFromDataTable(const FName& BodyPartToUpdate);

	/** Updates every skeletal mesh component of the component's owner with the current body part selection */
	void UpdateOwnerEveryMeshComponent();
	
	/** Updates a specific skeletal mesh component of the component's owner with the current body part selection */
	void UpdateOwnerMeshComponent(const FName& BodyPartToUpdate);

	/** Helper function to get the body part FName from the body part name container, via an index */
	FName GetBodyPartNameByIndex(const int32& Index);

public:
	// INTERFACE overrides
	/** Calls for the instance UpdateOwnerEveryMeshComponent method */
	virtual bool UpdateCharacterMesh_Implementation() override;

	/** 
	 * Request the execution of a method with the same signature by the component's owner.
	 * Expects the return of a pointer to the onwer gender component
	 */
	virtual UFGPGenderComponent* GetGenderComponent_Implementation() override;

	/** Does not apply here - implementation on character blueprint*/
	virtual UMaterialInstanceDynamic* CreateMaterialInstanceDynamic_Implementation(
		USkeletalMeshComponent* SelectedMesh) override;
	/** Does not apply here */
	virtual FFGPChrParts_BodyTextureColorStructure ApplyBodyColorsToMeshComponent_Implementation(
		const FName& BodyPartName
		, USkeletalMeshComponent* MeshComponentSelected
		, UMaterialInstanceDynamic* MatInstanceDynamic
		, const FFGPChrParts_BodyTextureColorStructure& ColorBody
		, FDataTableRowHandle ColorBodyDataTableHandle) override;

	/** Does not apply here - implementation on character blueprint*/
	virtual FFGPChrParts_EquipmentTextureColorStructure ApplyEquipmentColorsToMeshComponent_Implementation(
		const FName& BodyPartName, USkeletalMeshComponent* MeshComponentSelected
		, UMaterialInstanceDynamic* MatInstanceDynamic
		, const FFGPChrParts_EquipmentTextureColorStructure& ColorBody
		, FDataTableRowHandle ColorBodyDataTableHandle) override;	//implementation on character blueprint

	virtual USkeletalMeshComponent* GetHeadMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetEarsMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetHairMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetEyebrowMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetFacialHairMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetTorsoMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetArmUpperMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetArmLowerMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetHandMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetHipsMeshComponent_Implementation() override;
	virtual USkeletalMeshComponent* GetLegMeshComponent_Implementation() override;

};
