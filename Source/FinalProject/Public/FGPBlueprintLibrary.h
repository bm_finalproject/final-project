/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPBlueprintLibrary.h
 * Description	: Game-specific blueprint function library
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "FGPTypes.h"
#include "FGPCharacterModularTypes.h"
#include "Abilities/FGPAbilityTypes.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FGPBlueprintLibrary.generated.h"

class UAnimMontage;
class UFGPDataSingleton;

//class UAnimMontage;

/**
 * Game-specific blueprint library
 * Exposing native code to blueprints
 */
UCLASS()
class FINALPROJECT_API UFGPBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_UCLASS_BODY()
	
public:
	/**
	 * Show the native loading screen, such as on a map transfer.
	 * If bPlayUntilStopped is false, it will be displayed for PlayTime and automatically stop.
	 */
	UFUNCTION(BlueprintCallable, Category = Loading)
		static void PlayLoadingScreen(bool bPlayUntilStopped, float PlayTime);

	/** Turns off the native loading screen if it is visible. This must be called if bPlayUntilStopped was true */
	UFUNCTION(BlueprintCallable, Category = Loading)
		static void StopLoadingScreen();

	/** Returns true if this is being run from an editor preview */
	UFUNCTION(BlueprintPure, Category = Loading)
		static bool IsInEditor();

	/** Equality operator for ItemSlot */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "Equal (RPGItemSlot)", CompactNodeTitle = "==", Keywords = "== equal"), Category = Inventory)
	static bool EqualEqual_FGPItemSlot(const FFGPItemSlot& A, const FFGPItemSlot& B);

	/** Inequality operator for ItemSlot */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "NotEqualEqual (RPGItemSlot)", CompactNodeTitle = "!=", Keywords = "!= not equal"), Category = Inventory)
	static bool NotEqual_FGPItemSlot(const FFGPItemSlot& A, const FFGPItemSlot& B);

	/** Validity check for ItemSlot */
	UFUNCTION(BlueprintPure, Category = Inventory)
	static bool IsValidItemSlot(const FFGPItemSlot& ItemSlot);

	/** Checks if spec has any effects */
	UFUNCTION(BlueprintPure, Category = Ability)
	static bool DoesEffectContainerSpecHaveEffects(const FFGPGameplayEffectContainerSpec& ContainerSpec);

	/** Checks if spec has any targets */
	UFUNCTION(BlueprintPure, Category = Ability)
	static bool DoesEffectContainerSpecHaveTargets(const FFGPGameplayEffectContainerSpec& ContainerSpec);

	/** Adds targets to a copy of the passed in effect container spec and returns it */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "HitResults,TargetActors"))
	static FFGPGameplayEffectContainerSpec AddTargetsToEffectContainerSpec(
		const FFGPGameplayEffectContainerSpec& ContainerSpec,
		const TArray<FHitResult>& HitResults,
		const TArray<AActor*>& TargetActors);

	/** Applies container spec that was made from an ability */
	UFUNCTION(BlueprintCallable, Category = Ability)
	static TArray<FActiveGameplayEffectHandle> ApplyExternalEffectContainerSpec(const FFGPGameplayEffectContainerSpec& ContainerSpec);

	/** Get all sections by name from anim montage */
	UFUNCTION(BlueprintCallable, Category = Animation)
	static TArray<FName> GetAllSectionsFromAnimationMontage(UAnimMontage* Montage);

	/** Get a sections by name from anim montage */
	UFUNCTION(BlueprintCallable, Category = Animation)
	static FName GetNameFromSection(UAnimMontage* Montage, int sectionId);

	/** Get DataSingleton instance */
	UFUNCTION(BlueprintPure, Category = "FGP Data Singleton")
	static UFGPDataSingleton* GetDataSingleton(bool& IsValid);

	UFUNCTION(BlueprintPure, Category = "FGP Character Modular Parts")
	static void GetColorParameterInfo(const FFGPChrParts_ColorStructure& Color,
		const FName& BodyPartName, FName& ReturnParameterName, FLinearColor& ReturnParameterValue);

	UFUNCTION(BlueprintPure, Category = "FGP Character Modular Parts")
	static void GetBodyColorArray(const FFGPChrParts_BodyTextureColorStructure& Colors,
			TArray<FFGPChrParts_ColorStructure>& ReturnBodyColors);

	UFUNCTION(BlueprintPure, Category = "FGP Character Modular Parts")
	static void GetEquipmentColorArray(const FFGPChrParts_EquipmentTextureColorStructure& Colors,
			TArray<FFGPChrParts_ColorStructure>& ReturnEquipmentColors);
};
