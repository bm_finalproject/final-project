// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "GameFramework/GameStateBase.h"
#include "FGPGameStateBase.generated.h"

/**
 * Base class for GameState, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API AFGPGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	AFGPGameStateBase() {}

};
