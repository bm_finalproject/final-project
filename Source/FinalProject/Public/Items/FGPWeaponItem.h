// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/FGPItem.h"
#include "FGPWeaponItem.generated.h"

/**
 * Native base class for weapons, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API UFGPWeaponItem
	: public UFGPItem
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	UFGPWeaponItem()
	{
		ItemType = UFGPAssetManager::WeaponItemType;
	}
	
	/** Weapon actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<AActor> WeaponActor;
};
