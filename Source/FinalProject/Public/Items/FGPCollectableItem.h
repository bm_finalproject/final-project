/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCollectableItem.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "Items/FGPItem.h"
#include "FGPCollectableItem.generated.h"

class UStaticMesh;

/**
 * Native base class for weapons, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API UFGPCollectableItem
	: public UFGPItem
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	UFGPCollectableItem()
	{
		ItemType = UFGPAssetManager::CollectableItemType;
	}

	/** Weapon actor to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "3D Model")
	UStaticMesh* StaticMesh;
};
