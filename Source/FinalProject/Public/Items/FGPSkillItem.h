// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/FGPItem.h"
#include "FGPSkillItem.generated.h"

/**
 * Native base class for skills, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API UFGPSkillItem
	: public UFGPItem
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	UFGPSkillItem()
	{
		ItemType = UFGPAssetManager::SkillItemType;
	}
};
