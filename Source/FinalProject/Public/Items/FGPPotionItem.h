// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/FGPItem.h"
#include "FGPPotionItem.generated.h"

/**
 * Native base class for potions, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API UFGPPotionItem
	: public UFGPItem
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	UFGPPotionItem()
	{
		ItemType = UFGPAssetManager::PotionItemType;
	}
};
