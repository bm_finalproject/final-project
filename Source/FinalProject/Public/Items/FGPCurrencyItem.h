// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Items/FGPItem.h"
#include "FGPCurrencyItem.generated.h"

/**
 * Native base class for currency, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API UFGPCurrencyItem
	: public UFGPItem
{
	GENERATED_BODY()
	
public:
	/** Constructor */
	UFGPCurrencyItem()
	{
		ItemType = UFGPAssetManager::CurrencyItemType;
		MaxCount = 0; // Infinite
	}
};
