/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPGenderInterface.h
 * Description	: A interface for giving classes methods to interact with gender
 *				actor components
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "Object.h"
#include "FinalProject.h"
#include "FGPGenderInterface.generated.h"

class UFGPGenderComponent;

// This class does not need to be modified.
UINTERFACE(Blueprintable)
class FINALPROJECT_API UFGPGenderInterface : public UInterface
{
	GENERATED_BODY()
};

/** Interface for actors that expose access to a gender component */
class FINALPROJECT_API IFGPGenderInterface
{
	GENERATED_BODY()
	
public:

	/** Returns the gender component to use for this actor. It may live on another actor, such as a Pawn using the PlayerState's component */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Character | Gender")
	UFGPGenderComponent* GetGenderComponent();
};