/*******************************************************************
Bachelor of Games and Apps Development
Universidade Europeia
Lisboa
Portugal

(c) 2019 Universidade Europeia

File Name	: FGPGameMode.h
Description	: 
Author		: Bruno Matos
Mail		: bruno.rosal.matos@gmail.com
********************************************************************/

#pragma once


#include "GameFramework/GameModeBase.h"
#include "FGPGameMode.generated.h"

/**
 * Base class for GameMode, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API AFGPGameMode : public AGameModeBase
{
	GENERATED_UCLASS_BODY()
	
public:


private:


protected:
	virtual void BeginPlay() override;

};
