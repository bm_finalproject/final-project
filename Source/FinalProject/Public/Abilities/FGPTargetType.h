/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPTargetType.h
 * Description	: Signature of class that is used to determine targeting
 *					for abilities.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "GameplayAbilities/Public/Abilities/GameplayAbilityTypes.h"
#include "Abilities/FGPAbilityTypes.h"
#include "FGPTargetType.generated.h"

class AFGPCharacterBase;
class AActor;
class FGamepleplayEventData;



// /** Structure to define info about targeting information */
// USTRUCT(BlueprintType)
// struct FINALPROJECT_API FTargetInfo
// {
// 	GENERATED_BODY()
// 
// public:
// 	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Target Info")
// 	AFGPCharacterBase* TargetingCharacter;
// 
// 	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Target Info")
// 	AActor* TargetingActor;
// 
// 	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Target Info")
// 	FGameplayEventData EventData;
// 
// 	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Target Info")
// 	TArray<FHitResult> OutHitResults;
// 
// 	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Target Info")
// 	TArray<AActor*> OutActors;
// 
// 	FTargetInfo() { }
// };

/**
 * Class that is used to determine targeting for abilities.
 * It is meant to be blueprinted to run target logic.
 * This does not subclass GameplayAbilityTargetActor because this class is never
 * instanced into the world.
 * This can be used as a basis for a game-specific targeting blueprint.
 * If targeting is complicated we may need to instance into the world once or as
 * a pooled actor.
 */
UCLASS(Blueprintable, meta = (ShowWorldContextPin))
class FINALPROJECT_API UFGPTargetType
	: public UObject
{
	GENERATED_BODY()
	
public:
	// Constructor and overrides
	UFGPTargetType() {};

	/** Called to determine targets to apply gameplay effects to */
	UFUNCTION(BlueprintNativeEvent)
	void GetTargets(AFGPCharacterBase* TargetingCharacter
		, AActor* TargetingActor
		, FGameplayEventData EventData
		, TArray<FHitResult>& OutHitResults
		, TArray<AActor*>& OutActors) const;

};

/** Target type that uses the owner */
UCLASS(NotBlueprintable)
class FINALPROJECT_API UFGPTargetType_UseOwner
	: public UFGPTargetType
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	UFGPTargetType_UseOwner() {};

	/** Uses the passed in event data */
	virtual void GetTargets_Implementation(AFGPCharacterBase* TargetingCharacter
		, AActor* TargetingActor
		, FGameplayEventData EventData
		, TArray<FHitResult>& OutHitResults
		, TArray<AActor*>& OutActors) const override;

};

/** Target type that pulls the target out of the event data */
UCLASS(NotBlueprintable)
class FINALPROJECT_API UFGPTargetType_UseEventData
	: public UFGPTargetType
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	UFGPTargetType_UseEventData() {};

	/** Uses the passed in event data */
	virtual void GetTargets_Implementation(AFGPCharacterBase* TargetingCharacter
		, AActor* TargetingActor
		, FGameplayEventData EventData
		, TArray<FHitResult>& OutHitResults
		, TArray<AActor*>& OutActors) const override;

};