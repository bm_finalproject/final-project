// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Abilities/FGPAbilityTypes.h"

// Engine
#include "GameplayAbilities/Public/Abilities/GameplayAbility.h"
#include "Runtime/GameplayTags/Classes/GameplayTagContainer.h"
#include "FGPGameplayAbility.generated.h"

/**
 * Subclass of ability blueprint type with game-specific data
 * This class uses GameplayEffectContainers to allow easier execution of
 * gameplay effects based on a triggering tag
 */
UCLASS()
class FINALPROJECT_API UFGPGameplayAbility :
	public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	//Constructor and overrides
	UFGPGameplayAbility();

	/** Map of gameplay tags to gameplay effect containers */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = GameplayEffects)
	TMap<FGameplayTag, FFGPGameplayEffectContainer> EffectContainerMap;

	/**
	 * Make gameplay effect container spec to be applied later, using the
	 * passed in container 
	 */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual FFGPGameplayEffectContainerSpec MakeEffectContainerSpecFromContainer(
		const FFGPGameplayEffectContainer& Container
		, const FGameplayEventData& EventData
		, int32 OverrideGameplayLevel = -1);

	/**
	 * Search for and make a gameplay effect container spec to be applied
	 * later, from the EffectContainerMap
	 */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual FFGPGameplayEffectContainerSpec MakeEffectContainerSpec(
		FGameplayTag ContainerTag
		, const FGameplayEventData& EventData
		, int32 OverrideGameplayLevel = -1);

	/**
	 * Applies a gameplay effect container spec that was previously created
	 */
	UFUNCTION(BlueprintCallable, Category = Ability)
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainerSpec(
		const FFGPGameplayEffectContainerSpec& ContainerSpec);

	/** 
	 * Applies a gameplay effect container, by creating and then applying the
	 * spec 
	 */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (AutoCreateRefTerm = "EventData"))
	virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainer(
		FGameplayTag ContainerTag
		, const FGameplayEventData& EventData
		, int32 OverrideGameplayLevel = -1);
};
