// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Abilities/FGPAbilityTypes.h"

// Engine
#include "GameplayAbilities/Public/AbilitySystemComponent.h"
#include "FGPAbilitySystemComponent.generated.h"

class UFGPGameplayAbility;

/**
 * Subclass of ability system component with game-specific data
 * Provides utility functions
 */
UCLASS()
class FINALPROJECT_API UFGPAbilitySystemComponent
	: public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	// Constructors and overrides
	UFGPAbilitySystemComponent();

	/** Returns a list of currently active ability instances that match the tags */
	void GetActiveAbilitiesWithTags(
		const FGameplayTagContainer& GameplayTagContainer
		, TArray<UFGPGameplayAbility*>& ActiveAbilities);

	/** Returns the default level used for ability activations, derived from the character */
	int32 GetDefaultAbilityLevel() const;

	/** Version of function in AbilitySystemGlobals that returns correct type */
	static UFGPAbilitySystemComponent* GetAbilitySystemComponentFromActor(
		const AActor* Actor
		, bool LookForComponent = false);
};
