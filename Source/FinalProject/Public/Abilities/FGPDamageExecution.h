// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "FGPDamageExecution.generated.h"

/**
 * A damage execution, which allows doing damage by combining a raw Damage number
 * with AttackPower and DefensePower
 */
UCLASS()
class FINALPROJECT_API UFGPDamageExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:
	// Constructor
	UFGPDamageExecution();
	
	virtual void Execute_Implementation(
		const FGameplayEffectCustomExecutionParameters& ExecutionParams
		, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

};
