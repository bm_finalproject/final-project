/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPAbilityTypes.h
 * Description	: Structs for GameplayEffects.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "GameplayAbilities/Public/GameplayEffectTypes.h"
#include "GameplayAbilities/Public/Abilities/GameplayAbilityTargetTypes.h"
#include "FGPAbilityTypes.generated.h"

//class UFGPAbilitySystemComponent;
class UGameplayEffect;
class UFGPTargetType;

/**
 * Struct defining a list of gameplay effects, a tag, and targeting info.
 * These containers are defined statically in blueprints or assets and then turn
 * into Specs at runtime.
 */
USTRUCT(BlueprintType)
struct FINALPROJECT_API FFGPGameplayEffectContainer
{
	GENERATED_BODY()
	
public:
	FFGPGameplayEffectContainer() { }
	
	/** Sets the way that targeting happens */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameplayEffectContainer)
	TSubclassOf<UFGPTargetType> TargetType;

	/** Sets the way that targeting happens */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameplayEffectContainer)
	TArray<TSubclassOf<UGameplayEffect>> TargetGameplayEffectClasses;
};

/**
 * A processed version of FGPGameplayEffectContainer that can be passed around
 * and eventually applied.
 */
USTRUCT(BlueprintType)
struct FINALPROJECT_API FFGPGameplayEffectContainerSpec
{
	GENERATED_BODY()
	
public:
	FFGPGameplayEffectContainerSpec() { }
	
	/** Computed target data */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = GameplayEffectContainer)
	FGameplayAbilityTargetDataHandle TargetData;

	/** List of gameplay effects to apply to the target */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = GameplayEffectContainer)
	TArray<FGameplayEffectSpecHandle> TargetGameplayEffectSpecs;

	/** Returns true if this has any valid effect specs */
	bool HasValidEffects() const;

	/** Returns true if this has any valid targets */
	bool HasValidTargets() const;

	/** Adds new targets to target data */
	void AddTargets(const TArray<FHitResult>& HitResults
		, const TArray<AActor*>& TargetActors);

};