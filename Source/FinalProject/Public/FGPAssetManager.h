/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPAssetManager.h
 * Description	: Game implementation of asset manager, overrides functionality
 *					and stores game-specific types.
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once

#include "Engine/AssetManager.h"
#include "FGPAssetManager.generated.h"

class UFGPItem;

/**
 * Game implementation of asset manager, overrides functionality and stores
 * game-specific types.
 * Good place for game-specific loading logic.
 * Change AssetManagerClassName to this class in DefaultEngine.ini
 */
UCLASS()
class FINALPROJECT_API UFGPAssetManager
	: public UAssetManager
{
	GENERATED_BODY()
	
public:
	// Constructor and overrides
	UFGPAssetManager() {}

	/** Static types for items */
	static const FPrimaryAssetType PotionItemType;
	static const FPrimaryAssetType SkillItemType;
	static const FPrimaryAssetType CurrencyItemType;
	static const FPrimaryAssetType CollectableItemType;
	static const FPrimaryAssetType WeaponItemType;

	/** Return the current AssetManager object */
	static UFGPAssetManager& Get();

	/**
	 * Synchronously loads an RPGItem subclass, this can hitch but is useful
	 * when we cannot wait for an async load.
	 * This does not maintain a reference to the item so it will garbage
	 * collect if not loaded some other way.
	 *
	 * @param PrimaryAssetId The asset identifier to load
	 * @param bDisplayWarning If true, this will log a warning if the
	 *			item failed to load
	 */
	UFGPItem* ForceLoadItem(
		const FPrimaryAssetId& PrimaryAssetId
		, bool bLogWarning = true);
};
