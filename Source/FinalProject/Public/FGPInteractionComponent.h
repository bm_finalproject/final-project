/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPInteractionComponent.h
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#pragma once

#include "FinalProject.h"
#include "FGPInteractionInterface.h"
#include "Components/ActorComponent.h"
#include "FGPInteractionComponent.generated.h"

class UFGPQuestLog;
class AFGPPlayerControllerBase;
class UFGPItem;


UCLASS(Blueprintable, BlueprintType, Category = "FGP Interaction", ClassGroup = ("FGP Interaction"), meta = (BlueprintSpawnableComponent))
class FINALPROJECT_API UFGPInteractionComponent
	: public UActorComponent, public IFGPInteractionInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFGPInteractionComponent(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:

	UFUNCTION(BlueprintCallable, Category = "FGP Interaction")
	void BroadcastToQuestsForInteractEvents();

	UFUNCTION(BlueprintCallable, Category = "FGP Interaction")
	void BroadcastToQuestsForCollectedItemEvents(UFGPItem* ItemCollected);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "FGP Interaction")
	void InteractEvent(AActor* OtherActor);
	virtual void InteractEvent_Implementation(AActor* OtherActor) override;

private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	UFGPQuestLog* QuestLog;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Quest System", meta = (AllowPrivateAccess = "true"))
	AFGPPlayerControllerBase* PlayerController;
};
