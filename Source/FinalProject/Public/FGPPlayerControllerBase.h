/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPPlayerControllerBase.h
 * Description	: Base class for PlayerController
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#pragma once


#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "FGPTypes.h"
#include "FGPInventoryInterface.h"
#include "FGPPlayerControllerBase.generated.h"

class UFGPQuestLog;
class UFGPDataSingleton;

/**
 * Base class for PlayerController, should be blueprinted
 */
UCLASS()
class FINALPROJECT_API AFGPPlayerControllerBase
	: public APlayerController
	, public IFGPInventoryInterface
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	AFGPPlayerControllerBase(const FObjectInitializer& ObjectInitializer);
	virtual void BeginPlay() override;

	/** Quest log component for tracking player progress */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quest System")
	UFGPQuestLog* QuestLogComponent;

	/** Map of all items owned by this player, from definition to data */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	TMap<UFGPItem*, FFGPItemData> InventoryData;

	/**
	 * Map of slot, from type/num to item, initialized from ItemSlotsPerType
	 * on FGPGameInstanceBase
	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	TMap<FFGPItemSlot, UFGPItem*> SlottedItems;

	/** Delegate called when an inventory item has been added or removed */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FOnInventoryItemChanged OnInventoryItemChanged;
	
	/** Native version of above delegate, called before BP delegate */
	FOnInventoryItemChangedNative OnInventoryItemChangedNative;

	/** Delegate called when an inventory slot has changed */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FOnSlottedItemChanged OnSlottedItemChanged;

	/** Native version of above delegate, called before BP delegate */
	FOnSlottedItemChangedNative OnSlottedItemChangedNative;

	/** Delegate called when the inventory has been loaded/reloaded */
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FOnInventoryLoaded OnInventoryLoaded;

	/** Native version of above delegate, called before BP delegate */
	FOnInventoryLoadedNative OnInventoryLoadedNative;

	UFUNCTION(BlueprintCallable, Category = "Quest System")
	UFGPQuestLog* GetQuestLogComponent() const;

// 	/** Called right after we have possessed a pawn */
// 	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "BeginPossessPawn"))
// 	void ReceivePossess(APawn* newPawn);
// 
// 	/** Called right before unpossessing a pawn */
// 	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "EndPossessPawn"))
// 	void ReceiveUnPossess(APawn* previousPawn);

	/**
	 * Adds a new inventory item, will add it to an empty slot if possible.
	 * If the item supports count we can add more than one count. It will also
	 * update the level when adding if required.
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool AddInventoryItem(UFGPItem* NewItem, int32 ItemCount = 1,
		int32 ItemLevel = 1, bool bAutoSlot = true);

	/**
	 * Remove an inventory item, will also remove from slots.
	 * A remove count of <= 0 means to remove all copies.
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool RemoveInventoryItem(UFGPItem* RemovedItem, int32 RemoveCount = 1);

	/**
	 * Returns all inventory items of a given type. If none is passed as type,
	 * it will return all.
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void GetInventoryItems(TArray<UFGPItem*>& Items,FPrimaryAssetType ItemType);

	/**
	 * Returns number of instances of this item found in the inventory.
	 * This uses count from GetItemData.
	 */
	UFUNCTION(BlueprintPure, Category = Inventory)
	int32 GetInventoryItemCount(UFGPItem* Item) const;
	
	/**
	 * Returns the item data associated with an item.
	 * Returns false if none found
	 */
	UFUNCTION(BlueprintPure, Category = Inventory)
	bool GetInventoryItemData(UFGPItem* Item, FFGPItemData& ItemData) const;

	/**
	 * Sets slot to item, will remove from other slots if necessary.
	 * If passing null this will empty the slot.
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool SetSlottedItem(FFGPItemSlot ItemSlot, UFGPItem* Item);

	/** Returns item in slot, or null if empty */
	UFUNCTION(BlueprintPure, Category = Inventory)
	UFGPItem* GetSlottedItem(FFGPItemSlot ItemSlot) const;

	/** Returns all slotted items of a given type. If none is passed as type it will return all */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void GetSlottedItems(TArray<UFGPItem*>& Items, FPrimaryAssetType ItemType, bool bOutputEmptyIndexes);

	/** Fills in any empty slots with items in inventory */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	void FillEmptySlots();

	/**
	 * Manually save the inventory, this is called from add/remove functions
	 * automatically
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool SaveInventory();

	/**
	 * Loads inventory from save game on game instance,
	 * this will replace arrays.
	 */
	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool LoadInventory();

	// Implement IFGPInventoryInterface
	virtual const TMap<UFGPItem*, FFGPItemData>& GetInventoryDataMap()
		const override
	{
		return InventoryData;
	}
	virtual const TMap<FFGPItemSlot, UFGPItem *>& GetSlottedItemMap()
		const override
	{
		return SlottedItems;
	}
	virtual FOnInventoryItemChangedNative& GetInventoryItemChangedDelegate()
		override
	{
		return OnInventoryItemChangedNative;
	}
	virtual FOnSlottedItemChangedNative& GetSlottedItemChangedDelegate()
		override
	{
		return OnSlottedItemChangedNative;
	}
	virtual FOnInventoryLoadedNative& GetInventoryLoadedDelegate()
		override
	{
		return OnInventoryLoadedNative;
	}

protected:
	/** Auto slots a specific item, returns true if anything changed */
	bool FillEmptySlotWithItem(UFGPItem* NewItem);

	/** Calls the inventory update callbacks */
	void NotifyInventoryItemChanged(bool bAdded, UFGPItem* Item);
	void NotifySlottedItemChanged(FFGPItemSlot ItemSlot, UFGPItem* Item);
	void NotifyInventoryLoaded();

private:
	UPROPERTY()
	UFGPDataSingleton* DataSingleton;
};
