/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPBlueprintLibrary.cpp
 * Description	: Game-specific blueprint function library
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "FGPBlueprintLibrary.h"
#include "Animation/AnimMontage.h"
#include "FinalProject.h"
#include "FGPDataSingleton.h"

UFGPBlueprintLibrary::UFGPBlueprintLibrary(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void UFGPBlueprintLibrary::PlayLoadingScreen(bool bPlayUntilStopped, float PlayTime)
{
	//IFGPLoadingScreenModule& LoadingScreenModule = IFGPLoadingScreenModule::Get();
	//LoadingScreenModule.StartInGameLoadingScreen(bPlayUntilStopped, PlayTime);
}

void UFGPBlueprintLibrary::StopLoadingScreen()
{
	//IFGPLoadingScreenModule& LoadingScreenModule = IFGPLoadingScreenModule::Get();
	//LoadingScreenModule.StopInGameLoadingScreen();
}

bool UFGPBlueprintLibrary::IsInEditor()
{
	return GIsEditor;
}

bool UFGPBlueprintLibrary::EqualEqual_FGPItemSlot(const FFGPItemSlot& A, const FFGPItemSlot& B)
{
	return A == B;
}

bool UFGPBlueprintLibrary::NotEqual_FGPItemSlot(const FFGPItemSlot& A, const FFGPItemSlot& B)
{
	return A != B;
}

bool UFGPBlueprintLibrary::IsValidItemSlot(const FFGPItemSlot& ItemSlot)
{
	return ItemSlot.IsValid();
}

bool UFGPBlueprintLibrary::DoesEffectContainerSpecHaveEffects(const FFGPGameplayEffectContainerSpec& ContainerSpec)
{
	return ContainerSpec.HasValidEffects();
}

bool UFGPBlueprintLibrary::DoesEffectContainerSpecHaveTargets(const FFGPGameplayEffectContainerSpec& ContainerSpec)
{
	return ContainerSpec.HasValidTargets();
}

FFGPGameplayEffectContainerSpec UFGPBlueprintLibrary::AddTargetsToEffectContainerSpec(
	const FFGPGameplayEffectContainerSpec& ContainerSpec
	, const TArray<FHitResult>& HitResults
	, const TArray<AActor*>& TargetActors
) {
	FFGPGameplayEffectContainerSpec NewSpec = ContainerSpec;
	NewSpec.AddTargets(HitResults, TargetActors);
	return NewSpec;
}

TArray<FActiveGameplayEffectHandle> UFGPBlueprintLibrary::ApplyExternalEffectContainerSpec(
	const FFGPGameplayEffectContainerSpec& ContainerSpec
) {
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of gameplay effects
	for (const FGameplayEffectSpecHandle& SpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
	{
		if (SpecHandle.IsValid())
		{
			// iterate list of targets and apply to all
			for (TSharedPtr<FGameplayAbilityTargetData> Data : ContainerSpec.TargetData.Data)
			{
				AllEffects.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data.Get()));
			}
		}
	}
	return AllEffects;
}

TArray<FName> UFGPBlueprintLibrary::GetAllSectionsFromAnimationMontage(UAnimMontage* Montage)
{
	TArray<FName> MontageSections;
	if (Montage == NULL) return MontageSections;

	FName Name;
	int Index = -1;
	do
	{
		Name = Montage->GetSectionName((Index + 1));
		MontageSections.Add(Name);
		Index++;
	} while (Name != "None");

	return MontageSections;
}

FName UFGPBlueprintLibrary::GetNameFromSection(UAnimMontage* Montage, int sectionId)
{
	FName Name;
	if (Montage != NULL)
	{
		Name = Montage->GetSectionName(sectionId);
	}
	return Name;
}

UFGPDataSingleton* UFGPBlueprintLibrary::GetDataSingleton(bool& bIsValid)
{
	bIsValid = false;

	UObject* Instance;
	Instance = GEngine != nullptr ? GEngine->GameSingleton : nullptr;

	if (Instance == nullptr)
	{
		// if path is not correct editor might crash
		static ConstructorHelpers::FObjectFinder<UClass> DataObjectAsset(
			TEXT("Class'/Game/Dynamic/Blueprints/BP_DataSingleton.BP_DataSingleton_C'"));
		if (!DataObjectAsset.Succeeded())
		{
			LogWarning("BP_DataSingleton blueprint class not found.");
			return NULL;
		}
		//LogMsg("BP_DataSingleton blueprint class found.");
		Instance = DataObjectAsset.Object->GetDefaultObject();
	}

	UFGPDataSingleton* DataInstance = Cast<UFGPDataSingleton>(Instance);
	if (!DataInstance)
	{
		LogWarning("BP_DataSingleton Returned nullptr");
		return NULL;
	}
	if (!DataInstance->IsValidLowLevel())
	{
		LogWarning("BP_DataSingleton Not valid");
		return NULL;
	}

	bIsValid = true;
	return DataInstance;
}

void UFGPBlueprintLibrary::GetColorParameterInfo(
	const FFGPChrParts_ColorStructure& Color
	, const FName& BodyPartName
	, FName& ParameterName
	, FLinearColor& ParameterValue
) {
	FName PartName = BodyPartName;
	FFGPChrParts_ColorStructure ColorTemp = Color;

	bool bIsFacialHairRelated =
				BodyPartName.ToString().Equals("FacialHair", ESearchCase::IgnoreCase)
				&& ColorTemp.ParameterName == ETextureParameter::FacialHair;

	ParameterName = bIsFacialHairRelated ? FName("HairColor") : ColorTemp.GetParameterName();
	ParameterValue = ColorTemp.ParameterValue;
}

void UFGPBlueprintLibrary::GetBodyColorArray(
	const FFGPChrParts_BodyTextureColorStructure& Colors
	, TArray<FFGPChrParts_ColorStructure>& ReturnBodyColors
) {
	ReturnBodyColors.Empty();
	ReturnBodyColors.Add(Colors.SkinColor);
	ReturnBodyColors.Add(Colors.FacialHair);
	ReturnBodyColors.Add(Colors.FacialHairStubbleColor);
	ReturnBodyColors.Add(Colors.HairColor);
	ReturnBodyColors.Add(Colors.TatooColor);
}

void UFGPBlueprintLibrary::GetEquipmentColorArray(
	const FFGPChrParts_EquipmentTextureColorStructure& Colors
	, TArray<FFGPChrParts_ColorStructure>& ReturnEquipmentColors
) {
	ReturnEquipmentColors.Empty();
	ReturnEquipmentColors.Add(Colors.BodyMainColor);
	ReturnEquipmentColors.Add(Colors.BodySecondaryColor);
	ReturnEquipmentColors.Add(Colors.BodyTertiaryColor);
	ReturnEquipmentColors.Add(Colors.LeatherColor);
	ReturnEquipmentColors.Add(Colors.LeatherColor2);
	ReturnEquipmentColors.Add(Colors.MetalColor);
	ReturnEquipmentColors.Add(Colors.MetalColor2);
	ReturnEquipmentColors.Add(Colors.MetalColor3);
}
