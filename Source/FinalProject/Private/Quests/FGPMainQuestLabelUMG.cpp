/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPMainQuestLabelUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPMainQuestLabelUMG.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPPlayerControllerBase.h"

 // Engine
#include "Runtime/UMG/Public/Components/TextBlock.h"

void UFGPMainQuestLabelUMG::NativeConstruct()
{
	Super::NativeConstruct();

	

	// Retrieving the quest log component from the player controller
	AFGPPlayerControllerBase* PlayerController = GetOwningPlayer<AFGPPlayerControllerBase>();
	if (PlayerController != nullptr)
	{
		QuestLog = PlayerController->GetQuestLogComponent();
		if (QuestLog)
		{
			int MainQuestsCount = QuestLog->MainQuests.Num();
			if (MainQuestsCount > 0)
			{
				AFGPQuest* NewQuest = QuestLog->MainQuests[MainQuestsCount - 1];
				SetQuest(NewQuest);
			}
			QuestLog->OnMainQuestsContainerChanged.AddDynamic(this, &UFGPMainQuestLabelUMG::GetLatestMainQuestFromQuestLog);
		}
		if (!QuestInDisplay)
		{
			SetDefaultValuesToQuestElements();
		}

	}
}

AFGPQuest* UFGPMainQuestLabelUMG::GetQuest() const
{
	return QuestInDisplay;
}

void UFGPMainQuestLabelUMG::SetQuest(AFGPQuest* NewQuest)
{
	if (QuestInDisplay == NewQuest) return;
	QuestInDisplay = NewQuest;
	QuestChanged();
}

void UFGPMainQuestLabelUMG::QuestChanged_Implementation()
{
	UpdateQuestElements();
}

void UFGPMainQuestLabelUMG::UpdateQuestElements()
{
	if (!QuestInDisplay)
	{
		SetDefaultValuesToQuestElements();
		return;
	}

	if (TextBlock_QuestName && QuestInDisplay)
	{
		TextBlock_QuestName->SetText(QuestInDisplay->GetQuestName());
	}

	if (QuestInDisplay && GetVisibility() != ESlateVisibility::Visible)
	{
		SetVisibility(ESlateVisibility::Visible);
	}
}

void UFGPMainQuestLabelUMG::GetLatestMainQuestFromQuestLog(AFGPQuest* NewQuest)
{
	//LogWarning("GetLatestMainQuestFromQuestLog");
	SetQuest(NewQuest);
}

void UFGPMainQuestLabelUMG::SetDefaultValuesToQuestElements()
{
	if (!QuestInDisplay)
	{
		SetVisibility(ESlateVisibility::Collapsed);
	}

	if (TextBlock_QuestName)
	{
		TextBlock_QuestName->SetText(FText::FromString(""));

	}
}
