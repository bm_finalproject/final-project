/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPActiveQuestListEntryUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/


#include "FGPActiveQuestListEntryUMG.h"
#include "FGPQuest.h"
#include "FGPActiveQuestEntryObjectiveUMG.h"

 // Engine
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/VerticalBox.h"

void UFGPActiveQuestListEntryUMG::NativeConstruct()
{
	Super::NativeConstruct();
	UpdateQuestDialogElements();
}

AFGPQuest* UFGPActiveQuestListEntryUMG::GetQuestInformation() const
{
	return QuestInformation;
}

void UFGPActiveQuestListEntryUMG::SetQuestInformation(AFGPQuest* NewQuest)
{
	if (QuestInformation == NewQuest) return;
	QuestInformation = NewQuest;

	QuestInformation->QuestObjectivesChangedDelegate.RemoveDynamic(
		this, &UFGPActiveQuestListEntryUMG::QuestObjectivesChangedEvent);
	QuestInformation->QuestObjectivesChangedDelegate.AddDynamic(
		this, &UFGPActiveQuestListEntryUMG::QuestObjectivesChangedEvent);

	QuestInformationChanged();
}

void UFGPActiveQuestListEntryUMG::QuestInformationChanged_Implementation()
{
	UpdateQuestDialogElements();
}

void UFGPActiveQuestListEntryUMG::UpdateQuestDialogElements()
{
	if (QuestInformation == nullptr)
	{
		SetDefaultValuesToQuestInformationElements();
		return;
	}

	if (TextBlock_QuestName)
	{
		TextBlock_QuestName->SetText(QuestInformation->GetQuestName());
		FSlateColor TextColor;
		if (QuestInformation->GetQuestType() == EFGPQuestTypeEnum::QT_MainQuest)
			TextColor = MainQuestTextColor;
		else
			TextColor = SideQuestTextColor;
		TextBlock_QuestName->SetColorAndOpacity(TextColor);
	}

	if (VerticalBox_Objectives)
	{
		if (VerticalBox_Objectives->HasAnyChildren())
			VerticalBox_Objectives->ClearChildren();

		if (ObjectiveItemUMGLayout == NULL) return;

		TArray<FFGPObjectiveData> Objectives;
		QuestInformation->GetQuestObjectives(Objectives);
		for (FFGPObjectiveData ObjectiveData : Objectives)
		{
			UFGPActiveQuestEntryObjectiveUMG* ObjectiveUMG;
			ObjectiveUMG = CreateWidget<UFGPActiveQuestEntryObjectiveUMG>(GetOwningPlayer(), ObjectiveItemUMGLayout);

			if (ObjectiveUMG == NULL) continue;
			ObjectiveUMG->bIsInsideActiveQuestPanel = true;
			ObjectiveUMG->SetObjectiveData(ObjectiveData);
			VerticalBox_Objectives->AddChild(ObjectiveUMG);
		}
	}
}

void UFGPActiveQuestListEntryUMG::QuestObjectivesChangedEvent_Implementation()
{
	//LogWarning("Must update UI because objective list was updated");
	QuestInformationChanged();
}

void UFGPActiveQuestListEntryUMG::SetDefaultValuesToQuestInformationElements()
{
	if (QuestInformation == nullptr)
	{
		if (TextBlock_QuestName)
			TextBlock_QuestName->SetText(FText::FromString("Quest description will appear here."));

		if (VerticalBox_Objectives)
			if (VerticalBox_Objectives->HasAnyChildren())
				VerticalBox_Objectives->ClearChildren();
	}
}
