/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestObjectiveItemUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPQuestObjectiveItemUMG.h"
#include "Runtime/UMG/Public/Components/CheckBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

void UFGPQuestObjectiveItemUMG::NativeConstruct()
{
	Super::NativeConstruct();

	UpdateElements();
}

void UFGPQuestObjectiveItemUMG::SetObjectiveData(FFGPObjectiveData Objective)
{
	if (ObjectiveData == Objective) return;
	ObjectiveData = Objective;
	ObjectiveDataChanged();
}

void UFGPQuestObjectiveItemUMG::ObjectiveDataChanged_Implementation()
{
	UpdateElements();
}

void UFGPQuestObjectiveItemUMG::UpdateElements()
{
	if (CheckBox_IsComplete != NULL)
	{
		bool bIsComplete = ObjectiveData.bIsComplete;
		CheckBox_IsComplete->SetCheckedState(bIsComplete ? ECheckBoxState::Checked : ECheckBoxState::Unchecked);
	}

	if (TextBlock_Description != NULL)
	{
		int CurrentCount = ObjectiveData.AmountCurrent;
		int MaxCount = ObjectiveData.Amount;
		FText Description = ObjectiveData.Description;
		FText Text = Description;
		if (MaxCount > 0)
		{
			CurrentCount = CurrentCount > MaxCount ? MaxCount : CurrentCount;
			if (bIsInsideJournal)
			{
				Text = FText::FormatOrdered(
					FTextFormat::FromString("{0} - {1}/{2}"),
					Description, CurrentCount, MaxCount);
			}
			else
			{
				Text = FText::FormatOrdered(
					FTextFormat::FromString("{0} - {2}x"),
					Description, MaxCount);
			}
		}
		TextBlock_Description->SetText(Text);
	}
}
