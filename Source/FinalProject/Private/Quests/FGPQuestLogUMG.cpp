/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLogUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPQuestLogUMG.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPQuestDescriptionUMG.h"
#include "FGPQuestLogQuestListEntryUMG.h"

// Engine
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/ScrollBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/VerticalBox.h"
#include "GameFramework/PlayerController.h"

void UFGPQuestLogUMG::NativeConstruct()
{
	Super::NativeConstruct();

	
	if (Button_AbandonQuest)
	{
		Button_AbandonQuest->OnPressed.AddDynamic(this, &UFGPQuestLogUMG::AbandonQuest);
	}
	if (Button_QuestListShow)
	{
		Button_QuestListShow->OnPressed.AddDynamic(this, &UFGPQuestLogUMG::ToggleIsDisplayingCompleted);
	}
	if (Button_CompletedQuestsListShow)
	{
		Button_CompletedQuestsListShow->OnPressed.AddDynamic(this, &UFGPQuestLogUMG::ToggleIsDisplayingCompleted);
	}

	if (VerticalBox_QuestDetailsContainer)
	{
		VerticalBox_QuestDetailsContainer->SetVisibility(ESlateVisibility::Hidden);
	}
	
	UpdateElements();
}

void UFGPQuestLogUMG::UpdateElements()
{
	TArray<AFGPQuest*> MainQuests;
	TArray<AFGPQuest*> SideQuests;
	APlayerController* PController;
	int QuestsOnDisplayCount = 0;

	if (ScrollBox_QuestList)
	{
		ScrollBox_QuestList->ClearChildren();
	}

	// Get claimed quests information
	if (QuestLog)
	{
		MainQuests = QuestLog->MainQuests;
		SideQuests = QuestLog->SideQuests;
	}
	else {
		LogMsg("No quest log");

	}

	if (ScrollBox_QuestList && QuestListEntryUMGLayout && !bIsDisplayingCompleted)
	{
		PController = GetOwningPlayer();
		//LogMsg("Scrollbox and questlist entry layout are right");
		if (MainQuests.Num() > 0)
		{
			for (AFGPQuest* Quest : MainQuests)
			{
				if (AFGPQuest::IsQuestCompletedArchived(Quest)) continue;
				UFGPQuestLogQuestListEntryUMG* QuestEntryUMG;
				QuestEntryUMG = CreateWidget<UFGPQuestLogQuestListEntryUMG>(PController, QuestListEntryUMGLayout);

				if (QuestEntryUMG == NULL) continue;
				QuestEntryUMG->SetQuestInformation(Quest, this);
				ScrollBox_QuestList->AddChild(QuestEntryUMG);
				QuestsOnDisplayCount++;
				if (QuestInDisplay == nullptr)
					SetQuestToDisplay(Quest);
			}
		}

		if (SideQuests.Num() > 0)
		{
			for (AFGPQuest* Quest : SideQuests)
			{
				if (AFGPQuest::IsQuestCompletedArchived(Quest)) continue;
				UFGPQuestLogQuestListEntryUMG* QuestEntryUMG;
				QuestEntryUMG = CreateWidget<UFGPQuestLogQuestListEntryUMG>(PController, QuestListEntryUMGLayout);

				if (QuestEntryUMG == NULL) continue;
				QuestEntryUMG->SetQuestInformation(Quest, this);
				ScrollBox_QuestList->AddChild(QuestEntryUMG);
				QuestsOnDisplayCount++;
				if (QuestInDisplay == nullptr)
					SetQuestToDisplay(Quest);
			}
		}
	}
	else if (ScrollBox_QuestList && QuestListEntryUMGLayout && bIsDisplayingCompleted)
	{
		PController = GetOwningPlayer();
		//LogMsg("Scrollbox and questlist entry layout are right");
		if (MainQuests.Num() > 0)
		{
			for (AFGPQuest* Quest : MainQuests)
			{
				if (!AFGPQuest::IsQuestCompletedArchived(Quest)) continue;
				UFGPQuestLogQuestListEntryUMG* QuestEntryUMG;
				QuestEntryUMG = CreateWidget<UFGPQuestLogQuestListEntryUMG>(PController, QuestListEntryUMGLayout);

				if (QuestEntryUMG == NULL) continue;
				QuestEntryUMG->SetQuestInformation(Quest, this);
				ScrollBox_QuestList->AddChild(QuestEntryUMG);
				QuestsOnDisplayCount++;
				if (QuestInDisplay == nullptr)
					SetQuestToDisplay(Quest);
			}
		}

		if (SideQuests.Num() > 0)
		{
			for (AFGPQuest* Quest : SideQuests)
			{
				if (!AFGPQuest::IsQuestCompletedArchived(Quest)) continue;
				UFGPQuestLogQuestListEntryUMG* QuestEntryUMG;
				QuestEntryUMG = CreateWidget<UFGPQuestLogQuestListEntryUMG>(PController, QuestListEntryUMGLayout);

				if (QuestEntryUMG == NULL) continue;
				QuestEntryUMG->SetQuestInformation(Quest, this);
				ScrollBox_QuestList->AddChild(QuestEntryUMG);
				QuestsOnDisplayCount++;
				if (QuestInDisplay == nullptr)
					SetQuestToDisplay(Quest);
			}
		}
	}

	if (ScrollBox_QuestList && QuestListEntryUMGLayout)
	{
		if (MainQuests.Num() < 1 && SideQuests.Num() < 1)
		{
			LogMsg("No quests in quests lists");
			// Hide collapse quest list and disable test on hit for 'quests' button
			ScrollBox_QuestList->SetVisibility(ESlateVisibility::Collapsed);
			Button_QuestListShow->SetVisibility(ESlateVisibility::HitTestInvisible);
			Button_CompletedQuestsListShow->SetVisibility(ESlateVisibility::HitTestInvisible);
		}
		else
		{
			// Hide collapse quest list and disable test on hit for 'quests' button
			ScrollBox_QuestList->SetVisibility(QuestListBoxVisibilityState);
			if (bIsDisplayingCompleted)
			{
				Button_QuestListShow->SetVisibility(ESlateVisibility::Visible);
				Button_CompletedQuestsListShow->SetVisibility(ESlateVisibility::HitTestInvisible);
			}
			else
			{
				Button_QuestListShow->SetVisibility(ESlateVisibility::HitTestInvisible);
				Button_CompletedQuestsListShow->SetVisibility(ESlateVisibility::Visible);
			}
		}
	}
	else {
		LogMsg("Scrollbox and questlist entry layout are not right");

	}
}

void UFGPQuestLogUMG::SetQuestLog(UFGPQuestLog* NewQuestLog)
{
	if (QuestLog == NewQuestLog) return;
	QuestLog = NewQuestLog;
	if (!QuestLog)
		LogMsg("No QuestLogComponent was found");
	QuestLogChanged();
}

void UFGPQuestLogUMG::DisplayQuestDetails_Implementation(AFGPQuest* Quest)
{
	if (QuestLog == nullptr) return;
	if (Quest == nullptr) return;
	LogWarning("Selected quest for display information");

	TArray<AFGPQuest*> MainQuests = QuestLog->MainQuests;
	TArray<AFGPQuest*> SideQuests = QuestLog->SideQuests;
	int CurrQuestIndex = 0;

	if (TextBlock_Quest_Title)
	{
		TextBlock_Quest_Title->SetText(Quest->GetQuestName());
	}

	if (MainQuests.Contains(Quest))
	{
		Button_AbandonQuest->SetVisibility(ESlateVisibility::Collapsed);
	}

	if (SideQuests.Contains(Quest))
	{
		Button_AbandonQuest->SetVisibility(ESlateVisibility::Visible);
	}

	if (VerticalBox_QuestDetailsContainer)
	{
		if (VerticalBox_QuestDetailsContainer->GetVisibility() != ESlateVisibility::Visible)
		{
			VerticalBox_QuestDetailsContainer->SetVisibility(ESlateVisibility::Visible);
		}
	}
	if (WB_Quest_Description)
	{
		WB_Quest_Description->SetQuestInformation(Quest);
	}
}

void UFGPQuestLogUMG::SetQuestToDisplay(AFGPQuest* Quest)
{
	if (QuestInDisplay == Quest) return;
	QuestInDisplay = Quest;
	if (QuestInDisplay == nullptr) return;
	DisplayQuestDetails(Quest);
}

void UFGPQuestLogUMG::AbandonQuest()
{
	if (QuestLog == nullptr) return;
	if (QuestInDisplay == nullptr) return;

	LogWarning("Should abandon quest");
	AFGPQuest* NextQuest = nullptr;
	TArray<AFGPQuest*> MainQuests = QuestLog->MainQuests;
	TArray<AFGPQuest*> SideQuests = QuestLog->SideQuests;
	int CurrQuestIndex = 0;

	if (SideQuests.Contains(QuestInDisplay))
	{
		for (AFGPQuest* ItrQuest : SideQuests)
		{
			if (ItrQuest == QuestInDisplay)
				break;
			CurrQuestIndex++;
		}
		int NextIndex = CurrQuestIndex + 1;
		if (SideQuests.Num() > 1 && NextIndex < SideQuests.Num())
		{
			NextQuest = SideQuests[NextIndex];
		}
		else {
			if (MainQuests.Num() > 0)
			{
				NextQuest = MainQuests[0];
			}
		}
	}

	QuestLog->AbandonQuestIfPossible(QuestInDisplay);

	SetQuestToDisplay(NextQuest);
	UpdateElements();
}

void UFGPQuestLogUMG::ToggleIsDisplayingCompleted()
{
	SetIsDisplayingCompleted(!bIsDisplayingCompleted);
}

void UFGPQuestLogUMG::SetIsDisplayingCompleted(const bool& bIsDisplaying)
{
	if (bIsDisplayingCompleted == bIsDisplaying) return;
	bIsDisplayingCompleted = bIsDisplaying;
	IsDisplayingCompletedChanged(bIsDisplayingCompleted);
}

void UFGPQuestLogUMG::IsDisplayingCompletedChanged_Implementation(bool& bIsDisplaying)
{
	UpdateElements();
}

void UFGPQuestLogUMG::QuestLogChanged_Implementation()
{
	//LogMsg("Hello from the other side");
	UpdateElements();
}
