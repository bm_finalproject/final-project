/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPActiveQuestEntryObjectiveUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPActiveQuestEntryObjectiveUMG.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

void UFGPActiveQuestEntryObjectiveUMG::NativeConstruct()
{
	Super::NativeConstruct();
	UpdateElements();
}

void UFGPActiveQuestEntryObjectiveUMG::SetObjectiveData(FFGPObjectiveData Objective)
{
	if (ObjectiveData == Objective) return;
	ObjectiveData = Objective;
	ObjectiveDataChanged();
}

void UFGPActiveQuestEntryObjectiveUMG::ObjectiveDataChanged_Implementation()
{
	UpdateElements();
}

void UFGPActiveQuestEntryObjectiveUMG::UpdateElements()
{
	if (TextBlock_CompleteMarker)
	{
		if (ObjectiveData.bIsComplete)
		{
			TextBlock_CompleteMarker->SetText(CompletionMarkSymbol);
		}
		else
		{
			TextBlock_CompleteMarker->SetText(UnCompletedMarkSymbol);
		}

		if (bIsInsideActiveQuestPanel)
		{
			TextBlock_CompleteMarker->SetFont(TextFontInsideActiveQuestsPanel);
		}
		else
		{
			TextBlock_CompleteMarker->SetFont(TextFontInsideOtherPanels);
		}
	}

	if (TextBlock_Description != NULL)
	{
		int32 CurrentCount = ObjectiveData.AmountCurrent;
		int32 MaxCount = ObjectiveData.Amount;
		FText Description = ObjectiveData.Description;
		FText Text = Description;
		if (bIsInsideActiveQuestPanel)
		{
			TextBlock_Description->SetFont(TextFontInsideActiveQuestsPanel);
		}
		else
		{
			TextBlock_Description->SetFont(TextFontInsideOtherPanels);
		}

		if (MaxCount > 0)
		{
			CurrentCount = CurrentCount > MaxCount ? MaxCount : CurrentCount;
			if (bIsInsideQuestPrompt)
			{
				Text = FText::FormatOrdered(
					FTextFormat::FromString("{0} - {1}x"),
					Description, MaxCount);
			}
			else
			{
				Text = FText::FormatOrdered(
					FTextFormat::FromString("{0} - {1}/{2}"),
					Description, CurrentCount, MaxCount);
			}
		}
		TextBlock_Description->SetText(Text);
	}
}
