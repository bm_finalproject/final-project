/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestDescriptionUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/


#include "FGPQuestDescriptionUMG.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPPlayerControllerBase.h"
#include "FGPActiveQuestEntryObjectiveUMG.h"

 // Engine
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/ScrollBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

void UFGPQuestDescriptionUMG::NativeConstruct()
{
	Super::NativeConstruct();

	UpdateQuestDialogElements();

	// Retrieving the quest log component from the player controller
	AFGPPlayerControllerBase* PlayerController = GetOwningPlayer<AFGPPlayerControllerBase>();
	if (PlayerController != nullptr)
	{
		QuestLog = PlayerController->GetQuestLogComponent();
	}
}

AFGPQuest* UFGPQuestDescriptionUMG::GetQuestInformation() const
{
	return QuestInformation;
}

void UFGPQuestDescriptionUMG::SetQuestInformation(AFGPQuest* NewQuest)
{
	if (QuestInformation == NewQuest) return;

	QuestInformation = NewQuest;
	QuestInformationChanged();
}

void UFGPQuestDescriptionUMG::QuestInformationChanged_Implementation()
{
	UpdateQuestDialogElements();
}

void UFGPQuestDescriptionUMG::UpdateQuestDialogElements()
{
	if (QuestInformation == NULL)
	{
		SetDefaultValuesToQuestInformationElements();
		return;
	}
	if (QuestInformation == nullptr) return;

	if (ScrollBox_Objectives != nullptr && ObjectiveItemUMGLayout)
	{
		if (ScrollBox_Objectives->HasAnyChildren())
			ScrollBox_Objectives->ClearChildren();
		TArray<FFGPObjectiveData> Objectives;
		QuestInformation->GetQuestObjectives(Objectives);

		for (FFGPObjectiveData Objective : Objectives)
		{
			UFGPActiveQuestEntryObjectiveUMG* ObjectiveUMG;
			ObjectiveUMG = CreateWidget<UFGPActiveQuestEntryObjectiveUMG>(GetOwningPlayer(), ObjectiveItemUMGLayout);

			if (ObjectiveUMG == NULL) continue;
			ObjectiveUMG->SetObjectiveData(Objective);
			ScrollBox_Objectives->AddChild(ObjectiveUMG);
		}
	}

	if (Quest_Description != nullptr)
	{
		FText Description = QuestInformation->GetQuestDescription();
		Quest_Description->SetText(Description);
	}
}

void UFGPQuestDescriptionUMG::SetDefaultValuesToQuestInformationElements()
{
	if (Quest_Description != nullptr)
	{
		Quest_Description->SetText(FText::FromString("Quest description will appear here."));
	}
}
