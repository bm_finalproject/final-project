/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLog.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPQuestLog.h"
#include "FGPQuest.h"
#include "FGPMainQuestLabelUMG.h"
#include "FGPItem.h"
#include "FGPPlayerControllerBase.h"


UFGPQuestLog::UFGPQuestLog(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, ActiveQuestsMaxCount(3)
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	PrimaryComponentTick.bAllowTickOnDedicatedServer = false;

}

void UFGPQuestLog::BeginPlay()
{
	Super::BeginPlay();

	AFGPPlayerControllerBase* PController = Cast<AFGPPlayerControllerBase>(GetOwner());
	if (!PController) return;

	//Bind an event to the OnInventoryItemChanged delegate of the PlayerControllerBase class
	// to notify quests that they should update their objective status if needed
	// each time an item is added/removed to/from the player's inventory.
	PController->OnInventoryItemChanged.AddDynamic(this, &UFGPQuestLog::NotifyInventoryItemChanged);
}

void UFGPQuestLog::AddQuestToLog(AFGPQuest* Quest)
{
	if (Quest == NULL) return;
	if (Quest->GetQuestState() != EFGPQuestStatesEnum::QS_Unclaimed) return;

	if (Quest->PreRequisite != nullptr
		&& Quest->PreRequisite->GetQuestState() != EFGPQuestStatesEnum::QS_Completed)
		return;

	Quest->SetQuestLog(this);

	if (Quest->GetQuestType() == EFGPQuestTypeEnum::QT_MainQuest)
	{
		int previousCount = MainQuests.Num();
		{
			bool bCanAddMainQuest = true;
			if (MainQuests.Num() > 0)
			{
				if (MainQuests[previousCount - 1] != nullptr)
				{
					AFGPQuest* ExistingQuest = MainQuests[previousCount - 1];
					if (ExistingQuest->GetQuestState() != EFGPQuestStatesEnum::QS_Completed)
					{
						bCanAddMainQuest = false;
					}
					else
					{
						RemoveQuestFromActiveIfPossible(ExistingQuest);
					}
				}
			}
			if (!bCanAddMainQuest)
			{
				LogWarning("Trying to add a new main quest before completing the current one.");
				return;
			}
			MainQuests.AddUnique(Quest);
		}
		if (previousCount < MainQuests.Num())
		{
			MainQuestsContainerChangedEvent();
		}
	}
	else if (Quest->GetQuestType() == EFGPQuestTypeEnum::QT_SideQuest)
		SideQuests.AddUnique(Quest);
	Quest->QuestState = EFGPQuestStatesEnum::QS_Inactive;
	MakeQuestActiveIfPossible(Quest);
	CheckQuestCompletionForCollectedItems(Quest);
}

int UFGPQuestLog::GetActiveQuestsMaxCount() const
{
	return ActiveQuestsMaxCount;
}

void UFGPQuestLog::SetActiveQuestsMaxCount(const int& MaxCount)
{
	if (ActiveQuestsMaxCount == MaxCount) return;
	ActiveQuestsMaxCount = MaxCount;
	ActiveQuestMaxCountChanged(ActiveQuestsMaxCount);
}

bool UFGPQuestLog::MakeQuestActiveIfPossible(AFGPQuest* Quest)
{
	if (!Quest)
	{
		LogWarning((TEXT("No quest was passed.")));
		return false;
	}
	FString QName;
	Quest->GetName(QName);

	if (Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Inactive ||
		Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Unclaimed)
	{
		int PreviousCount = ActiveQuests.Num();

		if (ActiveQuests.Contains(Quest))
		{
			LogWarning("Quest was found inside active quests container: ", QName);
			return false;
		}
		if (PreviousCount >= ActiveQuestsMaxCount)
		{
			LogWarning("Active quests container has reached max capacity.");
			return false;
		}
		if (Quest->GetQuestType() == EFGPQuestTypeEnum::QT_MainQuest)
		{
			if (ActiveQuests.Num() > 0)
			{
				if (!ActiveQuests.Contains(Quest))
				{
					if (ActiveQuests[0] != nullptr && ActiveQuests[0]->GetQuestType() != EFGPQuestTypeEnum::QT_MainQuest)
					{
						TArray<AFGPQuest*> NewMainQuestArray;
						NewMainQuestArray.Add(Quest);
						ActiveQuests.Insert(NewMainQuestArray, 0);
					}
				}
			}
			else {
				ActiveQuests.AddUnique(Quest);
			}
		}
		else
			ActiveQuests.AddUnique(Quest);
		if (PreviousCount < ActiveQuests.Num())
		{
			Quest->QuestState = EFGPQuestStatesEnum::QS_Active;
			LogWarning("Quest was added to active quests container: ", QName);
			ActiveQuestsContainerChangedEvent();
			return true;
		}
	}
	LogWarning("Quest was not inactive or unclaimed: ", QName);
	return false;
}

bool UFGPQuestLog::RemoveQuestFromActiveIfPossible(AFGPQuest* Quest)
{
	if (!Quest) return false;
	FString QName;
	Quest->GetName(QName);

	int PreviousCount = ActiveQuests.Num();

	if (Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Inactive ||
		Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Unclaimed ||
		Quest->GetQuestState() == EFGPQuestStatesEnum::QS_MAX)
		return false;
	if (!ActiveQuests.Contains(Quest)) return false;

	int Count = ActiveQuests.Remove(Quest);
	if (Count > 0)
	{
		LogWarning("Quest was removed from active quests container: ", QName);
		if (Quest->IsCompleted())
		{
			LogWarning("Quest state changed to completed: ", QName);
			Quest->QuestState = EFGPQuestStatesEnum::QS_Completed;
		}
		else
		{
			LogWarning("Quest state changed to inactive:", QName);
			Quest->QuestState = EFGPQuestStatesEnum::QS_Inactive;
		}

		if (PreviousCount > ActiveQuests.Num())
		{
			ActiveQuestsContainerChangedEvent();
			return true;
		}
	}
	return false;
}

bool UFGPQuestLog::RemoveQuestFromActiveIfPossibleStatic(AFGPPlayerControllerBase* Player, AFGPQuest* Quest)
{
	if (!Player) return false;
	if (!Quest) return false;

	UFGPQuestLog* QuestLog = Player->GetQuestLogComponent();
	if (!QuestLog) return false;
	return QuestLog->RemoveQuestFromActiveIfPossible(Quest);
}

bool UFGPQuestLog::AbandonQuestIfPossible(AFGPQuest* Quest)
{
	if (!Quest) return false;
	FString QName;
	Quest->GetName(QName);

	if (Quest->GetQuestState() == EFGPQuestStatesEnum::QS_MAX ||
		Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Unclaimed ||
		Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed)
		return false;

	RemoveQuestFromActiveIfPossible(Quest);

	//Main quests should not be removed from the journal as they are mandatory for the story progression
	int Count = SideQuests.Remove(Quest);
	if (Count > 0)
	{
		LogWarning("Player abandoned quest: ", QName);
		Quest->RevertToInitialState();
		return true;
	}
	else
		return false;
}

void UFGPQuestLog::CheckQuestCompletionForCollectedItems(AFGPQuest* Quest)
{
	for (FFGPObjectiveData Objective : Quest->QuestObjectives)
	{
		UFGPItem* ItemToCollect = Objective.ItemToCollect;
		if (!ItemToCollect) continue;
		Quest->CollectedItemDelegate.Broadcast(ItemToCollect);
	}

}

bool UFGPQuestLog::DoesQuestLogContainQuest(AFGPPlayerControllerBase* Player, AFGPQuest* Quest)
{
	if (!Player) return false;
	if (!Quest) return false;

	UFGPQuestLog* QuestLog = Player->GetQuestLogComponent();
	if (!QuestLog) return false;
	return QuestLog->MainQuests.Contains(Quest) || QuestLog->SideQuests.Contains(Quest);
}

void UFGPQuestLog::BroadcastEnemyKillToQuests(AFGPPlayerControllerBase* Player, TSubclassOf<AActor> EnemyKilled)
{
	if (!Player) return;
	if (!EnemyKilled) return;
	UFGPQuestLog* QuestLog = Player->GetQuestLogComponent();
	if (!QuestLog) return;

	TArray<AFGPQuest*> Quests = QuestLog->MainQuests;
	Quests.Insert(QuestLog->SideQuests, Quests.Num());

	FString DisplayName;
	EnemyKilled->GetName(DisplayName);

	for (AFGPQuest* ItrQuest : Quests)
	{
		if (ItrQuest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed
			/*|| ItrQuest->IsCompleted()*/)
			continue;
		LogWarning("Killed enemy: ", DisplayName);
		ItrQuest->KilledTargetDelegate.Broadcast(EnemyKilled);
	}
}

void UFGPQuestLog::NotifyInventoryItemChanged_Implementation(bool bAdded, UFGPItem* Item)
{
	//LogWarning("Item added/removed to/from inventory, called from QuestLog");

	TArray<AFGPQuest*> _Quests;
	_Quests.Insert(MainQuests, _Quests.Num());
	_Quests.Insert(SideQuests, _Quests.Num());

	for (AFGPQuest* ItrQuest : _Quests)
	{
		if (ItrQuest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed
			/*|| ItrQuest->IsCompleted()*/)
			continue;
		ItrQuest->CollectedItemDelegate.Broadcast(Item);
	}
}

void UFGPQuestLog::MainQuestsContainerChangedEvent_Implementation()
{
	int32 MainQuestsTotal = MainQuests.Num();
	if (MainQuestsTotal > 0)
	{
		AFGPQuest* NewQuest = MainQuests[MainQuestsTotal - 1];
		OnMainQuestsContainerChanged.Broadcast(NewQuest);
	}
}

void UFGPQuestLog::ActiveQuestsContainerChangedEvent_Implementation()
{
	OnActiveQuestsContainerChanged.Broadcast();
}

void UFGPQuestLog::ActiveQuestMaxCountChanged_Implementation(const int& MaxCount)
{
	LogWarning("Active quest max count just changed! Was this intended?");
}

