/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPOnScreenActiveQuestsListUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/


#include "FGPOnScreenActiveQuestsListUMG.h"
#include "FGPQuestLog.h"
#include "FGPActiveQuestListEntryUMG.h"
#include "FGPPlayerControllerBase.h"

 // Engine
#include "Runtime/UMG/Public/Components/VerticalBox.h"

void UFGPOnScreenActiveQuestsListUMG::NativeConstruct()
{
	Super::NativeConstruct();

	UpdateElements();

	// Retrieving the quest log component from the player controller
	AFGPPlayerControllerBase* PlayerController = GetOwningPlayer<AFGPPlayerControllerBase>();
	if (PlayerController != nullptr)
	{
		QuestLog = PlayerController->GetQuestLogComponent();
		if (QuestLog)
		{
			int ActiveQuestsCount = QuestLog->ActiveQuests.Num();
			if (ActiveQuestsCount > 0)
			{
				// Update vertical box children list with starting quest list
				//as the player might start a game with quests already claimed and active
				UpdateElements();
			}
			QuestLog->OnActiveQuestsContainerChanged.AddDynamic(this,
				&UFGPOnScreenActiveQuestsListUMG::UpdateElements);
		}
	}
}

void UFGPOnScreenActiveQuestsListUMG::UpdateElements()
{
	if (VerticalBox_QuestList->HasAnyChildren())
		VerticalBox_QuestList->ClearChildren();

	if (QuestLog == NULL)
		SetDefaultValuesToElements();

	if (QuestItemUMGLayout && QuestLog)
	{
		TArray<AFGPQuest*> Quests = QuestLog->ActiveQuests;
		for (AFGPQuest* ItrQuest : Quests)
		{
			UFGPActiveQuestListEntryUMG* QuestUMG;
			QuestUMG = CreateWidget<UFGPActiveQuestListEntryUMG>(GetOwningPlayer(), QuestItemUMGLayout);

			if (QuestUMG == NULL) continue;
			QuestUMG->SetQuestInformation(ItrQuest);
			VerticalBox_QuestList->AddChild(QuestUMG);
		}
	}

	// Update this widget visibility state depending if it has any children or not
	if (VerticalBox_QuestList == NULL) return;
	if (VerticalBox_QuestList->HasAnyChildren())
	{
		if (Visibility != ESlateVisibility::HitTestInvisible)
			SetVisibility(ESlateVisibility::HitTestInvisible);
	}
	else
	{
		if (Visibility != ESlateVisibility::Collapsed)
			SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UFGPOnScreenActiveQuestsListUMG::SetDefaultValuesToElements()
{
// 	if (VerticalBox_QuestList == NULL) return;
// 	if (VerticalBox_QuestList->HasAnyChildren())
// 		VerticalBox_QuestList->ClearChildren();
}
