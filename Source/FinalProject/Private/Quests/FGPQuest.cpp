/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuest.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "Quests/FGPQuest.h"
#include "Quests/FGPQuestLog.h"
#include "FGPObjectiveLocationMarker.h"
#include "Items/FGPItem.h"

//Engine
#include "GameFramework/Actor.h"

// Sets default values
AFGPQuest::AFGPQuest(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, QuestName(FText::FromString(TEXT("{questname}")))
	, QuestState(EFGPQuestStatesEnum::QS_Unclaimed)
	, QuestDescription(FText::FromString(TEXT("{questdescription}")))
	, QuestTurnInDialog(FText::FromString(TEXT("{questturnindialog}")))
{
 	/** Set this actor to call Tick() every frame.
 	 *  Setting this to false improves performance if we don't need it.
 	 */
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.bAllowTickOnDedicatedServer = false;
}

void AFGPQuest::BeginPlay()
{
	Super::BeginPlay();

	// Binding quest objective action delegate to function
	LocationReachedDelegate.AddDynamic(this, &AFGPQuest::LocationReachedEvent);
	InteractedWithTargetDelegate.AddDynamic(this, &AFGPQuest::InteractedWithObjectiveTargetEvent);
	CollectedItemDelegate.AddDynamic(this, &AFGPQuest::CollectedItemEvent);
	KilledTargetDelegate.AddDynamic(this, &AFGPQuest::KilledTargetEvent);
}


FText AFGPQuest::GetQuestName() const
{
	return QuestName;
}

EFGPQuestTypeEnum AFGPQuest::GetQuestType() const
{
	return QuestType;
}

EFGPQuestStatesEnum AFGPQuest::GetQuestState() const
{
	return QuestState;
}

void AFGPQuest::GetQuestObjectives(TArray<FFGPObjectiveData>& ObjectiveList) const
{
	for (FFGPObjectiveData Objective : QuestObjectives)
	{
		ObjectiveList.Add(Objective);
	}
}

FText AFGPQuest::GetQuestDescription() const
{
	return QuestDescription;
}

void AFGPQuest::AlignQuestWithParentActor_Implementation()
{
	AActor* Parent = GetAttachParentActor();
	FVector ParentLocation = Parent->GetActorLocation();
	FRotator NewRotation = Parent->GetActorRotation();
	SetActorLocation(ParentLocation);
	SetActorRotation(NewRotation);
}

void AFGPQuest::UpdateThisActorOnEditor_Implementation()
{
#if WITH_EDITOR
	UWorld* ThisWorld = GetWorld();
	if (ThisWorld == nullptr) return;

	UClass* ThisActorClass = GetClass();
	FTransform ThisTransform = GetActorTransform();
	AActor* Parent = GetAttachParentActor();
	FActorSpawnParameters Parameters;
	AFGPQuest* NewQuest = Cast<AFGPQuest>(ThisWorld->SpawnActor(ThisActorClass));
	NewQuest->AttachToActor(Parent, FAttachmentTransformRules::KeepWorldTransform);
	NewQuest->SetActorTransform(ThisTransform);

	int32 OldObjectiveDefinition = QuestObjectives.Num();
	int32 NewObjectiveDefinition = NewQuest->QuestObjectives.Num();
	int32 MaxObjectiveCount = FMath::Min(OldObjectiveDefinition, NewObjectiveDefinition);

	for (int32 ObjectiveItr = 0; ObjectiveItr < MaxObjectiveCount; ++ObjectiveItr)
	{
		NewQuest->QuestObjectives[ObjectiveItr] = QuestObjectives[ObjectiveItr];
	}
	NewQuest->PreRequisite = PreRequisite;
	FString ObjName = GetActorLabel();
	NewQuest->SetActorLabel(ObjName);
#endif
}

UFGPQuestLog* AFGPQuest::GetQuestLog() const
{
	return QuestLogComponent;
}

void AFGPQuest::SetQuestLog(UFGPQuestLog* NewQuestLog)
{
	if (QuestLogComponent == NewQuestLog) return;
	QuestLogComponent = NewQuestLog;
	QuestLogChanged(QuestLogComponent);
}

void AFGPQuest::QuestLogChanged_Implementation(UFGPQuestLog* _QuestLog)
{
	FString QName;
	GetName(QName);
	if (_QuestLog)
		LogWarning("Quest found player's quest log: ", QName);
	else
		LogWarning("Quest did not find player's quest log: ", QName);
}

bool AFGPQuest::IsCompleted() const
{
	return bIsCompleted;
}

void AFGPQuest::SetIsCompleted(const bool& _bIsCompleted)
{
	if (bIsCompleted == _bIsCompleted) return;
	bIsCompleted = _bIsCompleted;
	IsCompletedChanged(bIsCompleted);
}

void AFGPQuest::RevertToInitialState()
{
	QuestState = EFGPQuestStatesEnum::QS_Unclaimed;
	// Reset quest to initial state
	TArray<FFGPObjectiveData> Objectives;
	GetQuestObjectives(Objectives);
	for (FFGPObjectiveData Objective : Objectives)
	{
		Objective.bIsComplete = false;
	}
	SetIsCompleted(false);
	SetQuestLog(nullptr);
}

bool AFGPQuest::IsQuestCompletedArchived(AFGPQuest* Quest)
{
	if (!Quest) return false;
	return (Quest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed);
}

bool AFGPQuest::IsQuestCompletedNotYetArchived(AFGPQuest* Quest)
{
	if (!Quest) return false;
	bool bIsCompleted = true;
	for (FFGPObjectiveData Objective : Quest->QuestObjectives)
	{
		if (!Objective.bIsComplete)
		{
			bIsCompleted = false;
			break;
		}
	}
	Quest->SetIsCompleted(bIsCompleted);
	return Quest->IsCompleted();
}

#if WITH_EDITOR
void AFGPQuest::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& e)
{
	Super::PostEditChangeChainProperty(e);
	
	// Check for modifications to values of properties of any listed quest objective
	int32 index = e.GetArrayIndex(TEXT("QuestObjectives"));
	if (index < 0) return;
	FName PropertyName = e.GetPropertyName();
	FText Text;
	FText TargetName;
	FText Description = QuestObjectives[index].Description;
	AActor* Target = QuestObjectives[index].Target;
	TSubclassOf<AActor> TargetToKill = QuestObjectives[index].TargetToKill;
	UFGPItem* Item = QuestObjectives[index].ItemToCollect;

	if (PropertyName == FFGPObjectiveData::PropertyNameType())
	{
		if (QuestObjectives[index].Type == EFGPQuestObjectiveTypesEnum::QOT_Location
			|| QuestObjectives[index].Type == EFGPQuestObjectiveTypesEnum::QOT_Interact)
		{
			QuestObjectives[index].Amount = -1;
		}
		else
		{
			QuestObjectives[index].Amount = 1;
		}

		if (Target)
		{
			AFGPObjectiveLocationMarker* LocationTarget = Cast<AFGPObjectiveLocationMarker>(Target);
			if (LocationTarget)
			{
				TargetName = LocationTarget->LocationName;
			}
			else
			{
				TargetName = FText::FromString(Target->GetHumanReadableName());
			}
		}
		else if (Item)
		{
			TargetName = Item->ItemName;
		}
	}

	if (PropertyName == FFGPObjectiveData::PropertyNameTarget())
	{
		if (QuestObjectives[index].Type == EFGPQuestObjectiveTypesEnum::QOT_Collect)
		{
			QuestObjectives[index].Target = nullptr;
		}

		if (Target)
		{
			AFGPObjectiveLocationMarker* LocationTarget = Cast<AFGPObjectiveLocationMarker>(Target);
			if (LocationTarget)
			{
				TargetName = LocationTarget->LocationName;
			}
			else
			{
				TargetName = FText::FromString(Target->GetHumanReadableName());
			}
		}
	}

	if (PropertyName == FFGPObjectiveData::PropertyNameTargetToKill())
	{
		if (QuestObjectives[index].Type != EFGPQuestObjectiveTypesEnum::QOT_Kill)
		{
			QuestObjectives[index].TargetToKill = nullptr;
			TargetToKill = QuestObjectives[index].TargetToKill;
		}

		if (TargetToKill)
		{
			FString TargetToKillName;
			TargetToKill->GetName(TargetToKillName);
			TargetName = FText::FromString(TargetToKillName);
		}
	}

	if (PropertyName == FFGPObjectiveData::PropertyNameItemToCollect())
	{
		if (Item && QuestObjectives[index].Type == EFGPQuestObjectiveTypesEnum::QOT_Collect)
		{
			TargetName = Item->ItemName;
		}
	}

	if (PropertyName == FFGPObjectiveData::PropertyNameDescription())
	{
		if (Target && QuestObjectives[index].Type != EFGPQuestObjectiveTypesEnum::QOT_Collect)
		{
			AFGPObjectiveLocationMarker* LocationTarget = Cast<AFGPObjectiveLocationMarker>(Target);
			if (LocationTarget)
			{
				TargetName = LocationTarget->LocationName;
			}
			else
			{
				TargetName = FText::FromString(Target->GetHumanReadableName());
			}
		}
		else if (Item && QuestObjectives[index].Type == EFGPQuestObjectiveTypesEnum::QOT_Collect)
		{
			TargetName = Item->ItemName;
		}
	}

	if (TargetName.ToString().Len() > 0
		&& !Description.ToString().Contains(TargetName.ToString(), ESearchCase::IgnoreCase))
	{
		Text = FText::FormatOrdered(
			FTextFormat::FromString("{0} {1}"),
			Description, TargetName);
	}
	else
	{
		Text = Description;
	}
	QuestObjectives[index].Description = Text;
}
#endif

void AFGPQuest::IsCompletedChanged_Implementation(bool& _bIsCompleted)
{
	FString QName;
	GetName(QName);
	LogWarning("Quest marked ", (_bIsCompleted ? "COMPLETED" : "UNCOMPLETED"));
}

void AFGPQuest::LocationReachedEvent_Implementation(AFGPObjectiveLocationMarker* Location)
{
	if (!Location) return;

	int32 ObjectiveIndex = -1;
	int32 MatchIndex = -1;
	FFGPObjectiveData NewObjectiveData;
	bool bUpdatedObjectiveData = false;
	for (FFGPObjectiveData ItrObjective : QuestObjectives)
	{
		ObjectiveIndex++;
		if (ItrObjective.Type != EFGPQuestObjectiveTypesEnum::QOT_Location)
			continue;

		if (!ItrObjective.bIsComplete && ItrObjective.Target == Location)
		{
			NewObjectiveData = ItrObjective;
			NewObjectiveData.bIsComplete = true;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}
		if (ItrObjective.bIsComplete && ItrObjective.Target == Location)
		{
			break;
		}
	}

	if (bUpdatedObjectiveData)
	{
		if (MatchIndex >= 0 && MatchIndex < QuestObjectives.Num())
		{
			QuestObjectives[MatchIndex] = NewObjectiveData;
			FText NewLocationName = Location->LocationName;
			LogWarning(NewLocationName.ToString());
			if (NewObjectiveData.bIsComplete)
			{
				//LogWarning("Location Reached!");
				QuestObjectivesChangedDelegate.Broadcast();
			}
			//else
			//	LogWarning("Can't update objective progress");

		}
	}
}

void AFGPQuest::InteractedWithObjectiveTargetEvent_Implementation(AActor* InteractedTarget)
{
	if (!InteractedTarget) return;
	//LogWarning("Interacted with quest target");

	int32 ObjectiveIndex = -1;
	int32 MatchIndex = -1;
	FFGPObjectiveData NewObjectiveData;
	bool bUpdatedObjectiveData = false;
	for (FFGPObjectiveData ItrObjective : QuestObjectives)
	{
		ObjectiveIndex++;
		if (ItrObjective.Type != EFGPQuestObjectiveTypesEnum::QOT_Interact)
			continue;

		if (!ItrObjective.bIsComplete && ItrObjective.Target == InteractedTarget)
		{
			NewObjectiveData = ItrObjective;
			NewObjectiveData.bIsComplete = true;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}
		if (ItrObjective.bIsComplete && ItrObjective.Target == InteractedTarget)
		{
			break;
		}
	}

	if (bUpdatedObjectiveData)
	{
		if (MatchIndex >= 0 && MatchIndex < QuestObjectives.Num())
		{
			QuestObjectives[MatchIndex] = NewObjectiveData;
			if (NewObjectiveData.bIsComplete)
			{
				//LogWarning("Interacted with target!");
				QuestObjectivesChangedDelegate.Broadcast();
			}
			else
				LogWarning("Can't update objective progress");

		}
	}
}

void AFGPQuest::CollectedItemEvent_Implementation(UFGPItem* ItemCollected)
{
	if (!ItemCollected) return;
	//LogWarning("Collected item called from event");
	if (!QuestLogComponent) return;
	AFGPPlayerControllerBase* PController = Cast<AFGPPlayerControllerBase>(QuestLogComponent->GetOwner());
	if (!PController) return;

	int32 ItemCountInInventory = PController->GetInventoryItemCount(ItemCollected);

	int32 ObjectiveIndex = -1;
	int32 MatchIndex = -1;
	FFGPObjectiveData NewObjectiveData;
	bool bUpdatedObjectiveData = false;
	for (FFGPObjectiveData ItrObjective : QuestObjectives)
	{
		ObjectiveIndex++;
		if (ItrObjective.Type != EFGPQuestObjectiveTypesEnum::QOT_Collect)
			continue;

		if (ItrObjective.ItemToCollect == ItemCollected)
		{
			NewObjectiveData = ItrObjective;
		}

		if (ItrObjective.ItemToCollect == ItemCollected
			&& ItrObjective.AmountCurrent != ItemCountInInventory)
		{
			NewObjectiveData.AmountCurrent = ItemCountInInventory;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}

		if (!ItrObjective.bIsComplete
			&& ItrObjective.ItemToCollect == ItemCollected
			&& ItrObjective.Amount <= ItemCountInInventory)
		{
			NewObjectiveData.bIsComplete = true;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}
		if (ItrObjective.bIsComplete
			&& ItrObjective.ItemToCollect == ItemCollected
			&& ItrObjective.Amount > ItemCountInInventory
			&& QuestState != EFGPQuestStatesEnum::QS_Completed)
		{
			// Objective was previously completed but the was removed from the inventory
			// before the quest was marked as completed
			//NewObjectiveData = ItrObjective;
			NewObjectiveData.bIsComplete = false;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
			break;
		}
		if (ItrObjective.bIsComplete
			&& ItrObjective.ItemToCollect == ItemCollected
			&& ItrObjective.Amount <= ItemCountInInventory)
		{
			break;
		}
	}

	if (bUpdatedObjectiveData)
	{
		if (MatchIndex >= 0 && MatchIndex < QuestObjectives.Num())
		{
			QuestObjectives[MatchIndex] = NewObjectiveData;
			QuestObjectivesChangedDelegate.Broadcast();
 			LogWarning("Objective UI updated");
		}
	}
}

void AFGPQuest::KilledTargetEvent_Implementation(TSubclassOf<AActor> Target)
{
	LogWarning("Killed enemy");
	if (!Target) return;
	//LogWarning("Interacted with quest target");
	int32 ObjectiveIndex = -1;
	int32 MatchIndex = -1;
	FFGPObjectiveData NewObjectiveData;
	bool bUpdatedObjectiveData = false;
	for (FFGPObjectiveData ItrObjective : QuestObjectives)
	{
		ObjectiveIndex++;
		if (ItrObjective.Type != EFGPQuestObjectiveTypesEnum::QOT_Kill)
			continue;

		if (ItrObjective.TargetToKill == Target)
		{
			FString DisplayName;
			Target->GetName(DisplayName);
			LogWarning("Killed enemy: ", DisplayName);
			NewObjectiveData = ItrObjective;
		}

		if (ItrObjective.TargetToKill == Target
			&& ItrObjective.AmountCurrent < ItrObjective.Amount)
		{
			NewObjectiveData.AmountCurrent++;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}

		if (!ItrObjective.bIsComplete && ItrObjective.TargetToKill == Target
			&& NewObjectiveData.AmountCurrent >= ItrObjective.Amount)
		{
			NewObjectiveData.bIsComplete = true;
			bUpdatedObjectiveData = true;
			MatchIndex = ObjectiveIndex;
		}
		if (ItrObjective.bIsComplete && ItrObjective.TargetToKill == Target)
		{
			break;
		}
	}

	if (bUpdatedObjectiveData)
	{
		if (MatchIndex >= 0 && MatchIndex < QuestObjectives.Num())
		{
			QuestObjectives[MatchIndex] = NewObjectiveData;
			QuestObjectivesChangedDelegate.Broadcast();
			LogWarning("Objective UI updated");
		}
	}
}

