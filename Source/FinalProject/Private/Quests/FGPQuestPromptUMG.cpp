/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestPromptUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPQuestPromptUMG.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPPlayerControllerBase.h"
#include "FGPActiveQuestEntryObjectiveUMG.h"

// Engine
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/ScrollBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"

void UFGPQuestPromptUMG::AcceptQuest()
{
	this->RemoveFromParent();
	bIsBeingDisplayed = false;

	if (QuestLog != nullptr)
	{
		QuestLog->AddQuestToLog(QuestInformation);
	}

	QuestInformation = NULL;
}

void UFGPQuestPromptUMG::CloseDialog()
{
	this->RemoveFromParent();
	bIsBeingDisplayed = false;
}

void UFGPQuestPromptUMG::TurnInQuest()
{
	if (QuestInformation)
	{
		AFGPPlayerControllerBase* Controller = Cast<AFGPPlayerControllerBase>(GetOwningPlayer());
		if (Controller == nullptr) return;
		if (UFGPQuestLog::RemoveQuestFromActiveIfPossibleStatic(Controller, QuestInformation))	// fix here PLAYER CONTROLLER BP
		{
			this->RemoveFromParent();
			bIsBeingDisplayed = false;
			QuestInformation = NULL;
		}
	}
}

void UFGPQuestPromptUMG::DisplayDialog()
{
	this->AddToViewport();
	bIsBeingDisplayed = true;
}

AFGPQuest* UFGPQuestPromptUMG::GetQuestInformation() const
{
	return QuestInformation;
}

void UFGPQuestPromptUMG::SetQuestInformation(AFGPQuest* NewQuest)
{
	if (QuestInformation == NewQuest) return;

	QuestInformation = NewQuest;
	QuestInformationChanged();
}

void UFGPQuestPromptUMG::QuestInformationChanged_Implementation()
{
	UpdateQuestDialogElements();
}

void UFGPQuestPromptUMG::UpdateQuestDialogElements()
{
	if (QuestInformation == NULL)
	{
		SetDefaultValuesToQuestInformationElements();
		return;
	}
	if (QuestInformation == nullptr) return;
	AFGPPlayerControllerBase* Controller = Cast<AFGPPlayerControllerBase>(GetOwningPlayer());
	bool bQuestLogContainsQuest = Controller != nullptr &&
		UFGPQuestLog::DoesQuestLogContainQuest(Controller, QuestInformation);

	if (Quest_Title != nullptr)
	{
		FText Title = QuestInformation->GetQuestName();
		Quest_Title->SetText(Title);
	}

	if (bQuestLogContainsQuest)
	{
		bool bIsCompletedNotYetArchived = AFGPQuest::IsQuestCompletedNotYetArchived(QuestInformation);

		if (TextBlock_Accept)
			TextBlock_Accept->SetText(FText::FromString("Turn in"));
		if (TextBlock_Reject)
			TextBlock_Reject->SetText(FText::FromString("Cancel"));

		if (Button_Accept)
		{
			Button_Accept->SetIsEnabled(bIsCompletedNotYetArchived);
			if (bIsCompletedNotYetArchived)
			{
				Button_Accept->OnClicked.RemoveAll(this);
				Button_Accept->OnClicked.AddDynamic(this, &UFGPQuestPromptUMG::TurnInQuest);
			}
		}
	}

	if (ScrollBox_Objectives != nullptr && ObjectiveItemUMGLayout)
	{
		TArray<FFGPObjectiveData> Objectives;
		QuestInformation->GetQuestObjectives(Objectives);

		for (FFGPObjectiveData Objective : Objectives)
		{
			UFGPActiveQuestEntryObjectiveUMG* ObjectiveUMG;
			ObjectiveUMG = CreateWidget<UFGPActiveQuestEntryObjectiveUMG>(GetOwningPlayer(), ObjectiveItemUMGLayout);

			if (ObjectiveUMG == NULL) continue;
			ObjectiveUMG->bIsInsideQuestPrompt = true;
			ObjectiveUMG->SetObjectiveData(Objective);
			ScrollBox_Objectives->AddChild(ObjectiveUMG);
		}
	}

	if (Quest_Description != nullptr)
	{
		FText Description = QuestInformation->GetQuestDescription();
		if (bQuestLogContainsQuest)
			Description = QuestInformation->QuestTurnInDialog;

		Quest_Description->SetText(Description);
	}
}

void UFGPQuestPromptUMG::NativeConstruct()
{
	Super::NativeConstruct();
	
	UpdateQuestDialogElements();

	if (Button_Reject != nullptr)
	{
		Button_Reject->OnClicked.AddDynamic(this, &UFGPQuestPromptUMG::CloseDialog);
	}

	if (Button_Accept != nullptr)
	{
		Button_Accept->OnClicked.AddDynamic(this, &UFGPQuestPromptUMG::AcceptQuest);
	}

	bIsBeingDisplayed = false;

	// Retrieving the quest log component from the player controller
	AFGPPlayerControllerBase* PlayerController = GetOwningPlayer<AFGPPlayerControllerBase>();
	if (PlayerController != nullptr)
	{
		QuestLog = PlayerController->GetQuestLogComponent();
	}
}

void UFGPQuestPromptUMG::SetDefaultValuesToQuestInformationElements()
{
	if (Quest_Title != nullptr)
	{
		Quest_Title->SetText(FText::FromString("Quest title"));
	}

// 	if (Quest_Objectives != nullptr)
// 	{
// 		Quest_Objectives->SetText(FText::FromString("Quest objectives will be listed here."));
// 	}

	if (Quest_Description != nullptr)
	{
		Quest_Description->SetText(FText::FromString("Quest description will appear here."));
	}
}
