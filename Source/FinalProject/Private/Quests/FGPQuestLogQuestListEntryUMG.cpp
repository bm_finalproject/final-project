/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPQuestLogQuestListEntryUMG.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPQuestLogQuestListEntryUMG.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPQuestLogUMG.h"

// Engine
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/CheckBox.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/SlateCore/Public/Styling/SlateWidgetStyleAsset.h"
#include "Runtime/Slate/Public/Framework/Styling/ButtonWidgetStyle.h"

void UFGPQuestLogQuestListEntryUMG::NativeConstruct()
{
	Super::NativeConstruct();
	UpdateElements();

	if (CheckBox_IsQuestActive)
	{
		//Button_QuestActiveToggle->OnClicked.AddDynamic(this, &UFGPQuestLogQuestListEntryUMG::ToggleActiveStateIfPossible);
		Button_QuestActiveToggle->OnPressed.AddDynamic(this, &UFGPQuestLogQuestListEntryUMG::ToggleActiveStateIfPossible);
	}
	if (Button_QuestEntry)
	{
		Button_QuestEntry->OnPressed.AddDynamic(this, &UFGPQuestLogQuestListEntryUMG::DisplayQuestInformation);
	}
}

void UFGPQuestLogQuestListEntryUMG::UpdateElements()
{
	if (QuestInformation == NULL)
	{
		if (Button_QuestEntry)
		{
			Button_QuestEntry->SetVisibility(ESlateVisibility::Collapsed);
		}
		if (TextBlock_Placeholder)
		{
			TextBlock_Placeholder->SetVisibility(ESlateVisibility::Visible);
		}
		return;
	}

	if (TextBlock_Placeholder)
	{
		TextBlock_Placeholder->SetVisibility(ESlateVisibility::Collapsed);
	}

	if (TextBlock_QuestName)
	{
		TextBlock_QuestName->SetText(QuestInformation->GetQuestName());
	}

	if (Button_QuestEntry)
	{
		// Updating button style depending on quest type
		UButtonWidgetStyle* Style = Cast<UButtonWidgetStyle>(SideQuestStyle->CustomStyle);
		if (QuestInformation->GetQuestType() == EFGPQuestTypeEnum::QT_MainQuest)
			Style = Cast<UButtonWidgetStyle>(MainQuestStyle->CustomStyle);
		
		Button_QuestEntry->SetStyle(Style->ButtonStyle);
		Button_QuestEntry->SetVisibility(ESlateVisibility::Visible);
	}

	UpdateCheckBoxIfQuestActive();
}

void UFGPQuestLogQuestListEntryUMG::SetQuestInformation(AFGPQuest* NewQuest, UFGPQuestLogUMG* EntryContainer)
{
	if (QuestInformation == NewQuest) return;
	if (QuestLogUMGContainer == EntryContainer)
	{
		LogWarning("QuestLogUMG container is the same as the previous one. Was this intentional?");
	}

	QuestInformation = NewQuest;
	QuestLogUMGContainer = EntryContainer;
	QuestInformationChanged();
}

void UFGPQuestLogQuestListEntryUMG::UpdateCheckBoxIfQuestActive()
{
	if (CheckBox_IsQuestActive)
	{
		ECheckBoxState IsChecked = ECheckBoxState::Unchecked;
		if (QuestInformation->GetQuestState() == EFGPQuestStatesEnum::QS_Active)
		{
			IsChecked = ECheckBoxState::Checked;
		}
		CheckBox_IsQuestActive->SetCheckedState(IsChecked);
	}
}

bool UFGPQuestLogQuestListEntryUMG::MakeQuestActiveIfPossible()
{
	if (!QuestInformation) return false;
	UFGPQuestLog* QuestLog = QuestInformation->GetQuestLog();
	if (!QuestLog) return false;
	bool bWasSuccess = QuestLog->MakeQuestActiveIfPossible(QuestInformation);
	bool bIsChecked = QuestInformation->QuestState == EFGPQuestStatesEnum::QS_Active;
	CheckBox_IsQuestActive->SetIsChecked(QuestInformation->QuestState == EFGPQuestStatesEnum::QS_Active);
	return bWasSuccess;
}

bool UFGPQuestLogQuestListEntryUMG::RemoveQuestFromActiveIfPossible()
{
	if (!QuestInformation) return false;
	UFGPQuestLog* QuestLog = QuestInformation->GetQuestLog();
	if (!QuestLog) return false;
	bool bWasSuccess = QuestLog->RemoveQuestFromActiveIfPossible(QuestInformation);
	bool bIsInactive = QuestInformation->QuestState == EFGPQuestStatesEnum::QS_Inactive;
	CheckBox_IsQuestActive->SetIsChecked(bIsInactive == true ? false : true);
	return bWasSuccess;
}

void UFGPQuestLogQuestListEntryUMG::ToggleActiveStateIfPossible()
{
	if (!QuestInformation) return;
	if (CheckBox_IsQuestActive->IsChecked() && QuestInformation->QuestState == EFGPQuestStatesEnum::QS_Active)
	{
		RemoveQuestFromActiveIfPossible();
	}
	else if (!CheckBox_IsQuestActive->IsChecked() && QuestInformation->QuestState == EFGPQuestStatesEnum::QS_Inactive)
	{
		MakeQuestActiveIfPossible();
	}
}

void UFGPQuestLogQuestListEntryUMG::DisplayQuestInformation()
{
	if (QuestLogUMGContainer && QuestInformation != nullptr)
	{
		QuestLogUMGContainer->SetQuestToDisplay(QuestInformation);
	}
}

void UFGPQuestLogQuestListEntryUMG::QuestInformationChanged_Implementation()
{
	UpdateElements();
}
