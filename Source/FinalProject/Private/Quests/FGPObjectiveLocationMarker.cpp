/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPObjectiveLocationMarker.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/


#include "FGPObjectiveLocationMarker.h"
#include "FinalProject.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPCharacterBase.h"
#include "FGPPlayerControllerBase.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"

AFGPObjectiveLocationMarker::AFGPObjectiveLocationMarker(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.bAllowTickOnDedicatedServer = false;

	TriggerArea = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(
		this, TEXT("TriggerArea"));
	RootComponent = TriggerArea;
	//TriggerArea->SetupAttachment(RootComponent);
	TriggerArea->SetSphereRadius(32.f);
	TriggerArea->OnComponentBeginOverlap.AddDynamic(this, &AFGPObjectiveLocationMarker::OverlapBegin);
}

// Called when the game starts or when spawned
void AFGPObjectiveLocationMarker::BeginPlay()
{
	Super::BeginPlay();

	//if (!TriggerArea) return;
}

void AFGPObjectiveLocationMarker::OverlapBegin_Implementation(
	UPrimitiveComponent* OverlappedComponent
	, AActor* OtherActor
	, UPrimitiveComponent* OtherComp
	, int32 OtherBodyIndex
	, bool bFromSweep
	, const FHitResult& SweepResult
) {
	if (OtherActor == nullptr) return;
	if (!OtherActor->IsA(PlayerCharacterActorClass)) return;
	if (!OtherComp) return;

	//Check if sphere collision component is ignoring InteractionComponent objects
	//Instances of this class MUST ignore interaction components as they exist for
	// interacting with quest giver actors (which are standing in front of the player character
// 	FString CompName;
// 	OtherComp->GetName(CompName);
// 	LogWarning(CompName);
	if (!QuestLog)
	{
		AFGPCharacterBase* Character = Cast<AFGPCharacterBase>(OtherActor);
		if (!Character) return;
		AFGPPlayerControllerBase* PController = Cast<AFGPPlayerControllerBase>(Character->GetController());
		if (!PController) return;
		QuestLog = PController->GetQuestLogComponent();
	}

	if (!QuestLog) return;
	TArray<AFGPQuest*> Quests = QuestLog->MainQuests;
	Quests.Insert(QuestLog->SideQuests, Quests.Num());

	for (AFGPQuest* ItrQuest : Quests)
	{
		if (ItrQuest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed ||
			ItrQuest->IsCompleted())
			continue;
		ItrQuest->LocationReachedDelegate.Broadcast(this);
	}
}


