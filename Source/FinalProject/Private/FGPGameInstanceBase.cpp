/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPGameInstanceBase.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "FGPGameInstanceBase.h"
#include "FinalProject.h"
#include "Kismet/GameplayStatics.h"
#include "FGPSaveGame.h"

UFGPGameInstanceBase::UFGPGameInstanceBase()
	: SaveSlot(TEXT("SaveGame"))
	, SaveUserIndex(0)
{}

void UFGPGameInstanceBase::AddDefaultInventory(
	UFGPSaveGame* SaveGame
	, bool bRemoveExtra /*= false*/
) {
	// If we want to remove extra, clear out the existing inventory
	if (bRemoveExtra)
	{
		SaveGame->InventoryData.Reset();
	}

	// Add the default inventory, this only adds if not already in the
	// inventory
	for (const TPair<FPrimaryAssetId, FFGPItemData>& Pair : DefaultInventory)
	{
		FPrimaryAssetId ItemData = Pair.Key;
		//LogMsg(ItemData.PrimaryAssetName.ToString());
		if (!SaveGame->InventoryData.Contains(Pair.Key))
		{
			SaveGame->InventoryData.Add(Pair.Key, Pair.Value);
		}
	}
}

bool UFGPGameInstanceBase::IsValidItemSlot(FFGPItemSlot ItemSlot) const
{
	if (ItemSlot.IsValid())
	{
		const int32* FoundCount = ItemSlotsPerType.Find(ItemSlot.ItemType);

		if (FoundCount)
		{
			return ItemSlot.SlotNumber < *FoundCount;
		}
	}
	return false;
}

UFGPSaveGame* UFGPGameInstanceBase::GetCurrentSaveGame()
{
	return CurrentSaveGame;
}

void UFGPGameInstanceBase::SetSavingEnabled(bool bEnabled)
{
	bSavingEnabled = bEnabled;
}

bool UFGPGameInstanceBase::LoadOrCreateSaveGame()
{
	// Drop reference to old save game, this will GC out
	CurrentSaveGame = nullptr;
	//LogMsg("Loading or Creating Game");
	if (UGameplayStatics::DoesSaveGameExist(SaveSlot, SaveUserIndex) && bSavingEnabled)
	{
		CurrentSaveGame = Cast<UFGPSaveGame>(UGameplayStatics::LoadGameFromSlot(SaveSlot, SaveUserIndex));
	}

	if (CurrentSaveGame != nullptr)
	{
		// Make sure it has any newly added default inventory
		//LogMsg("Loading Game");
		AddDefaultInventory(CurrentSaveGame, false);
		return true;
	}
	else
	{
		// This creates it on demand
		//LogMsg("Creating Game");
		CurrentSaveGame = Cast<UFGPSaveGame>(UGameplayStatics::CreateSaveGameObject(UFGPSaveGame::StaticClass()));
		AddDefaultInventory(CurrentSaveGame, true);
		return false;
	}
}

bool UFGPGameInstanceBase::WriteSaveGame()
{
	if (bSavingEnabled)
	{
		return UGameplayStatics::SaveGameToSlot(GetCurrentSaveGame(),
			SaveSlot, SaveUserIndex);
	}
	return false;
}

void UFGPGameInstanceBase::ResetSaveGame()
{
	bool bWasSavingEnabled = bSavingEnabled;
	bSavingEnabled = false;
	LoadOrCreateSaveGame();
	bSavingEnabled = bWasSavingEnabled;
}

