/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPDataSingleton.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPDataSingleton.h"

UFGPDataSingleton::UFGPDataSingleton(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}
