// Fill out your copyright notice in the Description page of Project Settings.

#include "Items/FGPItem.h"
#include "FinalProject.h"

FString UFGPItem::GetIdentifierString() const
{
	return GetPrimaryAssetId().ToString();
}

FPrimaryAssetId UFGPItem::GetPrimaryAssetId() const
{
	// This is a DataAsset and not a blueprint so we can just use the raw FNAME
	// For blueprints we need to handle stripping the _C suffix
	return FPrimaryAssetId(ItemType, GetFName());
}
