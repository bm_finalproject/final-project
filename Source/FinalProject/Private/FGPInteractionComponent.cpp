/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPInteractionComponent.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPInteractionComponent.h"
#include "FinalProject.h"
#include "FGPQuest.h"
#include "FGPQuestLog.h"
#include "FGPCharacterBase.h"
#include "FGPPlayerControllerBase.h"
#include "Collectables/FGPCollectable.h"
#include "Items/FGPItem.h"

UFGPInteractionComponent::UFGPInteractionComponent(const FObjectInitializer& ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	PrimaryComponentTick.bAllowTickOnDedicatedServer = false;
}

// Called when the game starts
void UFGPInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UFGPInteractionComponent::BroadcastToQuestsForInteractEvents()
{
	if (!QuestLog) return;
	AActor* Parent = GetOwner();
	TArray<AFGPQuest*> Quests = QuestLog->MainQuests;
	Quests.Insert(QuestLog->SideQuests, Quests.Num());

	for (AFGPQuest* ItrQuest : Quests)
	{
		if (ItrQuest->GetQuestState() == EFGPQuestStatesEnum::QS_Completed ||
			ItrQuest->IsCompleted())
			continue;
		ItrQuest->InteractedWithTargetDelegate.Broadcast(Parent);
	}
}

void UFGPInteractionComponent::BroadcastToQuestsForCollectedItemEvents(UFGPItem* ItemCollected)
{
	//
}

void UFGPInteractionComponent::InteractEvent_Implementation(AActor* OtherActor)
{
	//LogWarning("Interaction call from InteractionComponent");
	if (!PlayerController)
	{
		AFGPCharacterBase* Character = Cast<AFGPCharacterBase>(OtherActor);
		if (!Character) return;
		PlayerController = Cast<AFGPPlayerControllerBase>(Character->GetController());
	}
	if (!PlayerController) return;

	if (!QuestLog)
	{
		QuestLog = PlayerController->GetQuestLogComponent();
	}

	AActor* Parent = GetOwner();
	AFGPCollectable* CollectableItem = Cast<AFGPCollectable>(Parent);
	if (CollectableItem != nullptr)
	{
		//LogWarning("Actor is a collectable");
		UFGPItem* Item = CollectableItem->ItemType;
		int AmountToAdd = 1;
		FText ItemName = Item->ItemName;
		LogWarning(ItemName.ToString());

		bool bAddedItemToInventory = PlayerController->AddInventoryItem(Item, AmountToAdd, 1, false);
		
		if (bAddedItemToInventory)
		{
			//The broadcast of modifications to a inventory item is done by the OnInventoryItemChanged events
			//Set by the questlog component to the delegate on the player controller base class

			//Set timer to destroy collectable item
			FTimerDelegate TimerDel;
			FTimerHandle Handle;
			float DelayTime = 0.2f;
			TimerDel.BindLambda([this, CollectableItem]
			{
				//function body
				CollectableItem->Destroy();
			});

			Parent->GetWorldTimerManager().SetTimer(Handle, TimerDel, DelayTime, false);
		}
	}
	else {
		//LogWarning("Actor is NOT a collectable");
		BroadcastToQuestsForInteractEvents();
	}

	
}
