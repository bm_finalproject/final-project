/*******************************************************************
Bachelor of Games and Apps Development
Universidade Europeia
Lisboa
Portugal

(c) 2019 Universidade Europeia

File Name	: FGPGameMode.cpp
Description	: 
Author		: Bruno Matos
Mail		: bruno.rosal.matos@gmail.com
********************************************************************/

#include "FGPGameMode.h"
#include "FinalProject.h"

#include "FGPGameStateBase.h"
#include "FGPPlayerControllerBase.h"

//class AFGPGameMode {

AFGPGameMode::AFGPGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	GameStateClass = AFGPGameStateBase::StaticClass();
	PlayerControllerClass = AFGPPlayerControllerBase::StaticClass();
	
}

void AFGPGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	LogMsg("Starting new Game", "");
}



//};