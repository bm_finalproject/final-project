/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPTargetType.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "Abilities/FGPTargetType.h"

void UFGPTargetType::GetTargets_Implementation(
	AFGPCharacterBase* TargetingCharacter
	, AActor* TargetingActor
	, FGameplayEventData EventData
	, TArray<FHitResult>& OutHitResults
	, TArray<AActor*>& OutActors
) const
{
	return;
}

void UFGPTargetType_UseOwner::GetTargets_Implementation(
	AFGPCharacterBase* TargetingCharacter
	, AActor* TargetingActor
	, FGameplayEventData EventData
	, TArray<FHitResult>& OutHitResults
	, TArray<AActor*>& OutActors
) const
{
	OutActors.Add(TargetingCharacter);
}

void UFGPTargetType_UseEventData::GetTargets_Implementation(
	AFGPCharacterBase* TargetingCharacter
	, AActor* TargetingActor
	, FGameplayEventData EventData
	, TArray<FHitResult>& OutHitResults
	, TArray<AActor*>& OutActors
) const
{
	const FHitResult* FoundHitResult = EventData.ContextHandle.GetHitResult();
	if (FoundHitResult)
	{
		OutHitResults.Add(*FoundHitResult);
	}
	else if (EventData.Target)
	{
		OutActors.Add(const_cast<AActor*>(EventData.Target));
	}
}
