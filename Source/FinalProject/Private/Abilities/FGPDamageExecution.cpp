// Fill out your copyright notice in the Description page of Project Settings.

#include "FGPDamageExecution.h"
#include "FinalProject.h"
#include "Abilities/FGPAttributeSet.h"
#include "Abilities/FGPAbilitySystemComponent.h"

struct FGPDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(DefensePower);
	DECLARE_ATTRIBUTE_CAPTUREDEF(AttackPower);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);

	FGPDamageStatics()
	{
		// Capture the target's DefensePower. Not snapshotting it, as we want to use the health value at the moment
		// we apply the execution
		DEFINE_ATTRIBUTE_CAPTUREDEF(UFGPAttributeSet, DefensePower, Target, false);

		// Capture the source's AttackPower. Snapshotting the value at the moment we create the GameplayEffectSpec
		// that will execute the damage.
		// (example, firing a projectile: we create the GE Spec when the projectile is fired. When it hits the target,
		// we want to use the AttackPower at the moment the projectile was launched, and not when it hits)
		DEFINE_ATTRIBUTE_CAPTUREDEF(UFGPAttributeSet, AttackPower, Source, true);

		// Capture the source's raw Damage, which is normally passed in directly via the execution
		DEFINE_ATTRIBUTE_CAPTUREDEF(UFGPAttributeSet, Damage, Source, true);
	}

};

static const FGPDamageStatics& DamageStatics()
{
	static FGPDamageStatics DmgStatics;
	return DmgStatics;
}

UFGPDamageExecution::UFGPDamageExecution()
{
	RelevantAttributesToCapture.Add(DamageStatics().DefensePowerDef);
	RelevantAttributesToCapture.Add(DamageStatics().AttackPowerDef);
	RelevantAttributesToCapture.Add(DamageStatics().DamageDef);
}

void UFGPDamageExecution::Execute_Implementation(
	const FGameplayEffectCustomExecutionParameters& ExecutionParams
	, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput
) const
{
	UAbilitySystemComponent* TargetAbilitySystemComponent = ExecutionParams.GetTargetAbilitySystemComponent();
	UAbilitySystemComponent* SourceAbilitySystemComponent = ExecutionParams.GetSourceAbilitySystemComponent();

	AActor* SourceActor = SourceAbilitySystemComponent != nullptr ? SourceAbilitySystemComponent->AvatarActor : nullptr;
	AActor* TargetActor = TargetAbilitySystemComponent != nullptr ? TargetAbilitySystemComponent->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	// Gather the tags from the source and target as that can affect which buffs should be used
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	// --------------------------------------------
	// Damage Done = Damage * AttackPower / DefensePower
	// If DefensePower is 0, it is treated as 1.0

	float DefensePower = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(
		DamageStatics().DefensePowerDef, EvaluationParameters, DefensePower);
	if (DefensePower == 0.0f)
	{
		DefensePower = 1.0f;
	}

	float AttackPower = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(
		DamageStatics().AttackPowerDef, EvaluationParameters, AttackPower);

	float Damage = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(
		DamageStatics().DamageDef, EvaluationParameters, Damage);

	float DamageDone = Damage * AttackPower / DefensePower;
	if (DamageDone > 0.f)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(
			DamageStatics().DamageProperty,
			EGameplayModOp::Additive,
			DamageDone));
	}
}
