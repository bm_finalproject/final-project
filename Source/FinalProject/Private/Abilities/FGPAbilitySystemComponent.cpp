// Fill out your copyright notice in the Description page of Project Settings.

#include "Abilities/FGPAbilitySystemComponent.h"
#include "Abilities/FGPGameplayAbility.h"
#include "FinalProject.h"
#include "FGPCharacterBase.h"

// Engine
#include "GameplayAbilities/Public/AbilitySystemGlobals.h"


UFGPAbilitySystemComponent::UFGPAbilitySystemComponent()
{
}

void UFGPAbilitySystemComponent::GetActiveAbilitiesWithTags(
	const FGameplayTagContainer& GameplayTagContainer
	, TArray<UFGPGameplayAbility*>& ActiveAbilities)
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer,
		AbilitiesToActivate, false);

	// Iterate the list of all ability specs
	for (FGameplayAbilitySpec* Spec : AbilitiesToActivate)
	{
		//Iterate all instances on this ability spec
		TArray<UGameplayAbility*> AbilityInstances = Spec->GetAbilityInstances();

		for (UGameplayAbility* ActiveAbility : AbilityInstances)
		{
			ActiveAbilities.Add(Cast<UFGPGameplayAbility>(ActiveAbility));
		}
	}
}

int32 UFGPAbilitySystemComponent::GetDefaultAbilityLevel() const
{
	AFGPCharacterBase* OwningCharacter = Cast<AFGPCharacterBase>(OwnerActor);

	if (OwningCharacter != nullptr)
	{
		return OwningCharacter->GetCharacterLevel();
	}
	return 1;
}

UFGPAbilitySystemComponent* UFGPAbilitySystemComponent::GetAbilitySystemComponentFromActor(
	const AActor* Actor
	, bool LookForComponent)
{
	return Cast<UFGPAbilitySystemComponent>(UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor, LookForComponent));
}
