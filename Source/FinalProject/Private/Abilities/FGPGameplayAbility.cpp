// Fill out your copyright notice in the Description page of Project Settings.

#include "Abilities/FGPGameplayAbility.h"
#include "Abilities/FGPAbilitySystemComponent.h"
#include "Abilities/FGPTargetType.h"
#include "FinalProject.h"
#include "FGPCharacterBase.h"

UFGPGameplayAbility::UFGPGameplayAbility()
{

}

FFGPGameplayEffectContainerSpec
UFGPGameplayAbility::MakeEffectContainerSpecFromContainer(
	const FFGPGameplayEffectContainer& Container
	, const FGameplayEventData& EventData
	, int32 OverrideGameplayLevel
) {
	// First figure out our actor info
	FFGPGameplayEffectContainerSpec ReturnSpec;
	AActor* OwningActor = GetOwningActorFromActorInfo();
	AFGPCharacterBase* OwningCharacter = Cast<AFGPCharacterBase>(OwningActor);
	UFGPAbilitySystemComponent* OwningASC =	UFGPAbilitySystemComponent::GetAbilitySystemComponentFromActor(OwningActor);

	if (OwningASC)
	{
		// If we have a target type, run the targeting logic.
		// (Optional as targets can be added later.
		if (Container.TargetType.Get())
		{
			TArray<FHitResult> HitResults;
			TArray<AActor*> TargetActors;
			const UFGPTargetType* TargetTypeCDO = Container.TargetType.GetDefaultObject();
			AActor* AvatarActor = GetAvatarActorFromActorInfo();

			TargetTypeCDO->GetTargets(OwningCharacter, AvatarActor, EventData, HitResults, TargetActors);
			ReturnSpec.AddTargets(HitResults, TargetActors);
		}

		// If we don't have an override level, use the default on the ability
		// system component
		if (OverrideGameplayLevel == INDEX_NONE)
		{
			OverrideGameplayLevel = OverrideGameplayLevel = this->GetAbilityLevel();
		}

		// Build GameplayEffectSpecs for each applied effect
		for (const TSubclassOf<UGameplayEffect>& EffectClass :	Container.TargetGameplayEffectClasses)
		{
			ReturnSpec.TargetGameplayEffectSpecs.Add(
				MakeOutgoingGameplayEffectSpec(EffectClass, OverrideGameplayLevel));
		}
	}

	return ReturnSpec;
}

FFGPGameplayEffectContainerSpec
UFGPGameplayAbility::MakeEffectContainerSpec(
	FGameplayTag ContainerTag
	, const FGameplayEventData& EventData
	, int32 OverrideGameplayLevel
) {
	FFGPGameplayEffectContainer* FoundContainer = EffectContainerMap.Find(ContainerTag);
	
	if (FoundContainer)
	{
		return MakeEffectContainerSpecFromContainer(*FoundContainer, EventData, OverrideGameplayLevel);
	}
	return FFGPGameplayEffectContainerSpec();
}

TArray<FActiveGameplayEffectHandle>
UFGPGameplayAbility::ApplyEffectContainerSpec(
	const FFGPGameplayEffectContainerSpec& ContainerSpec
) {
	TArray<FActiveGameplayEffectHandle> AllEffects;

	// Iterate list of effect specs and apply them to their target data
	for (const FGameplayEffectSpecHandle& SpecHandle :
		ContainerSpec.TargetGameplayEffectSpecs)
	{
		AllEffects.Append(K2_ApplyGameplayEffectSpecToTarget(SpecHandle, ContainerSpec.TargetData));
	}
	return AllEffects;
}

TArray<FActiveGameplayEffectHandle>
UFGPGameplayAbility::ApplyEffectContainer(
	FGameplayTag ContainerTag
	, const FGameplayEventData& EventData
	, int32 OverrideGameplayLevel
) {
	FFGPGameplayEffectContainerSpec Spec = MakeEffectContainerSpec(ContainerTag,
		EventData, OverrideGameplayLevel);
	return ApplyEffectContainerSpec(Spec);
}

