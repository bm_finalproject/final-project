/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCollectable.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/

#include "FGPCollectable.h"
#include "FinalProject.h"
#include "FGPDataSingleton.h"
#include "FGPBlueprintLibrary.h"
#include "FGPInteractionComponent.h"
#include "Items/FGPCollectableItem.h"

//Engine
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFGPCollectable::AFGPCollectable(const FObjectInitializer& ObjectInitializer)
{
	// Set actor to not tick on update, should only answer to events
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.bAllowTickOnDedicatedServer = false;

	// Add Collision sphere for overlap events
	TriggerArea = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(
		this, TEXT("TriggerArea"));
	RootComponent = TriggerArea;
	TriggerArea->SetSphereRadius(64.f);
	TriggerArea->SetMobility(EComponentMobility::Stationary);
	TriggerArea->SetRelativeLocationAndRotation(FVector::ZeroVector, FRotator::ZeroRotator);

	// Add Static mesh for actor visuals
	StaticMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(
		this, TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMesh->SetMobility(EComponentMobility::Stationary);

	// Add interaction component to actor
	bool bIsValid = false;
	DataSingleton = UFGPBlueprintLibrary::GetDataSingleton(bIsValid);
	if (!bIsValid) return;
	TSubclassOf<UFGPInteractionComponent> InteractionComponentClass = DataSingleton->InteractionComponent_BPC;

	if (InteractionComponentClass)
	{
		//LogWarning("InteractionComponent class creation successful.");
		InteractionComponent = static_cast<UFGPInteractionComponent*>(
			ObjectInitializer.CreateDefaultSubobject(
				this
				, TEXT("Interaction Commponent")
				, UFGPInteractionComponent::StaticClass()
				, InteractionComponentClass
				, /*bIsRequired =*/ false
				, /*bIsAbstract =*/ false
				, /*bTransient =*/ false));

		if (!IsValid(InteractionComponent))
// 		{
// 			LogWarning((TEXT("%s created successfully."), *InteractionComponent->GetName()));
// 		}
// 		else
		{
			LogWarning("Failed to create InteractionComponent.");
		}
	}
	else
	{
		LogWarning("Failed to create InteractionComponent class.");
	}

	
}

// Called when the game starts or when spawned
void AFGPCollectable::BeginPlay()
{
	Super::BeginPlay();
	
}
