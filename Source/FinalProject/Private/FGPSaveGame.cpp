/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPSaveGame.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "FGPSaveGame.h"
#include "FinalProject.h"
#include "FGPGameInstanceBase.h"

void UFGPSaveGame::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsLoading() && SavedDataVersion != EFGPSaveGameVersion::LatestVersion)
	{
		if (SavedDataVersion < EFGPSaveGameVersion::AddedItemData)
		{
			for (const FPrimaryAssetId& ItemId : InventoryItems_DEPRECATED)
			{
				InventoryData.Add(ItemId, FFGPItemData(1, 1));
			}

			InventoryItems_DEPRECATED.Empty();
		}
		SavedDataVersion = EFGPSaveGameVersion::LatestVersion;
	}
}
