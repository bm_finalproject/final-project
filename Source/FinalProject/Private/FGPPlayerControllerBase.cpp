/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPPlayerControllerBase.cpp
 * Description	: Base class for PlayerController
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "FGPPlayerControllerBase.h"
#include "FinalProject.h"
#include "FGPCharacterBase.h"
#include "FGPGameInstanceBase.h"
#include "FGPSaveGame.h"
#include "Items/FGPItem.h"
#include "Quests/FGPQuestLog.h"
#include "FGPDataSingleton.h"
#include "FGPBlueprintLibrary.h"


AFGPPlayerControllerBase::AFGPPlayerControllerBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bool bIsValid = false;
	DataSingleton = UFGPBlueprintLibrary::GetDataSingleton(bIsValid);
	if (!bIsValid) return;
	TSubclassOf<UFGPQuestLog> QuestLogClass = DataSingleton->QuestLogComponent_BPC;

	if (QuestLogClass)
	{
		//LogWarning("QuestLogComponent class creation successful.");
		QuestLogComponent = static_cast<UFGPQuestLog*>(
			ObjectInitializer.CreateDefaultSubobject(
				this
				, TEXT("Quest Log Component")
				, UFGPQuestLog::StaticClass()
				, QuestLogClass
				, /*bIsRequired =*/ false
				, /*bIsAbstract =*/ false
				, /*bTransient =*/ false));

		if (!IsValid(QuestLogComponent))
// 		{
// 			LogWarning((TEXT("%s created successfully."), *QuestLogComponent->GetName()));
// 		}
// 		else
		{
			LogWarning("Failed to create QuestLogComponent.");
		}
	}
	else
	{
		LogWarning("Failed to create QuestLogComponent class.");
	}
}

void AFGPPlayerControllerBase::BeginPlay()
{
	// Load inventory off save game before starting play
	LoadInventory();

	// Start play
	Super::BeginPlay();
}

UFGPQuestLog* AFGPPlayerControllerBase::GetQuestLogComponent() const
{
	return QuestLogComponent;
}

bool AFGPPlayerControllerBase::AddInventoryItem(
	UFGPItem* NewItem
	, int32 ItemCount
	, int32 ItemLevel
	, bool bAutoSlot
) {
	bool bChanged = false;
	if (NewItem == nullptr)
	{
		LogWarning(TEXT("AddInventoryItem: Failed trying to add null item!"));
		return false;
	}

	if (ItemCount <= 0 || ItemLevel <= 0)
	{
		FString Msg = FString("AddInventoryItem: Failed trying to add item ")
			+ *NewItem->GetName()
			+ FString(" with negative count or level!");
		LogWarning(Msg);
		return false;
	}

	// Find current item data, which may be empty
	FFGPItemData OldData;
	GetInventoryItemData(NewItem, OldData);

	// Find modified data
	FFGPItemData NewData = OldData;
	NewData.UpdateItemData(FFGPItemData(ItemCount, ItemLevel),
		NewItem->MaxCount, NewItem->MaxLevel);

	if (OldData != NewData)
	{
		// If data changed, need to update storage and call callback
		InventoryData.Add(NewItem, NewData);
		NotifyInventoryItemChanged(true, NewItem);
		bChanged = true;
	}

	if (bAutoSlot)
	{
		// Slot item if required
		bChanged |= FillEmptySlotWithItem(NewItem);//{assignment by bitwise OR}
	}

	if (bChanged)
	{
		// If anything changed, write to save game
		SaveInventory();
		return true;
	}
	return false;
}

bool AFGPPlayerControllerBase::RemoveInventoryItem(
	UFGPItem* RemovedItem
	, int32 RemoveCount
) {
	if (RemovedItem == nullptr)
	{
		FString Msg = FString("RemoveInventoryItem: Failed trying to")
			+ FString("add null item!");
		LogWarning(Msg);
		return false;
	}

	// Find current item data, which may be empty
	FFGPItemData NewData;
	GetInventoryItemData(RemovedItem, NewData);

	if (!NewData.IsValid())
	{
		// Wasn't found
		return false;
	}

	// If RemoveCount <= 0, delete all
	if (RemoveCount <= 0)
	{
		NewData.ItemCount = 0;
	}
	else
	{
		NewData.ItemCount -= RemoveCount;
	}

	if (NewData.ItemCount > 0)
	{
		// Update data with new count
		InventoryData.Add(RemovedItem, NewData);
	}
	else
	{
		// Remove item entirely, make sure it is unslotted
		InventoryData.Remove(RemovedItem);
		
		for (TPair<FFGPItemSlot, UFGPItem*>& Pair : SlottedItems)
		{
			if (Pair.Value == RemovedItem)
			{
				Pair.Value = nullptr;
				NotifySlottedItemChanged(Pair.Key, Pair.Value);
			}
		}
	}

	// If we got this far, there is a change so notify and save
	NotifyInventoryItemChanged(false, RemovedItem);

	SaveInventory();
	return true;
}

void AFGPPlayerControllerBase::GetInventoryItems(
	TArray<UFGPItem*>& Items
	, FPrimaryAssetType ItemType
) {
	for (const TPair<UFGPItem*, FFGPItemData>& Pair : InventoryData)
	{
		if (Pair.Key != nullptr)
		{
			UFGPItem* ItemPtr = Pair.Key;
			FPrimaryAssetId AssetId = ItemPtr->GetPrimaryAssetId();

			// Filters based on item type
			if (AssetId.PrimaryAssetType == ItemType || !ItemType.IsValid())
			{
				Items.Add(ItemPtr);
			}
		}
	}
}

int32 AFGPPlayerControllerBase::GetInventoryItemCount(UFGPItem* Item) const
{
	const FFGPItemData* FoundItem = InventoryData.Find(Item);

	if (FoundItem != nullptr)
	{
		return FoundItem->ItemCount;
	}
	return 0;
}

bool AFGPPlayerControllerBase::GetInventoryItemData(
	UFGPItem* Item
	, FFGPItemData& ItemData
) const
{
	const FFGPItemData* FoundItem = InventoryData.Find(Item);

	if (FoundItem != nullptr)
	{
		ItemData = *FoundItem;
		return true;
	}
	ItemData = FFGPItemData(0, 0);
	return false;
}

bool AFGPPlayerControllerBase::SetSlottedItem(
	FFGPItemSlot ItemSlot
	, UFGPItem* Item
) {
	// Iterate entire inventory because we need to remove from old slot
	bool bFound = false;
	for (TPair<FFGPItemSlot, UFGPItem*>& Pair : SlottedItems)
	{
		if (Pair.Key == ItemSlot)
		{
			// Add to new slot
			bFound = true;
			Pair.Value = Item;
			NotifySlottedItemChanged(Pair.Key, Pair.Value);
		}
		else if (Item != nullptr && Pair.Value == Item)
		{
			// If this item was found in another slot, remove it
			Pair.Value = nullptr;
			NotifySlottedItemChanged(Pair.Key, Pair.Value);
		}
	}

	if (bFound)
	{
		SaveInventory();
		return true;
	}
	return false;
}

UFGPItem* AFGPPlayerControllerBase::GetSlottedItem(FFGPItemSlot ItemSlot) const
{
	UFGPItem* const* FoundItem = SlottedItems.Find(ItemSlot);

	if (FoundItem != nullptr)
	{
		return *FoundItem;
	}
	return nullptr;
}

void AFGPPlayerControllerBase::GetSlottedItems(
	TArray<UFGPItem*>& Items
	, FPrimaryAssetType ItemType
	, bool bOutputEmptyIndexes
) {
	for (TPair<FFGPItemSlot, UFGPItem*>& Pair : SlottedItems)
	{
		if (Pair.Key.ItemType == ItemType || !ItemType.IsValid())
		{
			Items.Add(Pair.Value);
		}
	}
}

void AFGPPlayerControllerBase::FillEmptySlots()
{
	bool bShouldSave = false;
	for (const TPair<UFGPItem*, FFGPItemData>& Pair : InventoryData)
	{
		bShouldSave |= FillEmptySlotWithItem(Pair.Key);//{assignment by bitwise OR}
	}

	if (bShouldSave)
	{
		SaveInventory();
	}
}

bool AFGPPlayerControllerBase::SaveInventory()
{
	// save inventory into active FGPGameInstanceBase subclass
	UWorld* World = GetWorld();
	UFGPGameInstanceBase* GameInstance = World != nullptr ? World->GetGameInstance<UFGPGameInstanceBase>() : nullptr;

	if (GameInstance == nullptr)
	{
		return false;
	}

	UFGPSaveGame* CurrentSaveGame = GameInstance->GetCurrentSaveGame();
	if (CurrentSaveGame != nullptr)
	{
		// Reset cached data in save game before writing to it
		CurrentSaveGame->InventoryData.Reset();
		CurrentSaveGame->SlottedItems.Reset();

		for (const TPair<UFGPItem*, FFGPItemData>& ItemPair : InventoryData)
		{
			FPrimaryAssetId AssetId;
			UFGPItem* Item = ItemPair.Key;
			FFGPItemData ItemData = ItemPair.Value;
			if (Item != nullptr)
			{
				AssetId = Item->GetPrimaryAssetId();
				CurrentSaveGame->InventoryData.Add(AssetId, ItemData);
			}
		}
		for (const TPair<FFGPItemSlot, UFGPItem*>& SlotPair : SlottedItems)
		{
			FPrimaryAssetId AssetId;
			UFGPItem* Item = SlotPair.Value;
			FFGPItemSlot ItemSlot = SlotPair.Key;
			if (Item != nullptr)
			{
				AssetId = Item->GetPrimaryAssetId();
				CurrentSaveGame->SlottedItems.Add(ItemSlot, AssetId);
			}
		}
		GameInstance->WriteSaveGame();
		return true;
	}
	return false;
}

bool AFGPPlayerControllerBase::LoadInventory()
{
	InventoryData.Reset();
	SlottedItems.Reset();

	// Fill in slots from active FGPGameInstanceBase subclass
	UWorld* World = GetWorld();
	UFGPGameInstanceBase* GameInstance = World != nullptr ? World->GetGameInstance<UFGPGameInstanceBase>() : nullptr;

	if (GameInstance == nullptr)
	{
		return false;
	}

	for (const TPair<FPrimaryAssetType, int32>& Pair : GameInstance->ItemSlotsPerType)
	{
		for (int32 SlotNumber = 0; SlotNumber < Pair.Value; SlotNumber++)
		{
			FPrimaryAssetType type = Pair.Key;
			//LogMsg("Creating Slots: ", type.GetName().ToString());
			SlottedItems.Add(FFGPItemSlot(Pair.Key, SlotNumber), nullptr);
		}
	}

	UFGPSaveGame* CurrentSaveGame = GameInstance->GetCurrentSaveGame();
	UFGPAssetManager& AssetManager = UFGPAssetManager::Get();
	if (CurrentSaveGame != nullptr)
	{
		//LogMsg("Found save game");

		// Copy from save game into controller data
		bool bFoundAnySlots = false;
		for (const TPair<FPrimaryAssetId, FFGPItemData>& ItemPair : CurrentSaveGame->InventoryData)
		{
			UFGPItem* LoadedItem = AssetManager.ForceLoadItem(ItemPair.Key);
			if (LoadedItem != nullptr)
			{
				//LogMsg("Adding to inventory: ", LoadedItem->ItemName.ToString());
				InventoryData.Add(LoadedItem, ItemPair.Value);
			}
		}

		for (const TPair<FFGPItemSlot, FPrimaryAssetId>& SlotPair : CurrentSaveGame->SlottedItems)
		{
			FPrimaryAssetId SlotItemAssetId = SlotPair.Value;
			if (SlotItemAssetId.IsValid())
			{
				UFGPItem* LoadedItem = AssetManager.ForceLoadItem(SlotItemAssetId);
				if (GameInstance->IsValidItemSlot(SlotPair.Key)	&& LoadedItem /*!= nullptr*/)
				{
					//LogMsg("Adding to slottedItems: ", LoadedItem->ItemName.ToString());
					SlottedItems.Add(SlotPair.Key, LoadedItem);
					bFoundAnySlots = true;
				}
			}
		}

		if (!bFoundAnySlots)
		{
			// Auto slot items as no slots were saved
			//LogMsg("Did not find any filled slots");
			FillEmptySlots();
		}
		NotifyInventoryLoaded();
		return true;
	}

	// Load failed but we reset inventory, so we need to notify UI
	NotifyInventoryLoaded();
	return false;
}

bool AFGPPlayerControllerBase::FillEmptySlotWithItem(UFGPItem* NewItem)
{
	// Look for an empty item slot to fill with this item
	//LogMsg("Starting to fill slot with item: ", NewItem->ItemName.ToString());
	FPrimaryAssetType NewItemType = NewItem->GetPrimaryAssetId().PrimaryAssetType;
	//LogMsg("New slot type: ", NewItemType.ToString());
	FFGPItemSlot EmptySlot;
	for (TPair<FFGPItemSlot, UFGPItem*>& Pair : SlottedItems)
	{
		FFGPItemSlot PairItemSlot = Pair.Key;
		UFGPItem* PairItem = Pair.Value;
		if (PairItemSlot.ItemType == NewItemType)
		{
			if (PairItem == NewItem)
			{
				// Item is already slotted
				//LogMsg("Item is already slotted: ", NewItem->ItemName.ToString());
				return false;
			}
			else if (PairItem == nullptr && (!EmptySlot.IsValid() || EmptySlot.SlotNumber > PairItemSlot.SlotNumber))
			{
				// We found an empty slot worth filling
				//LogMsg("Slot was empty: ", FString::FromInt(PairItemSlot.SlotNumber));
				EmptySlot = PairItemSlot;
			}
		}
	}

	if (EmptySlot.IsValid())
	{
		SlottedItems[EmptySlot] = NewItem;
		NotifySlottedItemChanged(EmptySlot, NewItem);
		return true;
	}
	return false;
}

void AFGPPlayerControllerBase::NotifyInventoryItemChanged(
	bool bAdded
	, UFGPItem* Item
) {
	// Notify native before blueprint
	OnInventoryItemChangedNative.Broadcast(bAdded, Item);
	OnInventoryItemChanged.Broadcast(bAdded, Item);

	// Notify QuestLog of changes to inventory, so quests can update their collectable objectives if need be

}

void AFGPPlayerControllerBase::NotifySlottedItemChanged(
	FFGPItemSlot ItemSlot
	, UFGPItem* Item
) {
	// Notify native before blueprint
	OnSlottedItemChangedNative.Broadcast(ItemSlot, Item);
	OnSlottedItemChanged.Broadcast(ItemSlot, Item);
}

void AFGPPlayerControllerBase::NotifyInventoryLoaded()
{
	// Notify native before blueprint
	OnInventoryLoadedNative.Broadcast();
	OnInventoryLoaded.Broadcast();
}
