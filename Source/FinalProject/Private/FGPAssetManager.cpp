// Fill out your copyright notice in the Description page of Project Settings.

#include "FGPAssetManager.h"
#include "FinalProject.h"
#include "Items/FGPItem.h"

const FPrimaryAssetType	UFGPAssetManager::PotionItemType = TEXT("Potion");
const FPrimaryAssetType	UFGPAssetManager::SkillItemType = TEXT("Skill");
const FPrimaryAssetType	UFGPAssetManager::CurrencyItemType = TEXT("Currency");
const FPrimaryAssetType	UFGPAssetManager::CollectableItemType = TEXT("Collectable");
const FPrimaryAssetType	UFGPAssetManager::WeaponItemType = TEXT("Weapon");

UFGPAssetManager& UFGPAssetManager::Get()
{
	UFGPAssetManager* _this = Cast<UFGPAssetManager>(GEngine->AssetManager);

	if (_this != nullptr)
	{
		return *_this;
	}
	else
	{
		FString Msg = FString("Invalid AssetManager in DefaultEngine.ini, ")
			+ FString(" must be UFGPAssetManager!");
		LogFatal(Msg);
		return *NewObject<UFGPAssetManager>();
	}
}

UFGPItem* UFGPAssetManager::ForceLoadItem(
	const FPrimaryAssetId& PrimaryAssetId
	, bool bLogWarning
) {
	FSoftObjectPath ItemPath = GetPrimaryAssetPath(PrimaryAssetId);

	// This does a synchronous load and may hitch
	UFGPItem* LoadedItem = Cast<UFGPItem>(ItemPath.TryLoad());

	if (bLogWarning && LoadedItem == nullptr)
	{
		FString Msg = FString("Failed to load item for identifier")
			+ *PrimaryAssetId.ToString()
			+ FString("!");
		LogWarning(Msg);
	}

	return LoadedItem;
}