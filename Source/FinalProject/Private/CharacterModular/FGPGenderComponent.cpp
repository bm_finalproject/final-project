/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPGenderComponent.cpp
 * Description	: 
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 ******************************************************************************/


#include "FGPGenderComponent.h"

// Sets default values for this component's properties
UFGPGenderComponent::UFGPGenderComponent(const FObjectInitializer& ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	PrimaryComponentTick.bAllowTickOnDedicatedServer = false;

}

ECharacterGender UFGPGenderComponent::GetGender() const
{
	return Gender;
}

void UFGPGenderComponent::SetGender(const ECharacterGender& SelectedGender)
{
	if (Gender == SelectedGender) return;
	Gender = SelectedGender;
	NotifyOnGenderChanged(Gender);
}

bool UFGPGenderComponent::IsFemale()
{
	return (Gender == ECharacterGender::Female);
}

void UFGPGenderComponent::ToggleGender()
{
	SetGender(Gender != ECharacterGender::Male ? ECharacterGender::Male : ECharacterGender::Female);
}

void UFGPGenderComponent::NotifyOnGenderChanged(const ECharacterGender& SelectedGender)
{
	GenderChangedNative(SelectedGender);
	GenderChanged(SelectedGender);
}

void UFGPGenderComponent::GenderChangedNative_Implementation(const ECharacterGender& SelectedGender)
{
	LogMsg("Actor is", GetGenderNameStringStatic(SelectedGender));
}


FName UFGPGenderComponent::GetGenderNameStatic(const ECharacterGender& SelectedGender)
{
	return FName(*GetGenderNameStringStatic(SelectedGender));
}

FName UFGPGenderComponent::GetGenderName()
{
	return UFGPGenderComponent::GetGenderNameStatic(Gender);
}

FString UFGPGenderComponent::GetGenderNameString()
{
	return UFGPGenderComponent::GetGenderNameStringStatic(Gender);
}

FString UFGPGenderComponent::GetGenderNameStringStatic(const ECharacterGender& SelectedGender)
{
	return FString(SelectedGender == ECharacterGender::Male ? "Male" : "Female");
}
