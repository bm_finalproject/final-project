// Fill out your copyright notice in the Description page of Project Settings.


#include "FGPModularComponent.h"
#include "FGPGenderComponent.h"


UFGPModularComponent::UFGPModularComponent(const FObjectInitializer& ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	PrimaryComponentTick.bAllowTickOnDedicatedServer = false;

	BodyPartsNames.Add("Head");
	BodyPartsNames.Add("Ears");
	BodyPartsNames.Add("Hair");
	BodyPartsNames.Add("Eyebrow");
	BodyPartsNames.Add("FacialHair");
	BodyPartsNames.Add("Torso");
	BodyPartsNames.Add("ArmUpper");
	BodyPartsNames.Add("ArmLower");
	BodyPartsNames.Add("Hand");
	BodyPartsNames.Add("Hips");
	BodyPartsNames.Add("Leg");

	for (FName Name : BodyPartsNames)
	{
		BodyParts.Add(Name);
	}
}

void UFGPModularComponent::BeginPlay()
{
	Super::BeginPlay();

	
}

void UFGPModularComponent::SelectMeshFromDataTable(const FName& BodyPartToUpdate)
{
	FName PartName = BodyPartToUpdate;
	if (!BodyParts.Contains(PartName))
		return;

	UFGPGenderComponent* GenderComponent = Execute_GetGenderComponent(this);
	if (GenderComponent == nullptr)
	{
		return;
	}

	bool bIsFemale = GenderComponent->IsFemale();
	UDataTable* DataTable = const_cast<UDataTable*>(BodyParts[PartName].BodyPart.DataTable);
	FName RowName = BodyParts[PartName].BodyPart.RowName;
	FFGPChrParts_MaleFemaleMeshes ChrModelSelected;

	if (DataTable != NULL)
	{
		//LogWarning(RowName.ToString());
		if (GetRowByName(DataTable, RowName, ChrModelSelected))
		{
			BodyParts[PartName].MeshPreview = bIsFemale ? ChrModelSelected.Female : ChrModelSelected.Male;
		}
		else {
			//LogWarning("Failed to get row from datatable:", GetBodyPartName_String(BodyPartToUpdate));
			BodyParts[PartName].MeshPreview = nullptr;
		}
	}
	else {
		//LogWarning("No datatable found:", GetBodyPartName_String(BodyPartToUpdate));
		BodyParts[PartName].MeshPreview = nullptr;
	}
}

void UFGPModularComponent::UpdateOwnerEveryMeshComponent()
{
	for (FName Name : BodyPartsNames)
	{
		UpdateOwnerMeshComponent(Name);
	}
}

void UFGPModularComponent::UpdateOwnerMeshComponent(const FName& BodyPartToUpdate)
{
	FName PartName = BodyPartToUpdate;
	if (!BodyParts.Contains(PartName))
		return;

	USkeletalMeshComponent* Mesh = nullptr;
	USkeletalMeshComponent* MasterComponent = Execute_GetHeadMeshComponent(this);

	if (PartName.ToString().Equals(BodyPartsNames[0].ToString()))
		Mesh = MasterComponent;	// interface function call

	else if (PartName.ToString().Equals(BodyPartsNames[1].ToString()))
		Mesh = Execute_GetEarsMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[2].ToString()))
		Mesh = Execute_GetHairMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[3].ToString()))
		Mesh = Execute_GetEyebrowMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[4].ToString()))
		Mesh = Execute_GetFacialHairMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[5].ToString()))
		Mesh = Execute_GetTorsoMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[6].ToString()))
		Mesh = Execute_GetArmUpperMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[7].ToString()))
		Mesh = Execute_GetArmLowerMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[8].ToString()))
		Mesh = Execute_GetHandMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[9].ToString()))
		Mesh = Execute_GetHipsMeshComponent(this);

	else if (PartName.ToString().Equals(BodyPartsNames[10].ToString()))
		Mesh = Execute_GetLegMeshComponent(this);

	if (Mesh == nullptr)
	{
		LogWarning("Could not retrieve a mesh component from the component's onwer. Are you sure you have implemented the interface functions on the owner class side?");
		return;
	}

	SelectMeshFromDataTable(PartName);

	//Change skeletal mesh component mesh model
	Mesh->SetSkeletalMesh(BodyParts[PartName].MeshPreview);

	// synchronize animations
	if (Mesh != MasterComponent)
	{
		Mesh->SetMasterPoseComponent(MasterComponent);
	}
}

bool UFGPModularComponent::UpdateOwnerCharacterMesh()
{
	return Execute_UpdateCharacterMesh(this);
}

void UFGPModularComponent::AddDynamicMaterialInstanceToBodyPartsMap(
	const FName& BodyPart
	, UMaterialInstanceDynamic* MatInstanceDynamic
) {
	FName PartName = BodyPart;
	if (!BodyParts.Contains(PartName))
		return;

	BodyParts[PartName].MatInstanceDynamic = MatInstanceDynamic;
}

////////////////////////////////////////////////////////////////////////////////
// --------------- MODULAR MANAGER INTERFACE IMPLEMENTATIONS -----------------//
// /////////////////////////////////////////////////////////////////////////////
bool UFGPModularComponent::UpdateCharacterMesh_Implementation()
{
	UpdateOwnerEveryMeshComponent();
	return true;
}

UFGPGenderComponent* UFGPModularComponent::GetGenderComponent_Implementation()
{
	UFGPGenderComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPGenderInterface>())
	{
		Component = IFGPGenderInterface::Execute_GetGenderComponent(GetOwner());
	}
	return Component;
}

UMaterialInstanceDynamic*
UFGPModularComponent::CreateMaterialInstanceDynamic_Implementation(
	USkeletalMeshComponent* SelectedMesh
) {
	return nullptr;
}

FFGPChrParts_BodyTextureColorStructure
UFGPModularComponent::ApplyBodyColorsToMeshComponent_Implementation(
	const FName& BodyPartName
	, USkeletalMeshComponent* MeshComponentSelected
	, UMaterialInstanceDynamic* MatInstanceDynamic
	, const FFGPChrParts_BodyTextureColorStructure& ColorBody
	, FDataTableRowHandle ColorBodyDataTableHandle
) {
	// Implementation on character blueprint
	FFGPChrParts_BodyTextureColorStructure FinalBodyColor;
	return FinalBodyColor;
}

FFGPChrParts_EquipmentTextureColorStructure
UFGPModularComponent::ApplyEquipmentColorsToMeshComponent_Implementation(
	const FName& BodyPartName, USkeletalMeshComponent* MeshComponentSelected
	, UMaterialInstanceDynamic* MatInstanceDynamic
	, const FFGPChrParts_EquipmentTextureColorStructure& ColorBody
	, FDataTableRowHandle ColorBodyDataTableHandle
) {
	// Implementation on character blueprint
	FFGPChrParts_EquipmentTextureColorStructure FinalBodyColor;
	return FinalBodyColor;
}

USkeletalMeshComponent* UFGPModularComponent::GetHeadMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if(GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetHeadMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetEarsMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetEarsMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetHairMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetHairMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetEyebrowMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetEyebrowMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetFacialHairMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetFacialHairMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetTorsoMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetTorsoMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetArmUpperMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetArmUpperMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetArmLowerMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetArmLowerMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetHandMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetHandMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetHipsMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetHipsMeshComponent(GetOwner());
	}
	return Component;
}

USkeletalMeshComponent* UFGPModularComponent::GetLegMeshComponent_Implementation()
{
	USkeletalMeshComponent* Component = nullptr;
	if (GetOwner() == nullptr)
		return nullptr;
	if (GetOwner()->Implements<UFGPModularManagerInterface>())
	{
		Component = IFGPModularManagerInterface::Execute_GetLegMeshComponent(GetOwner());
	}
	return Component;
}



FName UFGPModularComponent::GetBodyPartNameByIndex(const int32& Index)
{
	return (Index > BodyPartsNames.Num() ? FName("") : BodyPartsNames[Index]);
}

