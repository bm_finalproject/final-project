/*******************************************************************************
 * Bachelor of Games and Apps Development
 * Universidade Europeia
 * Lisboa
 * Portugal
 * 
 * (c) 2019 Universidade Europeia
 * 
 * File Name	: FGPCharacterBase.cpp
 * Description	: Base class for Character
 * 
 * Author		: Bruno Matos
 * Mail			: bruno.rosal.matos@gmail.com
 * 
 * (based on implementation provided by Epic Game's ActionRPG learning project.)
 ******************************************************************************/

#include "FGPCharacterBase.h"
#include "FinalProject.h"
#include "Items/FGPItem.h"
#include "Abilities/FGPGameplayAbility.h"

// Engine
#include "AbilitySystemGlobals.h"


AFGPCharacterBase::AFGPCharacterBase()
{
 	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UFGPAbilitySystemComponent>
		(TEXT("AbilitySystemComponent"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<UFGPAttributeSet>
		(TEXT("AttributeSet"));

	CharacterLevel = 1;
	bAbilitiesInitialized = false;
}

void AFGPCharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	// Try setting the inventory source, this will fail if it's
	// AI controlled character
	InventorySource = NewController;

	if (InventorySource != nullptr)
	{
		InventoryUpdateHandle =
			InventorySource->GetSlottedItemChangedDelegate()
				.AddUObject(this, &AFGPCharacterBase::OnItemSlotChanged);
		InventoryLoadedHandle =
			InventorySource->GetInventoryLoadedDelegate()
				.AddUObject( this,
					&AFGPCharacterBase::RefreshSlottedGameplayAbilities);
	}
	
	// Initialize abilities
	if (AbilitySystemComponent != nullptr)
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
		AddStartupGameplayAbilities();
	}

	OnSkeletalMeshChanged.AddDynamic(this, &AFGPCharacterBase::OnSkeletalMeshChangedEvent);
}

void AFGPCharacterBase::UnPossessed()
{
	// Unmap from inventory source
	if (InventorySource != nullptr && InventoryUpdateHandle.IsValid())
	{
		InventorySource->GetSlottedItemChangedDelegate()
			.Remove(InventoryUpdateHandle);
		InventoryUpdateHandle.Reset();

		InventorySource->GetInventoryLoadedDelegate()
			.Remove(InventoryLoadedHandle);
		InventoryLoadedHandle.Reset();
	}

	InventorySource = nullptr;
}

void AFGPCharacterBase::OnRep_Controller()
{
	Super::OnRep_Controller();

	// Our controller changed, must update ActorInfo on AbilitySystemComponent
	if (AbilitySystemComponent != nullptr)
	{
		AbilitySystemComponent->RefreshAbilityActorInfo();
	}
}

void AFGPCharacterBase::GetLifetimeReplicatedProps(
	TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFGPCharacterBase, CharacterLevel);
}

UAbilitySystemComponent* AFGPCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

float AFGPCharacterBase::GetHealth() const
{
	return AttributeSet->GetHealth();
}

float AFGPCharacterBase::GetMaxHealth() const
{
	return AttributeSet->GetMaxHealth();
}

float AFGPCharacterBase::GetMana() const
{
	return AttributeSet->GetMana();
}

float AFGPCharacterBase::GetMaxMana() const
{
	return AttributeSet->GetMaxMana();
}

float AFGPCharacterBase::GetMoveSpeed() const
{
	return AttributeSet->GetMoveSpeed();
}

int32 AFGPCharacterBase::GetCharacterLevel() const
{
	return CharacterLevel;
}

bool AFGPCharacterBase::SetCharacterLevel(int32 NewLevel)
{
	if (CharacterLevel != NewLevel && NewLevel > 0)
	{
		// Our level changed so we need to refresh abilities
		RemoveStartupGameplayAbilities();
		CharacterLevel = NewLevel;
		AddStartupGameplayAbilities();

		return true;
	}
	return false;
}

bool AFGPCharacterBase::ActivateAbilitiesWithItemSlot(
	FFGPItemSlot ItemSlot
	, bool bAllowRemoteActivation
) {
	FGameplayAbilitySpecHandle* FoundHandle = SlottedAbilities.Find(ItemSlot);

	if (FoundHandle != nullptr && AbilitySystemComponent != nullptr)
	{
		return (AbilitySystemComponent->TryActivateAbility(
						*FoundHandle, bAllowRemoteActivation));
	}

	return false;
}

void AFGPCharacterBase::GetActiveAbilitiesWithItemSlot(
	FFGPItemSlot ItemSlot
	, TArray<UFGPGameplayAbility*>& ActiveAbilities
) {
	FGameplayAbilitySpecHandle* FoundHandle = SlottedAbilities.Find(ItemSlot);

	if (FoundHandle != nullptr && AbilitySystemComponent != nullptr)
	{
		FGameplayAbilitySpec* FoundSpec =
			AbilitySystemComponent->FindAbilitySpecFromHandle(*FoundHandle);

		if (FoundSpec)
		{
			TArray<UGameplayAbility*> AbilityInstances =
				FoundSpec->GetAbilityInstances();

			// Find all ability instances executed from this slot
			for (UGameplayAbility* ActiveAbility : AbilityInstances)
			{
				ActiveAbilities.Add(Cast<UFGPGameplayAbility>(ActiveAbility));
			}
		}
	}
}

bool AFGPCharacterBase::ActivateAbilitiesWithTags(
	FGameplayTagContainer AbilityTags
	, bool bAllowRemoteActivation
) {
	if (AbilitySystemComponent != nullptr)
	{
		return (AbilitySystemComponent->TryActivateAbilitiesByTag(AbilityTags,
			bAllowRemoteActivation));
	}

	return false;
}

void AFGPCharacterBase::GetActiveAbilitiesWithTags(
	FGameplayTagContainer AbilityTags
	, TArray<UFGPGameplayAbility*>& ActiveAbilities
) {
	if (AbilitySystemComponent != nullptr)
	{
		AbilitySystemComponent->GetActiveAbilitiesWithTags(AbilityTags,
			ActiveAbilities);
	}
}

bool AFGPCharacterBase::GetCooldownRemainingForTag(
	FGameplayTagContainer CooldownTags
	, float& TimeRemaining
	, float& CooldownDuration
) {
	if (AbilitySystemComponent != nullptr && CooldownTags.Num() > 0)
	{
		TimeRemaining = 0.f;
		CooldownDuration = 0.f;

		FGameplayEffectQuery const Query =
			FGameplayEffectQuery::MakeQuery_MatchAnyOwningTags(CooldownTags);
		TArray<TPair<float, float>> DurationAndTimeRemaining =
			AbilitySystemComponent->
				GetActiveEffectsTimeRemainingAndDuration(Query);
		int32 DurationAndTimeRemainingArrayNum = DurationAndTimeRemaining.Num();
		if (DurationAndTimeRemainingArrayNum > 0)
		{
			int32 BestIdx = 0;
			TPair<float, float> iPair = DurationAndTimeRemaining[0];
			float LongestTime = iPair.Key;

			for (int32 Idx = 1; Idx < DurationAndTimeRemainingArrayNum; ++Idx)
			{
				iPair = DurationAndTimeRemaining[Idx];
				if (iPair.Key > LongestTime)
				{
					LongestTime = iPair.Key;
					BestIdx = Idx;
				}
			}

			iPair = DurationAndTimeRemaining[BestIdx];
			TimeRemaining = iPair.Key;
			CooldownDuration = iPair.Value;

			return true;
		}
	}
	return false;
}

bool AFGPCharacterBase::IsInteractable(FGameplayTagContainer TagContainer) const
{
	return InteractionTags.HasAny(TagContainer);
}

void AFGPCharacterBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	Execute_FGPPostInitializeComponents(this);
}

bool AFGPCharacterBase::FGPPostInitializeComponents_Implementation()
{
	return false;
}

// void AFGPCharacterBase::Tick(float DeltaSeconds)
// {
// 	Super::Tick(DeltaSeconds);
// 	
// 	Health = GetHealth();
// }

void AFGPCharacterBase::OnItemSlotChanged(
	FFGPItemSlot ItemSlot
	, UFGPItem* Item
) {
	RefreshSlottedGameplayAbilities();
}

void AFGPCharacterBase::RefreshSlottedGameplayAbilities()
{
	if (bAbilitiesInitialized)
	{
		// Refresh any invalid abilities and adds new ones
		RemoveSlottedGameplayAbilities(false);
		AddSlottedGameplayAbilities();
	}
}

void AFGPCharacterBase::AddStartupGameplayAbilities()
{
	check(AbilitySystemComponent);

	if (Role == ROLE_Authority && !bAbilitiesInitialized)
	{
		// Grant abilities, but only on the server
		for (TSubclassOf<UFGPGameplayAbility>& StartupAbility
			: GameplayAbilities)
		{
			AbilitySystemComponent->GiveAbility(
				FGameplayAbilitySpec(StartupAbility, GetCharacterLevel(),
					INDEX_NONE, this));
		}

		// Now apply passive abilities
		for (TSubclassOf<UGameplayEffect>& GameplayEffect
			: PassiveGameplayEffects)
		{
			FGameplayEffectContextHandle EffectContect =
				AbilitySystemComponent->MakeEffectContext();
			EffectContect.AddSourceObject(this);

			FGameplayEffectSpecHandle NewHandle =
				AbilitySystemComponent->MakeOutgoingSpec(GameplayEffect,
					GetCharacterLevel(), EffectContect);
			if (NewHandle.IsValid())
			{
				FActiveGameplayEffectHandle ActiveGEHandle =
					AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(
						*NewHandle.Data.Get(), AbilitySystemComponent);
			}
		}

		AddSlottedGameplayAbilities();

		bAbilitiesInitialized = true;
	}
}

void AFGPCharacterBase::RemoveStartupGameplayAbilities()
{
	check(AbilitySystemComponent);

	if (Role == ROLE_Authority && bAbilitiesInitialized)
	{
		// Remove any abilities added from a previous call
		TArray<FGameplayAbilitySpecHandle> AbilitiesToRemove;
		for (const FGameplayAbilitySpec& Spec
			: AbilitySystemComponent->GetActivatableAbilities())
		{
			if ((Spec.SourceObject == this)
				&& GameplayAbilities.Contains(Spec.Ability->GetClass()))
			{
				AbilitiesToRemove.Add(Spec.Handle);
			}
		}

		int32 AbilitiesTotal = AbilitiesToRemove.Num();
		// Do in two passes so the removal happens after we have the full list
		for (int32 iAbility = 0; iAbility < AbilitiesTotal; iAbility++)
		{
			AbilitySystemComponent->ClearAbility(AbilitiesToRemove[iAbility]);
		}

		// Remove all of the passive gameplay effects that were applied by
		// this character
		FGameplayEffectQuery Query;
		Query.EffectSource = this;
		AbilitySystemComponent->RemoveActiveEffects(Query);

		RemoveSlottedGameplayAbilities(true);
		bAbilitiesInitialized = false;
	}
}

void AFGPCharacterBase::AddSlottedGameplayAbilities()
{
	TMap<FFGPItemSlot, FGameplayAbilitySpec> SlottedAbilitySpecs;
	FillSlottedAbilitySpecs(SlottedAbilitySpecs);

	// Add abilities if needed
	for (const TPair<FFGPItemSlot, FGameplayAbilitySpec>& SpecPair
		: SlottedAbilitySpecs)
	{
		FGameplayAbilitySpecHandle& SpecHandle =
			SlottedAbilities.FindOrAdd(SpecPair.Key);

		if (!SpecHandle.IsValid())
		{
			SpecHandle = AbilitySystemComponent->GiveAbility(SpecPair.Value);
		}
	}
}

void AFGPCharacterBase::FillSlottedAbilitySpecs(
	TMap<FFGPItemSlot, FGameplayAbilitySpec>& SlottedAbilitySpecs
) {
	// First add default ones
	for (const TPair<FFGPItemSlot, TSubclassOf<UFGPGameplayAbility>>&
			DefaultPair : DefaultSlottedAbilities)
	{
		if (DefaultPair.Value.Get())
		{
			SlottedAbilitySpecs.Add(DefaultPair.Key,
				FGameplayAbilitySpec(DefaultPair.Value, GetCharacterLevel(),
					INDEX_NONE, this));
		}
	}

	// Now potentially override with inventory
	if (InventorySource != nullptr)
	{
		const TMap<FFGPItemSlot, UFGPItem*>& SLottedItemMap =
			InventorySource->GetSlottedItemMap();

		for (const TPair<FFGPItemSlot, UFGPItem*>& ItemPair : SLottedItemMap)
		{
			UFGPItem* SlottedItem = ItemPair.Value;

			if (SlottedItem != nullptr && SlottedItem->GrantedAbility != NULL)
			{
				// This will override anything from default
				SlottedAbilitySpecs.Add(ItemPair.Key,
					FGameplayAbilitySpec(SlottedItem->GrantedAbility,
						GetCharacterLevel(), INDEX_NONE, SlottedItem));
			}
		}
	}
}

void AFGPCharacterBase::RemoveSlottedGameplayAbilities(bool bRemoveAll)
{
	TMap<FFGPItemSlot, FGameplayAbilitySpec> SlottedAbilitySpecs;

	if (!bRemoveAll)
	{
		// Fill in map so we can compare
		FillSlottedAbilitySpecs(SlottedAbilitySpecs);
	}

	for (TPair<FFGPItemSlot, FGameplayAbilitySpecHandle>& ExistingPair
		: SlottedAbilities)
	{
		FGameplayAbilitySpec* FoundSpec = AbilitySystemComponent->
			FindAbilitySpecFromHandle(ExistingPair.Value);
		bool bShouldRemove = bRemoveAll || !FoundSpec;

		if (!bShouldRemove)
		{
			// Need to check desired ability specs, if we got here
			// FoundSpec is valid
			FGameplayAbilitySpec* DesiredSpec = 
				SlottedAbilitySpecs.Find(ExistingPair.Key);

			if (!DesiredSpec
				|| DesiredSpec->Ability != FoundSpec->Ability
				|| DesiredSpec->SourceObject != FoundSpec->SourceObject)
			{
				bShouldRemove = true;
			}
		}

		if (bShouldRemove)
		{
			if (FoundSpec)
			{
				// Need to remove registered ability
				AbilitySystemComponent->ClearAbility(ExistingPair.Value);
			}

			// Make sure handle is cleared even if ability wasn't found
			ExistingPair.Value = FGameplayAbilitySpecHandle();
		}
	}

}

void AFGPCharacterBase::HandleDamage(const FDamageInfoParams& DamageInfoParams)
{
	OnDamaged(DamageInfoParams);
}

void AFGPCharacterBase::HandleHealthChanged(
	float DeltaValue
	, const struct FGameplayTagContainer& EventTags
) {
	// Only call BP callback if this is not the initial ability setup
	if (bAbilitiesInitialized)
	{
		OnHealthChanged(DeltaValue, EventTags);
	}
}

void AFGPCharacterBase::HandleManaChanged(
	float DeltaValue
	, const struct FGameplayTagContainer& EventTags
) {
	if (bAbilitiesInitialized)
	{
		OnManaChanged(DeltaValue, EventTags);
	}
}

void AFGPCharacterBase::HandleMoveSpeedChanged(
	float DeltaValue
	, const struct FGameplayTagContainer& EventTags
) {
	// Update the character movement's walk speed
	GetCharacterMovement()->MaxWalkSpeed = GetMoveSpeed();

	if (bAbilitiesInitialized)
	{
		OnMoveSpeedChanged(DeltaValue, EventTags);
	}
}
