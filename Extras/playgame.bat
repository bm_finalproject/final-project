﻿@echo off

pushd ..
set "uplevel=%cd%"

REM this will search inside current directory and sub-directories
REM for /f "tokens=1 delims=" %%a in ('dir /b /s *.uproject') do (

REM START UE4Editor.exe "%%a" -game -log

REM )

REM this will search inside current directory, which is the project root
for /f "delims=" %%F in ('dir /b "%uplevel%\*.uproject" 2^>nul') do set MyProject=%%F

START C:\UERep\Engine\Binaries\Win64\UE4Editor.exe "%uplevel%\%MyProject%" PersistentMap -game

EXIT
